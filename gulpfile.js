'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload,
    newer = require('gulp-newer'),
    plumber = require('gulp-plumber'),

    postcss = require('gulp-postcss'),
    postcssPartialImport = require('postcss-partial-import'),
    postcssInlineSvg = require('postcss-inline-svg'),
    postcssSvgo = require('postcss-svgo'),

    cssbeautify = require('gulp-cssbeautify'),
    concat = require('gulp-concat'),
    csscomb = require('gulp-csscomb'),
    unusedImages = require('gulp-unused-images'),
    htmlprettify = require('gulp-html-prettify'),
    htmlmin = require('gulp-htmlmin'),
    cleanCSS = require('gulp-clean-css');


var processors = [
    postcssInlineSvg({path: 'src/img/icons/'}),
    postcssSvgo([{'addAttributesToSVGElement': true}])

];


var path = {
    assets: {
        root: 'assets/',
        js: 'assets/js/',
        css: 'assets/css/',
        img: 'assets/img/',
        upload: 'assets/upload/',
        fonts: 'assets/fonts/'
    },
    src: {
        html: 'src/*.html',
        php: 'src/*.php',
        js: 'src/js/*.js',
        style: 'src/style/*.scss',
        plugin: 'src/style/plugins.css',
        img: 'src/img/**/*.*',
        upload: 'src/upload/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    watch: {
        html: 'src/**/*.html',
        php: 'src/**/*.php',
        js: 'src/js/**/*.js',
        style: 'src/style/**/*.scss',
        img: 'src/img/**/*.*',
        upload: 'src/upload/**/*.*',
        fonts: 'src/fonts/**/*.*'
    },
    clean: './assets'
};

var config = {
    proxy: 'assets',
    /* server: {
         baseDir: "./assets"
     },*/
    tunnel: false,
    host: 'localhost',
    port: 9000,
    logPrefix: "daze"
};

gulp.task('webserver', function () {
    browserSync(config);
});

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});


/*gulp.task('html:assets', function () {
    gulp.src(path.src.html)
        .pipe(plumber())
        //.pipe(rigger())
        .pipe(nunjucks.compile())
        .pipe(htmlmin({
            /!* preserveLineBreaks:true,*!/
            collapseWhitespace: true,
            minifyCSS:true,
            minifyJS:true,
            minifyURLs:true,
            /!* removeOptionalTags:true,
             removeRedundantAttributes:true,*!/
            removeComments:true
        }))
        .pipe(htmlprettify({indent_char: ' ', indent_size: 4}))
        .pipe(gulp.dest(path.assets.html))
        .pipe(reload({stream: true}))
        .pipe(browserSync.stream());
});*/
gulp.task('php:assets', function () {
    gulp.src(path.src.php)
        .pipe(plumber())
        //.pipe(rigger())
        .pipe(gulp.dest(path.assets.root))
        .pipe(reload({stream: true}))
        .pipe(browserSync.stream());
});

gulp.task('js:assets', function () {
    gulp.src('bower_components/jquery/dist/jquery.min.js')
        .pipe(gulp.dest(path.assets.js));


    gulp.src(path.src.js)
        .pipe(rigger())

        //.pipe(uglify())

        .pipe(gulp.dest(path.assets.js))
        .pipe(reload({stream: true}));
});

gulp.task('style:assets', function () {
    /*   gulp.src(path.src.plugin)
           .pipe(plumber())
           .pipe(postcss(processors))
           /!*.pipe(rigger())*!/
           .pipe(gulp.dest(path.assets.css))
           .pipe(reload({stream: true}))
           .pipe(browserSync.stream());*/


    gulp.src(path.src.style)
    /*.pipe(rigger())*/
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(postcss([postcssPartialImport]))
        .pipe(sass({
            includePaths: ['src/style/'],
            /*  outputStyle: 'compressed',*/
            sourceMap: false,
            errLogToConsole: true
        }))
        .pipe(postcss(processors))
        /* .pipe(uncss({
         html: [path.assets.html+'*.html','http://localhost:9000'],
             outtime:2000
         }))*/

        .pipe(cleanCSS(/*{format: 'beautify'}*/))
        .pipe(prefixer({browsers: ['last 2 version', 'ie >= 11']}))


        //.pipe(csscomb())

        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.assets.css))
        .pipe(reload({stream: true}))
        .pipe(browserSync.stream());

    /*gulp.src([path.assets.css+'/plugins.css',path.assets.css+'/main.css'])

        .pipe(concat('style.css'))
        .pipe(gulp.dest(path.assets.css))
        /!*  .pipe(uncss({
         html: [path.assets.html+'*.html']
         }))*!/
        /!*     .pipe(postcss(processors))*!/
        .pipe(cleanCSS({keepBreaks:true,compatibility: 'ie8'}))
        .pipe(prefixer({browsers: ['last 30 version']}))
        .pipe(csscomb())
        /!*  .pipe(cssbeautify({
         indent: '   ',
         /!*openbrace: 'separate-line',*!/
         autosemicolon: true
         }))*!/
        .pipe(gulp.dest(path.assets.css))
        .pipe(reload({stream: true}));*/
});


gulp.task('image:assets', function () {
    gulp.src(path.src.upload)
        .pipe(newer(path.assets.upload))
        .pipe(imagemin({
                 progressive: true,
                 svgoPlugins: [{removeViewBox: false}],
                 use: [pngquant()],
                 interlaced: true
        }))
        .pipe(gulp.dest(path.assets.upload))
        .pipe(reload({stream: true}));
    gulp.src(path.src.img)
        .pipe(newer(path.assets.img))
        .pipe(imagemin({
                    progressive: true,
                    svgoPlugins: [{removeViewBox: false}],
                    use: [pngquant()],
                    interlaced: true
        }))
        .pipe(gulp.dest(path.assets.img))
        .pipe(reload({stream: true}));

});

gulp.task('fonts:assets', function () {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.assets.fonts))
});

gulp.task('assets', [
    'php:assets',
    'js:assets',
    'style:assets',
    'fonts:assets',
    'image:assets'
]);

gulp.task('image:filter', function () {
    return gulp.src([path.assets.img + '**/*', path.assets.css + '*.css', path.assets.html + '*.html'])
        .pipe(plumber())
        .pipe(unusedImages({}))
        .pipe(plumber.stop());


});


gulp.task('watch', function () {
    /*  watch([path.watch.html], function(event, cb) {
          gulp.start('html:assets');
      });*/
    watch([path.watch.php], function (event, cb) {
        gulp.start('php:assets');
    });
    watch([path.watch.style], function (event, cb) {
        gulp.start('style:assets');
    });
    watch([path.watch.js], function (event, cb) {
        gulp.start('js:assets');
    });
    watch([path.watch.fonts], function (event, cb) {
        gulp.start('fonts:assets');
    });
    watch([path.watch.img], function (event, cb) {
        gulp.start('image:assets');
    });

});


gulp.task('default', ['assets', 'webserver', 'watch']);

