</article>
</main><!-- /.page__main-->

<footer class="page__footer footer">
    <hr class="footer__hr">
    <div class="g-grid">
        <div class="g-d_b" hidden>
            <? include("_inc_nav_main_blocks.php"); ?>
        </div>

        <div class="footer__list g-row">
            <div class="g-col g-col_md_3">
                <span class="h3 footer__title">Контакты</span>
                <ul class="footer__list g-clr_4">
                    <li><a href="mailto:hello@mobifitness.ru" class="g-link-js">hello@mobifitness.ru</a></li>
                    <li><a href="tel:+74951339532" class="g-link-js">+7 (495) 133-95-32</a></li>
                </ul>
            </div>
            <div class="g-col g-col_md_4 g-plr_0_md g-hidden g-show_md">
                <span class="h3 footer__title">Для клиентов</span>
                <p>Тех. поддержка: <a href="tel:+74951339532" class="g-link-js g-clr_4">+7 (495) 133-95-32</a>
                    (доб. 200)
                    Оставить запрос: <a href="mailto:s@mobifitness.ru" class="g-link-js g-clr_4">s@mobifitness.ru</a>
                    <a href="<?= $p . "ofertaooo.html" ?>" target="_blank" class="g-link-js"> <br>Оферта</a> и <a
                            href="<?= $p . "privacy_policy.pdf" ?>" target="_blank" class="g-link-js">Политика
                        конфиденциальности</a></p>
            </div>
            <div class="g-col g-col_md_4">

                <span class="h3 footer__title">Подписывайтесь!</span>
                <p>Новости, видео, советы экспертов</p>
                <div class="footer__soc">
                    <? include("_inc_soc.php"); ?>
                </div>
            </div>
        </div>

    </div>
    <hr class="footer__hr">
    <div class="g-grid">
        <p><span class="footer__cop">© Mobifitness, 2014 - 2018.</span> Все права защищены.</p>
    </div>
</footer>
<div class="page__arrow-push btn-arrow-push js-scrl2top"></div>

</div><!-- /.page__wrap-->
</div>
<? include("_inc_popups.php"); ?>

<!-- Plus 20 widget -->
<script type="text/javascript" src="https://code.plus20.ru/js/loader.js?cid=14"></script>
<!-- /Plus 20 widget -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function () {
            try {
                w.yaCounter22994086 = new Ya.Metrika({
                    id: 22994086,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true
                });
            } catch (e) {
            }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/22994086" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = location.protocol + '//vk.com/rtrg?r=jQJtKlCHsZ6bxi84LcldR1gBkz/NWy95fNgdHC1gtuDp8SFqLnmejS4MulStHOUZbBJKY0Q*OQFM/wd9QnvR3NX8Oz6KD9TCRNuHSQA7XoEOQLarOQVDQIVXQVjq5ZWOfn5xVL8/6UHF5Sr1ABrfJI394l/PBlEpChC1/KeuMtc-&pixel_id=1000076421';</script>

</body></html>