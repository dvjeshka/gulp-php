<? $breadcrumbs = array(
    array("URL" => "/", "NAME" => "Учетная система"),
    array("URL" => "/kakos", "NAME" => "Приложение "),
    array("URL" => "/kakos2", "NAME" => "Онлайн-запись"),
    array("URL" => "/kakos3", "NAME" => "Другие продукты"),
    array("URL" => "/kakos4", "NAME" => " Контакты"),
) ?>
<nav class="nav-breadcrumbs g-grid">
    <ul class="nav-breadcrumbs__list">
        <? foreach ($breadcrumbs as $arItem) { ?>
            <li class="nav-breadcrumbs__item"><a href="<?= $arItem["URL"] ?>"
                                                 class="nav-breadcrumbs__link"><?= $arItem["NAME"] ?></a></li>
        <? } ?>
        <li class="nav-breadcrumbs__item">Вы тут</li>
    </ul>
</nav>

