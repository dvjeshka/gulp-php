<div class="page__popups g-hidden">
    <div class="js-popup-success-click"
         data-colorbox='{"href":"#popup-success","className":"popup-wrap popup-wrap_success"}'></div>
    <div id="popup-success" class="popup popup-success">
        Спасибо
        <div class="popup-success__title h1">
            <div class="text-variant text-variant-0">
                Что подписались <br> на нашу рассылку
            </div>
            <div class="text-variant text-variant-1">Ваша заявка принята</div>

            <? /* if ($popupAfterVariant == 1) { ?>
                Спасибо, что подписались на нашу рассылку
            <? $popupAfterVariant=0;} else { ?>
            
            <?}*/ ?>
            <!--Спасибо, ваша заявка принята!-->
        </div>
        <div>Присоединяйтесь к нам</div>

        <ul class="soc-big">
            <li class="soc-big__item"><a href="https://vk.com/mobifitness_ru" target="blank"
                                         class="soc-big__link soc-big__link_1 icon-soc-big icon-soc-big_vk">Вконтакте</a>
            </li>
            <li class="soc-big__item"><a href="https://www.instagram.com/mobifitness_ru/" target="blank"
                                         class="soc-big__link soc-big__link_2 icon-soc-big icon-soc-big_inst">Instagram</a>
            </li>
            <li class="soc-big__item"><a href="https://www.facebook.com/mobifitness.ru/" target="blank"
                                         class="soc-big__link soc-big__link_3 icon-soc-big icon-soc-big_fa">Facebook</a>
            </li>
        </ul>


    </div>

    <div id="popup-sale" class="popup">
        <p>Сейчас учетная система Mobifitness доступна в бета-версии. Мы продолжаем собирать обратную связь
            от клиентов и работаем над улучшениями продукта. Поэтому, мы решили предоставить первым пользователям скидку
            на весь набор функций.</p>
        <p>Ваш отзыв может повлиять на развитие системы и сделать рынок фитнеса более развитым и технологичным.</p>


    </div>

    <div id="popup-call-back" class="popup">
        <p>
            <small>Оставьте ваши контакты, и мы свяжемся <br>
                с вами в ближайшее время.
            </small>
        </p>
        <form class="form-dark js-validated">
            <fieldset class="">
                <legend class="h1 g-d_b">Заказать звонок</legend>
                <ul class="form-dark__list">
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Имя*</span>
                            <input type="text" class="form-dark__field" name="name">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Телефон*</span>
                            <input type="tel" class="form-dark__field js-mask" name="phone">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <input type="submit" class="btn btn_sz g-col_xs_12" value="Перезвоните мне">
                    </li>
                </ul>

            </fieldset>
        </form>
    </div>

    <div id="popup-consultation" class="popup">
        <p>
            <small>С удовольствием ответим на ваши вопросы!</small>
        </p>
        <form class="form-dark js-validated" action="?">
            <fieldset class="">
                <legend class="h1 g-d_b">Получите консультацию</legend>
                <ul class="form-dark__list">
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Имя*</span>
                            <input type="text" class="form-dark__field" name="name">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Телефон*</span>
                            <input type="tel" class="form-dark__field js-mask" name="phone">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <input type="submit" class="btn btn_sz g-col_xs_12" value="Перезвоните мне">
                    </li>
                </ul>

            </fieldset>
        </form>
    </div>

    <div id="popup-details" class="popup">
        <p>Отправьте заявку, и мы вышлем вам подробную информацию</p>
        <form class="form-dark js-validated" action="?">
            <fieldset class="">
                <legend class="h1 g-d_b">Узнайте подробнее</legend>
                <ul class="form-dark__list ">
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Имя*</span>
                            <input type="text" class="form-dark__field" name="name">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Телефон*</span>
                            <input type="tel" class="form-dark__field js-mask" name="phone">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">E-mail*</span>
                            <input type="email" class="form-dark__field js-mask" name="email">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Клуб / Компания</span>
                            <input type="text" class="form-dark__field js-mask" name="club"
                                   placeholder="Клуб / Компания">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <input type="submit" class="btn btn_sz g-col_xs_12" value="Отправить заявку">
                    </li>
                </ul>

            </fieldset>
        </form>
    </div>

    <div id="popup-request-rate" class="popup">
        <p>Отправьте заявку, и мы вышлем вам подробную информацию.</p>
        <form class="form-dark js-validated" action="?">
            <fieldset class="">
                <legend class="h1 g-d_b">Отличный выбор!</legend>
                <ul class="form-dark__list ">
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Имя*</span>
                            <input type="text" class="form-dark__field" name="name">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Телефон*</span>
                            <input type="tel" class="form-dark__field js-mask" name="phone">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">E-mail*</span>
                            <input type="email" class="form-dark__field js-mask" name="email">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Клуб / Компания</span>
                            <input type="text" class="form-dark__field js-mask" name="club"
                                   placeholder="Клуб / Компания">
                        </label>
                    </li>
                    <input type="hidden" name="tariff_name" value=""/>
                    <input type="hidden" name="tariff_period" value=""/>
                    <li class="form-line__item">
                        <input type="submit" class="btn btn_sz g-col_xs_12" value="Отправить заявку">
                    </li>
                </ul>

            </fieldset>
        </form>
    </div>
    <div id="popup-info-products" class="popup">
        <p>Отправьте заявку, <br> Получите подробную информацию <br> о продуктах Mobifitness</p>
        <form class="form-dark js-validated" action="?">
            <fieldset class="">
                <legend class="h1 g-d_b">Попробовать бесплатно</legend>
                <ul class="form-dark__list ">
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Имя*</span>
                            <input type="text" class="form-dark__field" name="name">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Телефон*</span>
                            <input type="tel" class="form-dark__field js-mask" name="phone">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">E-mail*</span>
                            <input type="email" class="form-dark__field js-mask" name="email">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Клуб / Компания</span>
                            <input type="text" class="form-dark__field js-mask" name="club"
                                   placeholder="Клуб / Компания">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <input type="submit" class="btn btn_sz g-col_xs_12" value="Отправить заявку">
                    </li>
                </ul>

            </fieldset>
        </form>
    </div>
    <div id="popup-request" class="popup">
        <p>Заполните форму, <br> и мы вышлем вам тестовый доступ.</p>
        <form class="form-dark js-validated" action="?">
            <fieldset class="">
                <legend class="h1 g-d_b">Попробовать бесплатно</legend>
                <ul class="form-dark__list ">
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Имя*</span>
                            <input type="text" class="form-dark__field" name="name">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Телефон*</span>
                            <input type="tel" class="form-dark__field js-mask" name="phone">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">E-mail*</span>
                            <input type="email" class="form-dark__field js-mask" name="email">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Клуб / Компания</span>
                            <input type="text" class="form-dark__field js-mask" name="club"
                                   placeholder="Клуб / Компания">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <input type="submit" class="btn btn_sz g-col_xs_12" value="Отправить заявку">
                    </li>
                </ul>

            </fieldset>
        </form>
    </div>
    <div id="popup-request-decor" class="popup">

        <form class="form-dark js-validated" action="?">
            <fieldset class="">
                <div class="popup-header-decor">
                    <div class="h1 g-d_b">Стать клиентом Mobifitness</div>
                    <p>Получите бесплатный тестовый доступ
                        и подробную информацию о продуктах Mobifitness.</p>
                </div>

                <ul class="form-dark__list form-line__list">
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Имя*</span>
                            <input type="text" class="form-dark__field" name="name">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Телефон*</span>
                            <input type="tel" class="form-dark__field js-mask" name="phone">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">E-mail*</span>
                            <input type="email" class="form-dark__field js-mask" name="email">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <label>
                            <span class="form-dark__label">Клуб / Компания</span>
                            <input type="text" class="form-dark__field js-mask" name="club"
                                   placeholder="Клуб / Компания">
                        </label>
                    </li>
                    <li class="form-line__item">
                        <input type="submit" class="btn btn_sz g-col_xs_12" value="Перезвоните мне">
                    </li>
                </ul>

            </fieldset>
        </form>
    </div>
</div>