<section class="product-target g-section-margin">
    <div class="g-grid">
        <h2 class="g-ta_c_xs">
            <?= $productTargetTitle ?>
        </h2>
        <ul class="product-target__list g-col-mw_xs_9">
            <li class="product-target__item product-target__item_1">фитнес-клубам</li>
            <li class="product-target__item product-target__item_2">школам танцев</li>
            <li class="product-target__item product-target__item_3">студиям йоги</li>
            <li class="product-target__item product-target__item_4">бассейнам</li>
            <li class="product-target__item product-target__item_5">спорткомплексам</li>
        </ul>
    </div>
</section>