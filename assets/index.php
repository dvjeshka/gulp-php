<?
//TODO: для титульной нужен доп класс:
$metaTitle = 'Программное обеспечение для управления фитнес-клубом - Mobifitness';
$metaDescription = 'Программное обеспечение (ПО) для автоматизации спортивного клуба. Система управления вашим фитнес-клубом, комплексное решение для управления';
$GLOBALS["bodyClass"] = "page_main";
include_once("_inc_header.php"); ?>

    <section class="index-main">
        <div class="g-grid g-pr">
            <div class="index-main__main">
                <h1 class="index-main__title">Система автоматизации управления фитнес-клубом</h1>
                <p class="index-main__text">Cделай свой клуб популярным, клиентов — лояльными, <br
                            class="g-hidden g-show_md">а продажи — в 2 раза
                    больше!</p>
                <div class="index-main__img-xs"></div>
                <form class="form-timer g-col-mw_md_4 js-validated" action="?">
                    <fieldset class="form-timer__inner">
                        <div class="g-row g-row_narrow-sz">
                            <div class="g-col g-col_md_6 g-mb_1_xs g-mb_md_0">
                                <input type="text" class="form-timer-input js-mask g-mb_2" placeholder="Имя"
                                       name="name">
                            </div>
                            <div class="g-col g-col_md_6 g-mb_1_xs g-mb_md_0">
                                <input type="email" class="form-timer-input js-mask g-mb_2" placeholder="Email"
                                       name="email">
                            </div>
                            <div class="g-col g-col_md_6 g-mb_1_xs g-mb_md_0">
                                <input type="tel" name="phone" class="form-timer-input js-mask g-mb_2"
                                       placeholder="Телефон">
                            </div>
                            <div class="g-col g-col_md_6 g-mb_1_xs g-mb_md_0">
                                <input type="text" class="form-timer-input js-mask g-mb_2" name="club"
                                       placeholder="Клуб / Компания">
                            </div>
                        </div>
                        <div class="timer">
                            <div class="timer__title">
                                Акция продлится еще
                            </div>
                            <div class="timer__list js-timer" data-timer="2018/12/28">

                            </div>
                        </div>


                        <input type="submit" class="form-timer-btn" value="Оставить заявку">
                    </fieldset>
                </form>
            </div>

            <div class="index-main__imgs">
                <div class="index-main__img index-main__book" data-px='{"d":1,"s":1000}'></div>
                <div class="index-main__img index-main__phone " data-px='{"d":1,"s":500}'></div>
                <div class="index-main__img index-main__card" data-px='{"d":-1,"s":250}'></div>

            </div>
        </div>
    </section>

    <section class="section-text g-section-margin_big">
        <div class="g-grid">
            <div class="g-row g-row_flex g-ai_c_md">
                <div class="g-col g-col_md_6  g-mb_0_md">
                    <p class="decor-circle decor-circle_text_big decor-circle_anim">
                        Как увеличить прибыль клубу? <br>
                        Все просто – сделайте <br>
                        счастливыми ваших клиентов <br>
                        благодаря Mobifitness! <br>
                        <a href="#popup-request-decor" class="decor-circle-go" data-colorbox>GO!</a>
                    </p>
                </div>
                <div class="g-col g-col_md_6">
                    <div class="g-hidden g-show_md">
                        <h2>Mobifitness — технологический лидер <br> на рынке фитнеса</h2>
                        <p>Благодаря Mobifitness каждый клуб может позволить себе собственную учетную систему, мобильное
                            приложение, качественное расписание занятий и другие продукты, которые оптимизируют рабочий
                            процесс сотрудников, повысят лояльность клиентов и продажи клуба..</p>

                        <p> Все наши продукты брендируются под фирменный стиль клуба, бесплатны для ваших клиентов, а
                            цены
                            подойдут даже небольшим студиям.</p>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="section-text section-text_club g-section-margin_big">
        <div class="g-grid">
            <div class="g-pr">
                <div class="g-col_md_5 g-pt_4_md">
                    <h2>Комплексное решение <br>
                        для управления вашим клубом
                    </h2>
                    <div class=" section-text__img-abs g-col_md_7 g-col-offset_md_6">
                        <!--   <img src="<? /*= $p . "img/screen_2.png" */ ?>"
                             alt="Комплексное решение для управления вашим клубом">-->
                        <div class="img-complex g-show_md g-hide_xs">
                            <div class="img-complex img-complex_0" data-px='{"d":0,"s":1200}'></div>
                            <div class="img-complex img-complex_1" data-px='{"d":0,"s":900}'></div>
                            <div class="img-complex img-complex_2" data-px='{"d":-1,"s":250}'></div>
                            <div class="img-complex img-complex_3" data-px='{"d":1,"s":250}'></div>
                        </div>
                    </div>
                    <p>Мы создали единую платформу, в которой есть все, <br class="g-hidden g-show_md">
                        что вам нужно:
                    </p>
                    <ul>
                        <li>Учетная система для управления клубом</li>
                        <li>Мобильное приложение для коммуникации с клиентами</li>
                        <li>Онлайн-запись для снижения нагрузки на рецепцию</li>
                        <li>Умная онлайн-касса для работы администратора</li>
                        <li>Личный кабинет и интернет магазин для увеличения продаж</li>
                    </ul>
                    <p>Все элементы системы интегрированы между собой и созданы для развития вашего бизнеса.
                    </p>


                </div>
            </div>
        </div>
    </section>


<? include("_inc_form_line.php"); ?>

    <section class="index-products">
        <div class="g-grid">
            <h2 class="g-ta_c_xs">Наши продукты</h2>
            <ul class="index-products__list g-row g-row_flex">
                <li class="index-products__item index-products__item_1 g-col g-col_md_6">
                    <a href="/crm" class="index-products__link g-link-js-parent">
                        <img class="index-products__img" src="<?= $p . "img/index_products/1.png" ?>"
                             alt="Облачная система автоматизации фитнес-клуб">
                        <div class="index-products__info">
                            <strong class="index-products__name">Учетная<br>система</strong>
                            <p class="index-products__text">Облачная система<br>
                                автоматизации<br>
                                фитнес-клуба</p>
                            <span class="g-tt_u g-link-js index-products__more">Подробнее</span>
                        </div>
                    </a>
                </li>
                <li class="index-products__item index-products__item_1 g-col g-col_md_6">
                    <a href="/app" class="index-products__link g-link-js-parent">
                        <img class="index-products__img" src="<?= $p . "img/index_products/2.png" ?>"
                             alt="Облачная система автоматизации фитнес-клуб">
                        <div class="index-products__info">
                            <strong class="index-products__name">Мобильное<br>приложение</strong>
                            <p class="index-products__text">Индивидуальное приложение <br>для клиентов <br>клуба</p>
                            <span class="g-tt_u g-link-js index-products__more">Подробнее</span>
                        </div>
                    </a>
                </li>
                <li class="index-products__item index-products__item_1 g-col g-col_md_6">
                    <a href="/timetable" class="index-products__link g-link-js-parent">
                        <img class="index-products__img" src="<?= $p . "img/index_products/3.png" ?>"
                             alt="Облачная система автоматизации фитнес-клуб">
                        <div class="index-products__info">
                            <strong class="index-products__name">Онлайн<br>запись</strong>
                            <p class="index-products__text">Виджет для сайта<br>
                                и соцсетей с широким<br>
                                функционалом</p>
                            <span class="g-tt_u g-link-js index-products__more">Подробнее</span>
                        </div>
                    </a>
                </li>
                <li class="index-products__item index-products__item_1 g-col g-col_md_6">
                    <a href="/other" class="index-products__link g-link-js-parent">
                        <img class="index-products__img" src="<?= $p . "img/index_products/4.png" ?>"
                             alt="Облачная система автоматизации фитнес-клуб">
                        <div class="index-products__info">
                            <strong class="index-products__name">Другие<br>продукты</strong>
                            <p class="index-products__text">Личный кабинет клиента,<br>
                                магазин для сайта<br>
                                и другое</p>
                            <span class="g-tt_u g-link-js index-products__more">Подробнее</span>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </section>

<? $productTargetTitle = "Кому будут интересны наши продукты";
include("_inc_product_target.php"); ?>



    <section class="form-line g-section-margin form-line_wave">
        <div class="g-grid">
            <div class="form-line__box">
                <div class="g-row g-row_flex g-ai_c_md">
                    <div class="g-col g-col_md_6">
                        <div class="form-line__text">
                            <p>
                                <small>6 полезных видеозаписей для вашего клуба!</small>
                            </p>
                            <p class="h2 h2_no_line g-clr_inh">Секреты успеха <br> ваших конкурентов!</p>
                            <p>Узнайте, как ваши конкуренты уже используют
                                мобильные приложения в своих клубах
                                для увеличения продаж и лояльности клиентов.
                            </p>
                        </div>
                    </div>
                    <form class="g-col form-dark g-col_md_6 js-validated" action="?">
                        <fieldset class="">
                            <ul class="form-line__list g-row">
                                <li class="form-line__item g-col g-col_md_6">
                                    <label>
                                        <span class="form-dark__label">Имя</span>
                                        <input type="text" class="form-dark__field" name="name">
                                    </label>
                                </li>
                                <li class="form-line__item g-col g-col_md_6">
                                    <label>
                                        <span class="form-dark__label">Телефон</span>
                                        <input type="tel" class="form-dark__field js-mask" name="phone">
                                    </label>
                                </li>
                                <li class="form-line__item g-col g-col_md_6">
                                    <label>
                                        <span class="form-dark__label">E-mail</span>
                                        <input type="email" class="form-dark__field js-mask" name="email">
                                    </label>
                                </li>
                                <li class="form-line__item g-col g-col_md_6">
                                    <span class="form-dark__label g-hide_xs g-show_md">&nbsp;</span>
                                    <input type="submit" class="btn btn_sz" value="Узнать секреты">
                                </li>
                            </ul>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>

<? $sliderClientsText = '
    <h2>С нами работают <br>уже более <span data-target="clubs">1600</span> фитнес-клубов <br class="g-hidden g-show_md">по всей России и СНГ</h2>
    <p>Среди клиентов Mobifitness можно найти не только крупные сети фитнес-клубов, но и небольшие студии танцев и йоги площадью не более 200 м<sup>2</sup>.</p>    
    ';
include_once("_inc_slider_clients.php"); ?>

<? include_once("_inc_reviews_items_index.php"); ?>

    <section class="statistic g-section-margin">
        <div class="g-grid">
            <div class="statistic__box g-ta_c_xs">
                <div class="g-col_md_10 g-mlr_auto_md">
                    <div class="g-row g-row_flex g-ai_b_md">
                        <div class="g-col g-col_md_4">
                            <h2 class="h2_lg">Статистика</h2>
                        </div>
                        <div class="g-col g-col_md_4">
                            <p>Фитнес-клубы выбирают Mobifitness</p>
                        </div>
                        <div class="g-col g-col_md_4"></div>
                    </div>
                    <div class="g-row">
                        <div class="g-col g-col_md_4">
                            <div class="statistic__item">
                                <div class="statistic__circle statistic__circle_anim">
                                    <div class="statistic__title" data-target='clients'>460</div>
                                    любимых клиентов
                                </div>
                                <div class="statistic__text">
                                    <div class="statistic__title" data-target='users'>305 000</div>
                                    <p>активных пользователей <br>
                                        мобильными приложениями</p>
                                </div>
                            </div>
                        </div>
                        <div class="g-col g-col_md_4">
                            <div class="statistic__item">
                                <div class="statistic__circle statistic__circle_anim">
                                    <div class="statistic__title" data-target='cities'>183</div>
                                    городов России и СНГ
                                </div>
                                <div class="statistic__text">
                                    <div class="statistic__title" data-target='applications'>1 964 000</div>
                                    <p>заявок из мобильных приложений <br>
                                        на заморозку и продление карты</p>
                                </div>
                            </div>
                        </div>
                        <div class="g-col g-col_md_4">
                            <div class="statistic__item">
                                <div class="statistic__circle statistic__circle_anim">
                                    <div class="statistic__title" data-target='clubs'>1500</div>
                                    фитнес-клубов
                                </div>
                                <div class="statistic__text">
                                    <div class="statistic__title" data-target='social'>3 103 000</div>
                                    <p>охват постов в соц.сетях, сделанных <br>
                                        членами фитнес–клубов из мобильных <br>
                                        приложений</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="statistic__footer">
                Благодаря бесплатным push-уведомлениям клуб экономит в среднем 30 000 рублей в месяц
            </div>
        </div>
    </section>

    <section class="video-list g-section-margin">
        <div class="g-grid">
            <h2>Видеозаписи выступлений Mobifitness</h2>
            <div class="video-list__scroll g-scroll-line g-scroll-line_i_md g-scroll-line_a_xs">

                <div class="video-list__list "
                     data-slick='{"respondTo": "min", "slidesToShow": 3,"slidesToScroll": 3,"dots": true, "fade": false,"arrows":true,"responsive": [{"breakpoint": 767,"settings":{"slidesToShow": 2, "slidesToScroll": 2,"arrows": false}},{"breakpoint": 567,"settings":{"slidesToShow": 1, "slidesToScroll": 1,"arrows": false}}]}'>
                    >
                    <div class="">
                            <a href="https://www.youtube.com/embed/DRCMs9rfHFM" class="video-list__item"
                               data-colorbox='{"iframe":true,"inline":false}'>
                                <div class="video-list__image"
                                     style="background-image:url(<?= $p . "img/video_list/video_list_item_1.png" ?>)">
                                    <div class="video-list__play icon-play"></div>
                                </div>
                                <time class="g-fs_sm" datetime="2017-08-26">26.08.2018</time>
                                <div class="video-list__name">Russian Fitness Fair</div>
                                <div class="video-list__descr">«Как изменится индустрия»</div>
                            </a>
                    </div>
                    <div class="">
                            <a href="https://www.youtube.com/embed/gRRzifmwjh4" class="video-list__item"
                               data-colorbox='{"iframe":true,"inline":false}'>
                                <div class="video-list__image"
                                     style="background-image:url(<?= $p . "img/video_list/video_list_item_2.png" ?>)">
                                    <div class="video-list__play icon-play"></div>
                                </div>
                                <time class="g-fs_sm" datetime="2017-09-27">27.09.2018</time>
                                <div class="video-list__name">День открытых дверей <br> в Школе Управления FPA</div>
                                <div class="video-list__descr">«Как сократить расходы клуба с помощью технологий»</div>
                            </a>
                    </div>
                    <div class="">
                            <a href="https://www.youtube.com/channel/UClm9mioWT2Q7hoEyiPCG6cg" target="_blank"
                               class="video-list__item">
                                <div class="video-list__image"
                                     style="background-image:url(<?= $p . "img/video_list/video_list_item_3.png" ?>)">
                                </div>
                            </a>
                    </div>
                </div>


            </div>

        </div>
    </section>

    <section class="text-box-img g-section-margin g-hidden g-show_md">
        <div class="g-grid">
            <div class="text-box-img__box text-box-img__box_man">
                <div class="text-box-img__img-wrap">
                    <div class="text-box-img__text">
                        <h2 class="h2_lg">Академия Mobifitness</h2>
                        <p>А вы уже участвуете в наших вебинарах
                            для клубов?</p>
                        <ul>
                            <li>Только эксперты</li>
                            <li>Записи вебинаров</li>
                            <li>Простой и доступный язык</li>
                            <li>Теория и практические рекомендации</li>
                            <li>Возможность бесплатной регистрации</li>
                            <li>Разбор кейсов реальных фитнес-клубов</li>
                            <li>Домашние задания и бесплатная обратная связь</li>

                        </ul>
                    </div>
                </div>

                <div class="text-box-img__btn">
                    <a href="https://academy.mobifitness.ru" class="btn btn-icon-arrow" target="_blank">
                        Перейти на сайт <br>
                        Академии Mobifitness
                    </a>
                </div>

            </div>
        </div>
    </section>

<? include_once("_inc_subs.php"); ?>



<? include_once("_inc_footer.php"); ?>