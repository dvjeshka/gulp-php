<ul class="header__soc soc">
    <li class="soc__item"><a href="https://vk.com/mobifitness_ru" class="soc__link icon-vk" target="_blank"
                             rel="nofollow">Вконтакте</a></li>
    <li class="soc__item"><a href="https://www.facebook.com/mobifitness.ru/" class="soc__link icon-fa" target="_blank"
                             rel="nofollow">Facebook</a></li>
    <li class="soc__item"><a href="https://www.instagram.com/mobifitness_ru/" class="soc__link icon-inst"
                             target="_blank" rel="nofollow">Instagram</a></li>
    <li class="soc__item"><a href="https://www.youtube.com/channel/UClm9mioWT2Q7hoEyiPCG6cg"
                             class="soc__link icon-youtube" target="_blank" rel="nofollow">Youtube</a></li>
    <li class="soc__item"><a href="https://blog.mobifitness.ru" class="soc__link icon-write" target="_blank">Blog.mobifitness</a>
    </li>
    <li class="soc__item g-hide_md g-d_b" hidden><a
                href="https://academy.mobifitness.ru/?utm_source=menu&utm_medium=mobifitness&utm_campaign=menumobifitness"
                class="soc__link icon-ak-soc" target="_blank">Blog.mobifitness</a></li>
</ul>