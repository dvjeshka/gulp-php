$(document).ready(function($) {
    var handlerUrl = 'https://new.mobifitness.ru/amo/handler.php';
    var utm = {utm_source:'', utm_medium:'', utm_campaign:'', utm_term:'', utm_content:''};
    var param = window.location.search; param = param.replace('?',''); param = param.split('&');
    for( var i=0; i<param.length; i++){
        var j = param[i].split('=');
        if(j[0] == 'utm_source') utm.utm_source = j[1];
        if(j[0] == 'utm_medium') utm.utm_medium = j[1];
        if(j[0] == 'utm_campaign') utm.utm_campaign = j[1];
        if(j[0] == 'utm_term') utm.utm_term = j[1];
        if(j[0] == 'utm_content') utm.utm_content = j[1];
    }

    var referrer = document.referrer.split('?')[0].replace('http://', '').replace('https://', '').split('/')[0];
    if(utm.utm_source == ''){
        if(referrer == ''){
            utm.utm_source = 'direct';
        }else if(referrer.indexOf('yandex') + 1 > 0){
            utm.utm_source = 'yandex';
        }else if(referrer.indexOf('google') + 1 > 0){
            utm.utm_source = 'google';
        }else{
            utm.utm_source = referrer;
            utm.utm_medium = 'referral';
        }
    }

    var current_date = new Date;
    var cookie_year = current_date.getFullYear ( );
    var cookie_month = current_date.getMonth ( );
    var cookie_day = current_date.getDate ( )+1;

    if(utm.utm_source!='') setCookie( "iTrack_utm_source", utm.utm_source, cookie_year, cookie_month, cookie_day, '/' );
    if(utm.utm_medium!='') setCookie( "iTrack_utm_medium", utm.utm_medium, cookie_year, cookie_month, cookie_day, '/' );
    if(utm.utm_campaign!='') setCookie( "iTrack_utm_campaign", utm.utm_campaign, cookie_year, cookie_month, cookie_day, '/' );
    if(utm.utm_term!='') setCookie( "iTrack_utm_term", utm.utm_term, cookie_year, cookie_month, cookie_day, '/' );
    if(utm.utm_content!='') setCookie( "iTrack_utm_content", utm.utm_content, cookie_year, cookie_month, cookie_day, '/' );
    if(referrer!='') setCookie( "iTrack_referrer", referrer, cookie_year, cookie_month, cookie_day, '/' );

    function getCookie(name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    function deleteCookie(name) {
        setCookie(name, "", {
            expires: -1
        })
    }

    function setCookie ( name, value, exp_y, exp_m, exp_d, path, domain, secure ){
        var cookie_string = name + "=" + escape ( value );

        if ( exp_y ){
            var expires = new Date ( exp_y, exp_m, exp_d );
            cookie_string += "; expires=" + expires.toGMTString();
        }

        if ( path ) cookie_string += "; path=" + escape ( path );
        if ( domain ) cookie_string += "; domain=" + escape ( domain );
        if ( secure ) cookie_string += "; secure";

        document.cookie = cookie_string;
    }

    var request = {
        name: '', phone: '', email: '', company: '', message: '', site: window.location.hostname + window.location.pathname,
        utm_source:'', utm_medium:'', utm_campaign:'', utm_term:'', utm_content:'', roistat: '', ref: ''
    };

    if(typeof getCookie('iTrack_utm_source')!='undefined') request.utm_source = getCookie('iTrack_utm_source');
    if(typeof getCookie('iTrack_utm_medium')!='undefined') request.utm_medium = getCookie('iTrack_utm_medium');
    if(typeof getCookie('iTrack_utm_campaign')!='undefined') request.utm_campaign = getCookie('iTrack_utm_campaign');
    if(typeof getCookie('iTrack_utm_term')!='undefined') request.utm_term = getCookie('iTrack_utm_term');
    if(typeof getCookie('iTrack_utm_content')!='undefined') request.utm_content = getCookie('iTrack_utm_content');
    if(typeof getCookie('roistat_visit')!='undefined') request.roistat = getCookie('roistat_visit');
    if(typeof getCookie('iTrack_referrer')!='undefined') request.ref = getCookie('iTrack_referrer');

    $('form').submit(function(){
        $send = true;

        if ($(this).hasClass('no-metrics')){
            return false;
        }

        request.name = $(this).find('input[name=name]').length > 0 ? $(this).find('input[name=name]').val() : '';
        request.phone = $(this).find('input[name=phone]').length > 0 ? $(this).find('input[name=phone]').val().replace(/[^\d;]/g, '') : '';
        request.email = $(this).find('input[name=email]').length > 0 ? $(this).find('input[name=email]').val() : '';
        request.company = $(this).find('input[name=club]').length > 0 ? $(this).find('input[name=club]').val().replace(/"/g, "'") : '';
        request.message = $(this).find('textarea[name=message]').length > 0 ? $(this).find('textarea[name=message]').val() : '';
        if(request.message != ''){
            request.message = request.message.replace(/"/g, "'");
        }
        if($(this).find('input[name=phone]').hasClass("input-error")){
            request.phone = '';
        }
        if($(this).find('input[name=email]').hasClass("input-error")){
            request.email = '';
        }
        if( request.phone == '' /*|| request.email == '' */){
            $send = false;
        }

        if($send) {
            $.ajax({
                url: handlerUrl,
                data: request,
                type: 'POST',
                success: function(text){
                    console.log(text);
                },
                error: function(text) {
                    console.log(text);
                }
            });
        }
    });
});