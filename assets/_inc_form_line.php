<section class="form-line g-section-margin">
    <div class="g-grid">
        <div class="form-line__box">
            <div class="g-row g-row_flex g-ai_c_md">
                <div class="g-col g-col_md_6">


                    <? if ($formLineVariant == 3) { ?>
                        <div class="form-line__text">
                            <p>
                                <small>Бесплатная разработка и дизайн!</small>
                            </p>
                            <p class="h2 h2_no_line g-clr_inh">Оставьте заявку и получите
                                специальное предложение <br>
                                для своего клуба</p>
                            <p>C нами работает уже более <span data-target="clubs">1500</span> клубов,
                                присоединяйтесь и вы!
                            </p>
                        </div>

                    <? } else if ($formLineVariant == 2) { ?>
                        <div class="g-mb_2_xs g-mb_0_md">
                            <p class="h2 h2_no_line g-clr_inh">Получите единый инструмент<br>
                                управления вашим бизнесом</p>
                            <p>Оставьте заявку и получите подробную <br>
                                консультацию наших специалистов.</p>
                        </div>

                    <? } else if ($formLineVariant == 1) { ?>
                        <div class="form-line__text">
                            <p>
                                <small>Бесплатная разработка и дизайн!</small>
                            </p>
                            <p class="h2 h2_no_line g-clr_inh">Онлайн-запись на сайт в подарок!</p>
                            <p>C нами работает уже более <span data-target="clubs">1600</span> клубов,
                                присоединяйтесь и вы!
                            </p>
                        </div>

                    <? } else { ?>
                        <div class="form-line__text">
                            <p>
                                <small>При покупке мобильного приложения в декабре!</small>
                            </p>
                            <p class="h2 h2_no_line g-clr_inh">Онлайн-запись <br class="g-hide_md"> на сайт <br
                                        class="g-hidden g-show_md"> в подарок!</p>
                            <p>Теперь ваши клиенты смогут записаться на групповые
                                и персональные занятия без участия рецепции
                                на сайте и в соцсетях вашего клуба!
                            </p>
                        </div>
                    <? } ?>
                </div>
                <form class="g-col form-dark g-col_md_6 js-validated" action="?">
                    <fieldset class="">
                        <ul class="form-line__list g-row">
                            <li class="form-line__item g-col g-col_md_6">
                                <label>
                                    <span class="form-dark__label">Имя*</span>
                                    <input type="text" class="form-dark__field" name="name">
                                </label>
                            </li>
                            <li class="form-line__item g-col g-col_md_6">
                                <label>
                                    <span class="form-dark__label">Телефон*</span>
                                    <input type="tel" name="phone" class="form-dark__field js-mask">
                                </label>
                            </li>
                            <li class="form-line__item g-col g-col_md_6">
                                <label>
                                    <span class="form-dark__label">E-mail*</span>
                                    <input type="email" name="email" class="form-dark__field js-mask">
                                </label>
                            </li>
                            <li class="form-line__item g-col g-col_md_6">
                                <span class="form-dark__label g-hide_xs g-show_md">&nbsp;</span>

                                <? if ($formLineVariant == 3) { ?>
                                    <input type="submit" class="btn btn_sz" value="Получить предложение">

                                <? } else if ($formLineVariant == 2) { ?>
                                    <input type="submit" class="btn btn_sz" value="Оставить заявку">

                                <? } else if ($formLineVariant == 1) { ?>
                                    <input type="submit" class="btn btn_sz" value="Получить расписание">

                                <? } else { ?>
                                    <input type="submit" class="btn btn_sz" value="Получить подарок">
                                <? } ?>
                            </li>
                        </ul>
                        <input type="hidden" name="group" value="free-schedule">
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</section>