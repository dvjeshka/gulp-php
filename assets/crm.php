<?
$metaTitle = 'Автоматизация фитнес-клуба, CRM система для управления фитнес-клубом';
$metaDescription = 'Программное обеспечение для автоматизации работы фитнес-клуб. Комплексное решение для управления вашим спортивным клубом избавит вас и сотрудников от рутины - Mobifitness';
include_once("_inc_header.php"); ?>


    <section class="box-main box-main_erp g-section-margin">
        <div class="g-grid">
            <div class="box-main__box">
                <div class="g-row">
                    <div class="g-col g-col_md_8">
                        <div class="box-main__text">

                            <h1 class="h1_lg">Система автоматизации <br class="g-hidden g-show_md"> и учета </h1>

                            <p class="box-main__descr">Универсальный инструмент для управления вашим клубом <br>
                            </p>

                            <p class="g-d_f g-ai_c_xs box-main__control"><a href="#popup-details"
                                                                            class="btn btn_plr g-mr_2_xs"
                                                          data-colorbox>Попробовать бесплатно</a><a
                                        href="#popup-consultation" class="g-link-js g-fs_sm g-tt_u_xs"
                                        data-colorbox>Получить
                                    консультацию</a></p>

                            <!--<div class="box-main__img" data-px='{"d":1,"s":1500}'>
                            </div>-->

                            <div class="box-main__img-anims">
                                <div class="box-main__img-anim box-main__img-anim_0" data-px='{"d":0,"s":1200}'></div>
                                <div class="box-main__img-anim box-main__img-anim_1" data-px='{"d":0,"s":900}'></div>
                                <div class="box-main__img-anim box-main__img-anim_2" data-px='{"d":1,"s":250}'></div>
                                <div class="box-main__img-anim box-main__img-anim_3" data-px='{"d":1,"s":250}'></div>
                            </div>

                            <div class="box-main__circle-decor" data-px='{"d":-1,"s":250}'>
                                Бета-версия
                                <div class="box-main__circle-decor-title">-40%</div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-text section-text_reshen g-section-margin_big">
        <div class="g-grid">
            <div class="g-pr">
                <div class="g-col_md_5 g-pt_4_md g-col-offset_md_7">
                    <h2>Избавьте себя и сотрудников <br class="g-hidden g-show_md">
                        от рутины</h2>
                    <div class=" section-text__img-abs g-col_md_8 ">
                        <!--   <img src="<? /*= $p . "img/page_erp/reshen.png" */ ?>"
                             alt="Избавьте себя и сотрудников от рутины">-->
                        <div class="img-complex">
                            <div class="img-complex img-complex_0" data-px='{"d":1,"s":1200}'></div>
                            <div class="img-complex img-complex_1" data-px='{"d":-1,"s":900}'></div>
                        </div>
                    </div>
                    <p>Лучшее решение для автоматизации бизнеса:</p>
                    <ul>
                        <li>Быстрое внедрение</li>
                        <li>Бесплатная техподдержка</li>
                        <li>Оптимизация работы персонала</li>
                        <li>Удобный и понятный интерфейс</li>
                        <li>Автоматическое составление отчетов</li>
                        <li>Качественная коммуникация с клиентом</li>
                        <li>Управление маркетинговыми кампаниями</li>
                    </ul>


                </div>
            </div>
        </div>
    </section>

<? $productTargetTitle = "Кому необходима наша учетная система";
include("_inc_product_target.php"); ?>


    <section class="slider-func g-section-margin">
        <div class="g-grid">
            <h2 class="g-ta_c_xs">Функционал</h2>
            <div class="slider-func__slider"
                 data-slick='{"respondTo": "min", "adaptiveHeight":false,"slidesToShow": 3, "slidesToScroll": 3,"dots": true, "fade": false,"arrows":true,"responsive": [{"breakpoint": 767,"settings":{"slidesToShow": 2, "slidesToScroll": 2,"arrows": true,"dots": true}},{"breakpoint": 639,"settings":{"slidesToShow": 1, "slidesToScroll": 1,"arrows": true,"dots": true}}]}'>
                <div>
                    <div class="slider-func__item">
                        <div class="slider-func__circle">
                            <div class="slider-func__icon slider-func__icon_1"></div>
                        </div>
                        <h3 class="slider-func__name g-clr_4">Для отдела продаж</h3>
                        <div class="slider-func__list-wrap">
                            <ul>
                                <li>ведите базу клиентов в CRM</li>
                                <li>выстраивайте цепочки автопродаж</li>
                                <li>работайте на удержание <br> клиентов</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="slider-func__item">
                        <div class="slider-func__circle">
                            <div class="slider-func__icon slider-func__icon_2"></div>
                        </div>
                        <h3 class="slider-func__name g-clr_4">Для директора</h3>
                        <div class="slider-func__list-wrap">
                            <ul>
                                <li>пользуйтесь дашбордом руководителя</li>
                                <li>ведите финансовую отчетность</li>
                                <li>отслеживайте аналитику работы клуба</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="slider-func__item">
                        <div class="slider-func__circle">
                            <div class="slider-func__icon slider-func__icon_3"></div>
                        </div>
                        <h3 class="slider-func__name g-clr_4">Для маркетолога</h3>
                        <div class="slider-func__list-wrap">
                            <ul>
                                <li>отслеживайте сквозную аналитику</li>
                                <li>отправлеяйте e-mail, SMS и push-уведомления клиентам</li>
                                <li>проводите NPS опросы</li>
                                <li>настраивайте тригерные рассылки</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="slider-func__item">
                        <div class="slider-func__circle">
                            <div class="slider-func__icon slider-func__icon_4"></div>
                        </div>
                        <h3 class="slider-func__name g-clr_4">Для тренера</h3>
                        <div class="slider-func__list-wrap">
                            <ul>
                                <li> отслеживайте запись
                                    на тренировки
                                </li>
                                <li>управляйте своим графиком</li>
                                <li> рассылайте уведомления об
                                    отмене или переносе занятий
                                </li>
                                <li> отслеживайте расчет
                                    заработной платы
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="slider-func__item">
                        <div class="slider-func__circle">
                            <div class="slider-func__icon slider-func__icon_5 "></div>
                        </div>
                        <h3 class="slider-func__name g-clr_4">Для рецепции</h3>
                        <div class="slider-func__list-wrap">
                            <ul>
                                <li> управляйте арендой залов</li>
                                <li>ведите расписание</li>
                                <li> отмечайте визиты
                                    в одно касание
                                </li>
                                <li>рассылайте уведомления
                                    клиентам
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="slider-func__item">
                        <div class="slider-func__circle">
                            <div class="slider-func__icon slider-func__icon_2"></div>
                        </div>
                        <h3 class="slider-func__name g-clr_4">Для клиентов</h3>
                        <div class="slider-func__list-wrap">
                            <ul>
                                <li>пользуйтесь мобильным приложением</li>
                                <li>записывайтесь и оплачивайте занятия онлайн</li>
                                <li>проходите в клуб без использования карты</li>
                                <li>участвуйте в программе лояльности</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<? $formLineVariant = 2;
include("_inc_form_line.php"); ?>
        <!--       <? /* function sectionFormLine($variant) {include("_inc_form_line.php");}*/ ?>

    --><? /*  sectionFormLine(2)*/ ?>


    <section class="section-text section-text_evator section-text_img-abs_left g-section-margin_big">
        <div class="g-grid g-grid_lg">
            <div class="g-pr">

                <div class="g-col_md_5 section-text__text ">
                    <h2>Скоро! <br> Терминал администратора <br>
                        с онлайн-кассой</h2>
                    <div class=" section-text__img-abs g-col_md_5">
                        <div class="slider-screen-img slider-screen-img_evator">
                            <div class="slider-screen"
                                 data-slick='{"dots":false,"slidesToShow": 1, "slidesToScroll": 1,"dots": false, "fade": false,"arrows":true,"responsive": [{"breakpoint": 767,"settings":{"slidesToShow": 1, "slidesToScroll": 1,"arrows": false}}]}'>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_erp/slider-evator/1.png" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_erp/slider-evator/2.png" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_erp/slider-evator/3.png" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_erp/slider-evator/4.png" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                            </div>

                        </div>


                    </div>
                    <p>
                        Специально для рецепции мы разработали приложение
                        для терминала администратора. Теперь администратор
                        клуба легко может:
                    </p>
                    <br>
                    <ul data-dots-listener='{"parent":".section-text","target":".slider-screen"}'>
                        <li>продавать товары и услуги в одно касание, пробивать чеки</li>
                        <li>отмечать визиты и записывать клиентов на занятия</li>
                        <li>просматривать расписание групповых тренировок</li>
                        <li>получать полную информацию по абонементу посетителя</li>
                    </ul>
                </div>

            </div>
        </div>
    </section>


    <section class="statistic g-section-margin">
        <div class="g-grid">
            <div class="statistic__box g-ta_c_xs">
                <div class="g-col_md_10 g-mlr_auto_md">
                    <div class="g-row g-row_flex g-ai_b_md">
                        <div class="g-col g-col_md_4">
                            <h2 class="h2_lg">Статистика</h2>
                        </div>
                        <div class="g-col g-col_md_4">
                            <p>Фитнес-клубы выбирают Mobifitness</p>
                        </div>
                        <div class="g-col g-col_md_4"></div>
                    </div>
                    <div class="g-row">
                        <div class="g-col g-col_md_4">
                            <div class="statistic__item">
                                <div class="statistic__circle">
                                    <div class="statistic__title" data-target='clients'>460</div>
                                    любимых клиентов
                                </div>

                            </div>
                        </div>
                        <div class="g-col g-col_md_4">
                            <div class="statistic__item">
                                <div class="statistic__circle">
                                    <div class="statistic__title" data-target='cities'>183</div>
                                    городов России и СНГ
                                </div>

                            </div>
                        </div>
                        <div class="g-col g-col_md_4">
                            <div class="statistic__item">
                                <div class="statistic__circle">
                                    <div class="statistic__title" data-target='clubs'>1500</div>
                                    фитнес-клубов
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="g-mt_6_xs g-ta_c_xs">
            <a href="#popup-request-decor" class="btn btn-icon btn-icon_mod" data-colorbox>
                Стать клиентом Mobifitness
            </a>
        </div>
    </section>


    <section class="advantages">
        <div class="g-grid">
            <h2 class="g-ta_c_xs">Какие преимущества?</h2>
            <div class="g-row g-mt_7_md">
                <div class="g-col g-col_md_6">
                    <ul>
                        <li>Широкий функционал</li>
                        <li>Выгодные тарифы. Низкая комиссия</li>
                        <li>Удобный и понятный интерфейс</li>
                        <li>Бесплатное и моментальное внедрение</li>
                        <li>Быстрая и профессиональная техническая поддержка</li>
                        <li>Специально разработанные приложения для ваших клиентов, администраторов и инструкторов</li>
                    </ul>

                </div>
                <div class="g-col g-col_md_6">
                    <ul>
                        <li>Бесплатный тестовый период</li>
                        <li>Система не требующая обучения</li>
                        <li>Облачная система, доступная с любого устройства</li>
                        <li>Готовое решение онлайн-кассы с приложением администратора</li>
                        <li>Полная сквозная интеграция всех инструментов управления клубом</li>
                        <li>Защита данных клуба и персональных данных клиентов</li>
                    </ul>

                </div>
            </div>
        </div>
    </section>

    <section class="works">
        <div class="g-grid">
            <h2 class="g-ta_c_xs">Как это работает?</h2>
            <p class="g-ta_c_xs">Начать пользоваться Mobifitness очень просто!</p>
            <div class="works__scroll">
                <ul class="works__list">
                    <li class="works__item works__item_1">Оставьте заявку</li>
                    <li class="works__item works__item_2">Получите логин и пароль</li>
                    <li class="works__item works__item_3">Зайдите <br> в административную панель</li>
                    <li class="works__item works__item_4">Пользуйтесь системой</li>

                </ul>
            </div>

        </div>
    </section>

<? $formBoxClass = "form-box_mac2";
include_once("_inc_form_box.php"); ?>


    <section class="rates rates_erp g-section-margin">
        <div class="g-grid">
            <h2 class="g-ta_c_xs">Тарифы</h2>
            <form action="?" class="rates__form">
                <div class="g-row g-row_flex g-ai_c_md">
                    <div class="g-col g-col_md_3">
                        <div class="rates__text-decor">
                            <div class="rates__text-decor-title">Бета-версия</div>
                            <div class="rates__text-decor-numb">-40%</div>
                            <div class="rates__text-decor-text">
                                на учетную систему <br>
                                Mobifitnes<br>
                            </div>
                            <a href="#popup-sale" class="rates__text-decor-link g-link-js"
                               data-colorbox='{"className":"popup-wrap popup-wrap_sale"}'>Почему дешевле?</a>
                        </div>
                    </div>
                    <div class="g-col g-col_md_9">
                        <ul class="rates__list">
                            <li class="rates__item rates__item_1">
                                <div class="rates__box">
                                    <div class="rates__label">Тариф</div>
                                    <h3 class="rates__name">Начальный</h3>
                                    <div class="rates__soc">Учетная система <br>
                                        с минимумом функций
                                    </div>
                                    <div class="rates__price">
                                        <fieldset class="rates__select js-rates__select">
                                            <s class="rates__sale rates__select-sale-parent"> <span
                                                        class="rates__sale-name">3 190</span>руб./мес.</span></s>
                                            <strong class="rates__select-name g-link-js">
                                                <span class="h1">1 190</span> руб./мес. <span
                                                        class="rates__select-arrow"><span
                                                            class="g-icon-down"></span></span>
                                            </strong>

                                            <ul class="rates__select-list">
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <s class="rates__sale rates__sale_select"> <span
                                                                    class="rates__sale-name">3 190</span>руб./мес.</span>
                                                        </s>
                                                        <input class="chr__input" type="radio" name="rates__item_1"
                                                               checked
                                                               value="1 год">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="48">1 190</span> руб. / месяц
                                                        <span class="rates__label">При оплате за год</span>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <s class="rates__sale rates__sale_select">
                                                            <span class="rates__sale-name">3 590</span>руб./мес.
                                                        </s>
                                                        <input class="chr__input" type="radio" name="rates__item_1"
                                                               value="6 месяцев">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="15">2 200</span> руб. / месяц
                                                        <span class="rates__label">При оплате за полгода</span>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <s class="rates__sale rates__sale_select"> <span
                                                                    class="rates__sale-name">4 190</span>руб./мес.</s>
                                                        <input class="chr__input" type="radio" name="rates__item_1"
                                                               value="3 месяца">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="0">2 500</span> руб. / месяц
                                                        <span class="rates__label">При оплате за 3 месяца</span>
                                                    </span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </fieldset>
                                    </div>
                                    <div class="rates__saving">
                                        при оплате за год <br> экономия <span class="rates__saving-numb">48</span>%
                                    </div>
                                </div>
                                <div class="rates__btn-wrap">
                                    <a href="#popup-request-rate" class="rates__btn btn btn_plr" data-colorbox>Оплатить
                                        тариф</a>
                                </div>

                                <div class="rates__adaptive">
                                    <div class="btn-arrow rates__btn-arrow js-rates__btn-arrow"></div>
                                </div>
                            </li>
                            <li class="rates__item rates__item_2">
                                <div class="rates__box">
                                    <div class="rates__label">Тариф</div>
                                    <h3 class="rates__name">Профессиональный</h3>
                                    <div class="rates__soc">Учетная система <br>
                                        с максимальным функционалом
                                    </div>
                                    <div class="rates__price">
                                        <fieldset class="rates__select js-rates__select">
                                            <s class="rates__sale rates__select-sale-parent"> <span
                                                        class="rates__sale-name">5 490</span>
                                                руб./мес.</s>
                                            <strong class="rates__select-name g-link-js"
                                            >
                                                <span class="h1">3 300</span> руб./мес. <span
                                                        class="rates__select-arrow"><span
                                                            class="g-icon-down"></span>   </span></strong>

                                            <ul class="rates__select-list">
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <s class="rates__sale rates__sale_select"> <span
                                                                    class="rates__sale-name">5 490</span>
                                                            руб./мес.</s>
                                                        <input class="chr__input" type="radio" name="rates__item_2"
                                                               checked
                                                               value="1 год">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="22">3 300</span> руб. / месяц
                                                        <span class="rates__label">При оплате за год</span>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <s class="rates__sale rates__sale_select"> <span
                                                                    class="rates__sale-name">5 990</span>
                                                            руб./мес.</s>
                                                        <input class="chr__input" type="radio" name="rates__item_2"
                                                               value="6 месяцев">
                                                        <span class="chr__text">
                                                          <span class="h1" data-percent="12">3 600</span> руб. / месяц
                                                        <span class="rates__label">При оплате за полгода</span>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <s class="rates__sale rates__sale_select"> <span
                                                                    class="rates__sale-name">6 990</span>
                                                            руб./мес.</s>
                                                        <input class="chr__input" type="radio" name="rates__item_2"
                                                               value="3 месяца">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="0">4 200</span> руб. / месяц
                                                        <span class="rates__label">При оплате за 3 месяца</span>
                                                    </span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </fieldset>
                                    </div>
                                    <div class="rates__saving">
                                        при оплате за год <br> экономия <span class="rates__saving-numb">22</span>%
                                    </div>
                                </div>
                                <div class="rates__btn-wrap">
                                    <a href="#popup-request-rate" class="rates__btn btn btn_plr" data-colorbox
                                       data-block="Тариф Начальный">Оплатить
                                        тариф</a>
                                </div>
                                <div class="rates__adaptive">
                                    <div class="btn-arrow rates__btn-arrow js-rates__btn-arrow"></div>
                                </div>
                            </li>
                            <li class="rates__item rates__item_3">
                                <div class="rates__box">
                                    <div class="rates__label">Тариф</div>
                                    <h3 class="rates__name">Все включено</h3>
                                    <div class="rates__soc">Полный пакет <br>
                                        продуктов Mobifitness
                                    </div>
                                    <div class="rates__price">
                                        <fieldset class="rates__select js-rates__select">
                                            <s class="rates__sale rates__select-sale-parent"> <span
                                                        class="rates__sale-name">10980</span>руб./мес.</s>
                                            <strong class="rates__select-name g-link-js">
                                                <span class="h1">8 790</span> руб./мес.
                                                <span class="rates__select-arrow"><span
                                                            class="g-icon-down"></span></span>
                                            </strong>

                                            <ul class="rates__select-list">
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <s class="rates__sale rates__sale_select"> <span
                                                                    class="rates__sale-name">10980 </span>
                                                            руб./мес.</s>
                                                        <input class="chr__input" type="radio" name="rates__item_3"
                                                               checked
                                                               value="1 год">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="22">8 790</span> руб. / месяц
                                                        <span class="rates__label">При оплате за год</span>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <s class="rates__sale rates__sale_select"> <span
                                                                    class="rates__sale-name">11980 </span>
                                                            руб./мес.</s>
                                                        <input class="chr__input" type="radio" name="rates__item_3"
                                                               value="6 месяцев">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="15">9 590</span> руб. / месяц
                                                        <span class="rates__label">При оплате за полгода</span>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <s class="rates__sale rates__sale_select"> <span
                                                                    class="rates__sale-name">13980  </span>
                                                            руб./мес.</s>
                                                        <input class="chr__input" type="radio" name="rates__item_3"
                                                               value="3 месяца">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="0">11 190</span> руб. / месяц
                                                        <span class="rates__label">При оплате за 3 месяца</span>
                                                    </span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </fieldset>
                                    </div>
                                    <div class="rates__saving">
                                        при оплате за год <br> экономия <span class="rates__saving-numb">22</span>%
                                    </div>
                                </div>
                                <div class="rates__btn-wrap">
                                    <a href="#popup-request-rate" class="rates__btn btn btn_plr" data-colorbox>Оплатить
                                        тариф</a>
                                </div>
                                <div class="rates__adaptive">
                                    <div class="btn-arrow rates__btn-arrow js-rates__btn-arrow"></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
            <div class="rates-table">
                <ul class="rates-table__table">
                    <li class="rates-table__item">
                        <h4 class="rates-table__name rates-table__row">Учет клиентов</h4>
                        <div class="rates-table__rows">
                            <div class="rates-table__row">
                                <p class="rates-table__text">
                                    Удобная работа с карточками клиентов <br>
                                    (вся информация об активных услугах, история
                                    посещений, взаиморасчетах в одном месте)
                                </p>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                <div class="rates-table__rows rates-table__rows_children">
                                    <div class="rates-table__row">
                                        <p class="rates-table__text">
                                            Удобная работа с карточками клиентов <br>
                                            (вся информация об активных услугах, история
                                            посещений, взаиморасчетах в одном месте)
                                        </p>
                                        <div class="rates-table__point rates-table__point_1"></div>
                                        <div class="rates-table__point rates-table__point_2"></div>
                                        <div class="rates-table__point rates-table__point_3"></div>
                                    </div>
                                    <div class="rates-table__row">
                                        <p class="rates-table__text">
                                            Удобная работа с карточками клиентов <br>
                                            (вся информация об активных услугах, история
                                            посещений, взаиморасчетах в одном месте)
                                        </p>
                                        <div class="rates-table__point rates-table__point_1"></div>
                                        <div class="rates-table__point rates-table__point_2"></div>
                                        <div class="rates-table__point rates-table__point_3"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Электронные клубные карты </p>
                                <div class="rates-table__point rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Управление членствами и пакетами услуг</p>
                                <div class="rates-table__point rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                        </div>
                    </li>
                    <li class="rates-table__item">
                        <h4 class="rates-table__name rates-table__row">Работа с расписанием и визитами</h4>
                        <div class="rates-table__rows">
                            <div class="rates-table__row">
                                <p class="rates-table__text">Удобный планировщик групповых <br>
                                    и персональных занятий</p>
                                <div class="rates-table__point rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Учет посещений клиентов </p>
                                <div class="rates-table__point rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Гибкий планировщик графика работы сотрудников</p>
                                <div class="rates-table__point rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Напоминания о записях тренерам и клиентам</p>
                                <div class="rates-table__point rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Статистика и история посещений</p>
                                <div class="rates-table__point rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                        </div>
                    </li>
                    <li class="rates-table__item">
                        <h4 class="rates-table__name rates-table__row">Маркетинг</h4>
                        <div class="rates-table__rows">
                            <div class="rates-table__row">
                                <p class="rates-table__text">Сегментирование клиентов по стадии жизненного цикла</p>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Автоматическое поздравление с днем рождения</p>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                        </div>
                    </li>
                    <li class="rates-table__item">
                        <h4 class="rates-table__name rates-table__row">Учет финансов</h4>
                        <div class="rates-table__rows">
                            <div class="rates-table__row">
                                <p class="rates-table__text">Касса</p>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Лицевые счета клиентов (депозиты)</p>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Интеграция с облачными онлайн-кассами</p>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                        </div>
                    </li>

                    <li class="rates-table__item">
                        <h4 class="rates-table__name rates-table__row">Интеграции</h4>
                        <div class="rates-table__rows">
                            <div class="rates-table__row">
                                <p class="rates-table__text">Виджет записи для amoCRM</p>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                        </div>
                    </li>
                    <li class="rates-table__item">
                        <h4 class="rates-table__name rates-table__row">Подключаемое оборудование</h4>
                        <div class="rates-table__rows">
                            <div class="rates-table__row">
                                <p class="rates-table__text">Web-камера</p>
                                <div class="rates-table__point rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Облачная онлайн-касса</p>
                                <div class="rates-table__point rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Сканер штрих и магнитных карт</p>
                                <div class="rates-table__point rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                        </div>
                    </li>
                    <li class="rates-table__item">
                        <h4 class="rates-table__name rates-table__row">Сервисы для сайта и соцсетей и онлайн
                            платежи</h4>
                        <div class="rates-table__rows">
                            <div class="rates-table__row">
                                <p class="rates-table__text">Онлайн расписание и запись на сайте и в соц сетях</p>

                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Виджет личного кабинета на сайте</p>

                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Прием платежей на сайте, в мобильном приложении или
                                    терминале в клубе</p>

                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                        </div>
                    </li>
                    <li class="rates-table__item">
                        <h4 class="rates-table__name rates-table__row">Мобильное приложение для клиентов</h4>
                        <div class="rates-table__rows">
                            <div class="rates-table__row">
                                <p class="rates-table__text">iOS</p>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row">
                                <p class="rates-table__text">Android</p>
                                <div class="rates-table__point rates-table__point_3"></div>
                            </div>
                        </div>
                    </li>


                    <li class="rates-table__item">
                        <h4 class="rates-table__name rates-table__row">Скоро!</h4>
                        <div class="rates-table__rows">
                            <div class="rates-table__row rates-table__row_clock">
                                <p class="rates-table__text">P&amp;L отчет</p>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row rates-table__row_clock">
                                <p class="rates-table__text">Отчеты по клиентам (взаиморасчеты с клиентами,
                                    обязательства по членствам и пакетам услуг,
                                    статистика посещений, посещаемость занятий
                                    и другие отчеты)</p>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row rates-table__row_clock">
                                <p class="rates-table__text">Аналитика финансовых результатов</p>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row rates-table__row_clock">
                                <p class="rates-table__text">Анализ работы сотрудников </p>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row rates-table__row_clock">
                                <p class="rates-table__text">Система скидок (срок действия скидки, скидки для
                                    сегментов клиентов)</p>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row rates-table__row_clock">
                                <p class="rates-table__text">Договоры, печать договоров и контрактов</p>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row rates-table__row_clock">
                                <p class="rates-table__text">Бонусные счета клиентов, программа лояльности,
                                    сертификаты</p>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row rates-table__row_clock">
                                <p class="rates-table__text">Конструктор скидок</p>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row rates-table__row_clock">
                                <p class="rates-table__text">Аренда залов</p>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row rates-table__row_clock">
                                <p class="rates-table__text">Лист ожидания записавшихся</p>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row rates-table__row_clock">
                                <p class="rates-table__text">Личный кабинет тренера (управление записью, отметка
                                    визита)</p>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                            </div>
                            <div class="rates-table__row rates-table__row_clock">
                                <p class="rates-table__text">Оценка тренировок</p>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                            </div>

                        </div>
                    </li>


                </ul>
                <div class="g-row g-row_flex rates-table__btns">
                    <p class="g-col g-col_md_3 g-ta_c_xs">
                    </p>
                    <p class="g-col g-col_md_3 g-ta_c_xs">
                        <a href="#popup-request-rate" onclick="func.jsSetTariff('Старт','rates__item_1')"
                           class="rates-table__btn btn btn_plr" data-colorbox="">тариф
                            «Начальный»</a>
                        <small class="rates-table__small">Неделя в подарок</small>
                    </p>
                    <p class="g-col g-col_md_3 g-ta_c_xs rates-table__col-btn-main">
                        <a href="#popup-request-rate" onclick="func.jsSetTariff('Базовый','rates__item_2')"
                           class="rates-table__btn btn btn-gr btn_plr" data-colorbox="">Тариф
                            «Профессиональный»</a>
                        <small class="rates-table__small">Две недели в подарок</small>
                    </p>
                    <p class="g-col g-col_md_3 g-ta_c_xs">
                        <a href="#popup-request-rate" onclick="func.jsSetTariff('Все включено','rates__item_3')"
                           class="rates-table__btn btn " data-colorbox="">тариф «все
                            включено»</a>
                        <small class="rates-table__small">Месяц в подарок</small>
                    </p>
                </div>

            </div>
        </div>
    </section>


<? $sliderClientsText = '
                <h2>Клиенты Mobifitness </h2>
                <p>У нас уже более <span data-target="clients">500</span> клиентов по России и СНГ.<br>Среди них, как небольшие одиночные клубы площадью от 200 квадратных метров, так и крупные федеральные сети фитнес-клубов.</p>    
            ';
include_once("_inc_slider_clients.php"); ?>


<? include_once("_inc_footer.php"); ?>