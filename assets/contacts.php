<?
$metaTitle = 'Контакты – Mobifitness';
$metaDescription = 'Контакты - Автоматизируйте работу своего фитнес-клуба уже сейчас – Mobifitness ';
include_once("_inc_header.php"); ?>

        <h1 class="g-hidden">Контакты</h1>
        <section class="page__contacts-info g-section-margin">
            <div class="g-grid">
                <div class="g-row">
                    <div class="g-col g-col_md_6 g-mb_0_md g-mb_4_xs">
                        <h2 class="h2_no_line">Офис в Москве, <br>
                            клиенты по всему миру!</h2>
                        <div class="g-row">
                            <div class="g-col g-col_md_6 g-mb_0_md g-mb_2_xs">
                                <h3 class="">Контакты:</h3>
                                <ul class="g-clr_4">
                                    <li><a href="mailto:hello@mobifitness.ru" class="g-link-js">hello@mobifitness.ru</a>
                                    </li>
                                    <li><a href="tel:+74951339532" class="g-link-js">+7 (495) 133-95-32</a></li>
                                    <li class="g-mr_2_m_md"><p>г. Москва, Барабанный переулок 4/4</p></li>
                                </ul>
                            </div>
                            <div class="g-col g-col_md_6">
                                <h3 class="g-hidden g-show_md">Техническая поддержка:
                                </h3>
                                <div class="h3 g-d_b g-hide_md" hidden>Техническая поддержка:</div>
                                <p><a href="tel:+74951339532" class="g-link-js g-clr_4">+7 (495) 133-95-32 (доб.
                                        200)</a>
                                    <br>
                                    <a href="mailto:s@mobifitness.ru" class="g-link-js g-clr_4">s@mobifitness.ru</a>
                                    <br>
                                    <a href="https://mobifitnesshelp.zendesk.com/hc/ru" target="blank"
                                       class="g-link-js g-clr_4">Справочный центр Mobifitness</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="g-col g-col_md_6">
                        <h2 class="h2_no_line">Подписывайтесь!</h2>
                        <ul class="soc-big g-mt_8_md">
                            <li class="soc-big__item"><a href="https://vk.com/mobifitness_ru" target="blank"
                                                         class="soc-big__link soc-big__link_1 icon-soc-big icon-soc-big_vk">Вконтакте</a>
                            </li>
                            <li class="soc-big__item"><a href="https://www.instagram.com/mobifitness_ru/" target="blank"
                                                         class="soc-big__link soc-big__link_2 icon-soc-big icon-soc-big_inst">Instagram</a>
                            </li>
                            <li class="soc-big__item"><a href="https://www.facebook.com/mobifitness.ru" target="blank"
                                                         class="soc-big__link soc-big__link_3 icon-soc-big icon-soc-big_fa">Facebook</a>
                            </li>
                            <li class="soc-big__item"><a
                                        href="https://blog.mobifitness.ru/?utm_source=sait&utm_medium=mobifitness&utm_campaign=sait "
                                        target="blank"
                                        class="soc-big__link soc-big__link_4 icon-soc-big icon-soc-big_write">Блог</a>
                            </li>
                            <li class="soc-big__item"><a href="https://www.youtube.com/channel/UClm9mioWT2Q7hoEyiPCG6cg"
                                                         target="blank"
                                                         class="soc-big__link soc-big__link_5 icon-soc-big icon-soc-big_youtube">YouTube</a>
                            </li>
                            <li class="soc-big__item"><a href="https://academy.mobifitness.ru/" target="blank"
                                                         class="soc-big__link soc-big__link_6 icon-soc-big icon-soc-big_ak">Академия<br>
                                    Mobifitness</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="page__contacts-control g-section-margin">
            <div class="g-grid">
                <div class="g-row">
                    <div class="g-col g-col_md_6">
                        <section class="form-line form-line_square">
                            <div class="form-line__box">

                                <div class="form-line__text g-pr">
                                    <h2 class="h2_no_line g-clr_inh">Абсолютно для всех клубов!</h2>
                                    <p>
                                        <small class="g-d_b_xs">Просто оставьте заявку, и мы вышлем <br>
                                            вам всю информацию.
                                        </small>
                                    </p>

                                    <form class="form-dark js-validated" action="?">
                                        <fieldset class="">
                                            <ul class="form-line__list g-row">
                                                <li class="form-line__item g-col g-col_md_6">
                                                    <label>
                                                        <span class="form-dark__label">Имя*</span>
                                                        <input type="text" class="form-dark__field" name="name">
                                                    </label>
                                                </li>
                                                <li class="form-line__item g-col g-col_md_6"
                                                >
                                                    <label>
                                                        <span class="form-dark__label">Телефон*</span>
                                                        <input type="tel" class="form-dark__field js-mask" name="phone">
                                                    </label>
                                                </li>
                                                <li class="form-line__item g-col g-col_md_6"
                                                >
                                                    <label>
                                                        <span class="form-dark__label">E-mail*</span>
                                                        <input type="email" class="form-dark__field js-mask"
                                                               name="email">
                                                    </label>
                                                </li>
                                                <li class="form-line__item g-col g-col_md_6">
                                                    <label>
                                                        <span class="form-dark__label">Название компании</span>
                                                        <input type="text" class="form-dark__field js-mask"
                                                               name="club">
                                                    </label>
                                                </li>
                                                <li class="form-line__item g-col">
                                                    <label>
                                                        <span class="form-dark__label">Комментарий</span>
                                                        <textarea maxlength="200" name="message" rows="4"
                                                                  class="form-dark__field js-mask"></textarea>
                                                    </label>
                                                </li>

                                                <li class="form-line__item g-col g-col_md_6"
                                                >

                                                    <input type="submit" class="btn g-col_xs_12" value="Отправить">
                                                </li>
                                            </ul>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                    <div class="g-col g-col_md_6 g-hidden g-show_md">
                        <div class="map-box">
                            <!-- <iframe src="https://yandex.ru/map-widget/v1/-/CBUYYWCVxC" max-width="560" max-height="400" frameborder="0"></iframe> -->
                            <script type="text/javascript" charset="utf-8" async
                                    src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A9f29bc70d77c767e7a10907d2050c367b61b6601a2fd5fd2db62363f3eae5ca4&amp;width=100%&amp;height=320&amp;lang=ru_RU&amp;scroll=true"></script>
                            <!--<a href="https://yandex.ru/maps/?um=constructor%3A6fdcfbc671c861bb3b45f3e2d73000d28468e24217d34c95655fbca1f2478d81&amp;source=constructorStatic" target="_blank"><img src="https://api-maps.yandex.ru/services/constructor/1.0/static/?um=constructor%3A6fdcfbc671c861bb3b45f3e2d73000d28468e24217d34c95655fbca1f2478d81&amp;width=560&amp;height=400&amp;lang=ru_RU" alt="" style="border: 0;" /></a>-->
                        </div>
                        <!-- соц сети -->
                        <div class="g-row">
                            <div class="g-col g-w_a_xs">
                                <div class="soc-frame">
                                    <script type="text/javascript" src="//vk.com/js/api/openapi.js?149"></script>
                                    <!-- VK Widget -->
                                    <div id="vk_groups"></div>
                                    <script type="text/javascript"> VK.Widgets.Group("vk_groups", {
                                            mode: 3,
                                            width: 237,
                                            no_cover: 1
                                        }, 114225725); </script>
                                </div>
                            </div>
                            <div class="g-col g-w_a_xs g-f_r_md">
                                <div class="soc-frame">
                                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fmobifitness.ru%2F&tabs&width=237&height=154&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId"
                                            width="237" height="160" style="border:none;overflow:hidden" scrolling="no"
                                            frameborder="0" allowTransparency="true"></iframe>
                                </div>

                            </div>
                        </div>

                        <!-- // -->
                    </div>
                </div>
            </div>
        </section>


<? include_once("_inc_footer.php"); ?>