<section class="section-reviews g-section-margin">
    <div class="g-grid">
        <h2 class="g-ta_c_xs">Отзывы</h2>
        <p class="g-ta_c_xs">Клиенты говорят о мобильном приложении для фитнес-клубов</p>
        <div class="g-col_md_10 g-mlr_auto_md">
            <div class="slider-reviews"
                 data-slick='{"respondTo": "min", "slidesToShow": 2,"slidesToScroll": 2,"dots": true, "fade": false,"arrows":true,"responsive": [{"breakpoint": 639,"settings":{"slidesToShow": 1, "slidesToScroll": 1,"arrows": false}}]}'>
                <div class="slider-reviews__item">
                    <a href="#" class="reviews-item g-link-js-parent">
                        <div class="reviews-item__avatar"
                             style="background-image: url(<?= $p . "img/reviews/se.jpg" ?>);"></div>
                        <div class="reviews-item__text">
                            <p class="reviews-item__name">Сидягина Екатерина, сооснователь студии "YogaArt".</p>
                            <p>Мы никак не нарадуемся такому прекрасному расписанию! Очень удобно им пользоваться и нам,
                                и клиентам! Не знаем, как раньше жили! Процветания Вам и всей команде Mobifitness!
                                Благодарим от всего сердца за такую крутую штуку!</p>
                        </div>
                        <span class="g-link-js g-clr_4 g-fs_sm g-hidden">Подробнее</span>
                    </a>
                </div>
                <div class="slider-reviews__item">
                    <a href="#" class="reviews-item g-link-js-parent">
                        <div class="reviews-item__avatar"
                             style="background-image: url(<?= $p . "img/reviews/ax.jpg" ?>);"></div>
                        <div class="reviews-item__text">
                            <p class="reviews-item__name">Артем Хабеев, руководитель студии йоги "Дыши".</p>
                            <p>В расписании, когда ты нажимаешь на занятие, открывается окно с информацией о
                                преподавателе и самом занятии, тем самым снимается задача оформлять отдельную страницу
                                на сайте с описанием занятий и тренерского состава клуба.</p>
                        </div>
                        <span class="g-link-js g-clr_4 g-fs_sm g-hidden">Подробнее</span>
                    </a>
                </div>

                <div class="slider-reviews__item">
                    <a href="#" class="reviews-item g-link-js-parent">
                        <div class="reviews-item__avatar"
                             style="background-image: url(<?= $p . "img/reviews/mz.jpg" ?>);"></div>
                        <div class="reviews-item__text">
                            <p class="reviews-item__name">Марина Захарова, главный редактор "CLUB BUSINESS".</p>
                            <p>То, что делает Mobifitness - удобно и красиво. Для клуба такое решение закрывает кучу
                                важных задач - от продлений до сарафанного радио.</p>
                        </div>
                        <span class="g-link-js g-clr_4 g-fs_sm g-hidden">Подробнее</span>
                    </a>
                </div>
                <div class="slider-reviews__item">
                    <a href="#" class="reviews-item g-link-js-parent">
                        <div class="reviews-item__avatar"
                             style="background-image: url(<?= $p . "img/reviews/kp.jpg" ?>);"></div>
                        <div class="reviews-item__text">
                            <p class="reviews-item__name">Кирилл Потамошев, генеральный директор "ProФитнес".</p>
                            <p>Благодаря продуктам Mobifitness получилось полностью автоматизировать клуб и отказаться
                                от рецепции.</p>
                        </div>
                        <span class="g-link-js g-clr_4 g-fs_sm g-hidden">Подробнее</span>
                    </a>
                </div>

            </div>
        </div>
        <div class="section-reviews__btn g-ta_c_xs">
            <a href="#popup-request-decor" class="btn btn-icon btn-icon_mod" data-colorbox>
                Стать клиентом Mobifitness
            </a>
        </div>
    </div>
</section>