<?php

    header('Access-Control-Allow-Origin: *');
	require_once $_SERVER['DOCUMENT_ROOT'].'/class.loveis.php';
	
	$time = date('Y-m-d H:i:s');
	foreach ($_POST as $key => $val)
		$_POST[$key] = htmlspecialchars($val);
		
	// формируем список адресов
	$mails = array();
	//$mails['dunaev.vasiliy@gmail.com'] = 'developer';
	$mails['hello@mobifitness.ru'] = 'mobifitness';
	$mails['apopova@mobifitness.ru'] = 'apopova';
	$mails['amerkuri@mobifitness.ru'] = 'amerkuri';
	$mails['a.yakubek@mobifitness.ru'] = 'zhk';
	$mails['wsr@ittools.ru'] = 'developer';

	$referer = '';
	if (!empty($_SERVER['HTTP_REFERER'])) {
		$referer = $_SERVER['HTTP_REFERER'];
	}

	$firstReferer = '';
	if (!empty($_POST['firstReferer'])) {
		$firstReferer = $_POST['firstReferer'];
	}

    $groupName = 'Главная';
    if (empty($_POST['group'])){
        $path = parse_url($_SERVER["HTTP_REFERER"]);
        $path = $path['path'];
        switch($path){
            case '/':
                $groupName = 'Главная';
                break;
            case '/erp':
                $groupName = 'Учетная система';
                break;
            case '/mobilapp':
                $groupName = 'Приложение';
                break;
            case '/raspisanie':
                $groupName = 'Онлайн-запись';
                break;
            case '/other':
                $groupName = 'Другие продукты';
                break;
            case '/contacts':
                $groupName = 'Контакты';
                break;        
        }
    }
    if (!empty($_POST['group']) && $_POST['group'] == 'subscribe'){
        $groupName = 'Будь в курсе событий';
    }
    if (!empty($_POST['group']) && $_POST['group'] == 'free-schedule'){
        $groupName = 'Секреты успеха';
    }
    if (!empty($_POST['group']) && $_POST['group'] == 'secret') {
        $groupName = 'Цепочка с главного сайта';
    }

	// собираем данные формы
	$post_data = array();
	if (isset($_GET['action']) && $_GET['action'] == 'send') {
        if (!empty($_POST['name'])){
		  $post_data[] = array('name' => 'Имя', 'value' => $_POST['name'], 'is_textarea' => false);
        }
        if (!empty($_POST['email'])){
            $post_data[] = array('name' => 'E-mail', 'value' => $_POST['email'], 'is_textarea' => false);
        }
        if (!empty($_POST['phone'])){
            $post_data[] = array('name' => 'Телефон', 'value' => $_POST['phone'], 'is_textarea' => false);
        }
        if (!empty($_POST['club'])){
            $post_data[] = array('name' => 'Клуб', 'value' => $_POST['club'], 'is_textarea' => false);
        }
        if (!empty($_POST['tariff_name'])){
            $post_data[] = array('name' => 'Название тарифа', 'value' => $_POST['tariff_name'], 'is_textarea' => false);
        }
        if (!empty($_POST['tariff_period'])){
            $post_data[] = array('name' => 'Выбранный период тарифа', 'value' => $_POST['tariff_period'], 'is_textarea' => false);
        }
        if (!empty($_POST['message'])){
            $post_data[] = array('name' => 'Сообщение', 'value' => $_POST['message'], 'is_textarea' => true);
        }
        if (!empty($_POST['selectedBlock'])){
            $post_data[] = array('name' => 'Выбранный блок', 'value' => $_POST['selectedBlock'], 'is_textarea' => true);
        }
        $post_data[] = array('name' => 'Реферрер', 'value' => $referer, 'is_textarea' => false);	
        $post_data[] = array('name' => 'Страница входа', 'value' => $firstReferer, 'is_textarea' => false);	
		$post_data[] = array('name' => 'Время отправки заявки', 'value' => $time, 'is_textarea' => false);
		LoveisMailer::send_mail('Получена заявка на сайте '.$_SERVER['HTTP_HOST'].' '.$time, $post_data, $mails);
		echo 'sended';

        if (!empty($_POST['email'])){
            $subscribeData = array(
                'name'  => !empty($_POST['name']) ? $_POST['name'] : reset(explode('@', $_POST['email'])),
                'email' => $_POST['email']
            );
            subscribe($subscribeData, $groupName);
        }
	}

    /*if (isset($_GET['action']) && $_GET['action'] == 'tarif') {
        $post_data[] = array('name' => 'Имя', 'value' => $_POST['name'], 'is_textarea' => false);
        $post_data[] = array('name' => 'E-mail', 'value' => $_POST['email'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Телефон', 'value' => $_POST['phone'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Тариф', 'value' => $_POST['mf-tarif-order-title'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Период', 'value' => $_POST['mf-tarif-order-payment'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Реферрер', 'value' => $_POST['referrer'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Страница', 'value' => $_SERVER["HTTP_REFERER"], 'is_textarea' => false);

        $post_data[] = array('name' => 'Время отправки заявки', 'value' => $time, 'is_textarea' => false);
        LoveisMailer::send_mail('Получена заявка на сайте '.$_SERVER['HTTP_HOST'].' '.$time, $post_data, $mails);
        echo 'sended';
    }

    if (isset($_GET['action']) && $_GET['action'] == 'tarif_sn') {
        $post_data[] = array('name' => 'Имя', 'value' => $_POST['name'], 'is_textarea' => false);
        $post_data[] = array('name' => 'E-mail', 'value' => $_POST['email'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Телефон', 'value' => $_POST['phone'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Тариф', 'value' => $_POST['mf-tarif-order-title_sn'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Период', 'value' => $_POST['mf-tarif-order-payment_sn'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Реферрер', 'value' => $_POST['referrer_sn'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Страница', 'value' => $_SERVER["HTTP_REFERER"], 'is_textarea' => false);

        $post_data[] = array('name' => 'Время отправки заявки', 'value' => $time, 'is_textarea' => false);
        LoveisMailer::send_mail('Получена заявка на сайте '.$_SERVER['HTTP_HOST'].' '.$time, $post_data, $mails);
        echo 'sended';
    }
	
	if (isset($_GET['action']) && $_GET['action'] == 'lmp') {
		$post_data[] = array('name' => 'Имя', 'value' => $_POST['name'], 'is_textarea' => false);
		$post_data[] = array('name' => 'E-mail', 'value' => $_POST['email'], 'is_textarea' => false);
		$post_data[] = array('name' => 'Телефон', 'value' => $_POST['phone'], 'is_textarea' => false);
		$post_data[] = array('name' => 'Компания', 'value' => $_POST['club'], 'is_textarea' => false);
		$post_data[] = array('name' => 'Тариф', 'value' => $_POST['mf-tarif-order-title'], 'is_textarea' => false);
		$post_data[] = array('name' => 'Период', 'value' => $_POST['mf-tarif-order-payment'], 'is_textarea' => false);
		$post_data[] = array('name' => 'Реферрер', 'value' => $_POST['referrer'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Страница', 'value' => $_SERVER["HTTP_REFERER"], 'is_textarea' => false);
		
		$post_data[] = array('name' => 'Время отправки заявки', 'value' => $time, 'is_textarea' => false);
        $mails = array();
        $mails['webinar2@mobifitness.ru'] = 'webinar';
		LoveisMailer::send_mail('Заявка на видео конкурентов на сайте '.$_SERVER['HTTP_HOST'].' '.$time, $post_data, $mails);
		echo 'sended';

        $subscribeData = array(
            'name'  => $_POST['name'],
            'email' => $_POST['email']
        );
        subscribe($subscribeData);
	}

    if (isset($_GET['action']) && $_GET['action'] == 'callme') {
        $post_data[] = array('name' => 'Имя', 'value' => $_POST['name'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Телефон', 'value' => $_POST['phone'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Реферрер', 'value' => $_POST['referrer'], 'is_textarea' => false);
        $post_data[] = array('name' => 'Страница', 'value' => $_SERVER["HTTP_REFERER"], 'is_textarea' => false);

        $post_data[] = array('name' => 'Время отправки заявки', 'value' => $time, 'is_textarea' => false);
        unset($mails['hello@mobifitness.ru']);
        $mails['webinar2@mobifitness.ru'] = 'webinar';
        LoveisMailer::send_mail('Получена заявка на сайте '.$_SERVER['HTTP_HOST'].' '.$time, $post_data, $mails);
        echo 'sended';
    }*/

	$body = '';
	if (isset($_POST['name'])){
		$body .= '<b>Имя</b>: '.$_POST['name'].'<br/>';
	}
	if (isset($_POST['email'])){
		$body .= '<b>E-mail</b>: '.$_POST['email'].'<br/>';
	}
	if (isset($_POST['phone'])){
		$body .= '<b>Телефон</b>: '.$_POST['phone'].'<br/>';
	}
	if (isset($_POST['message'])){
		$body .= '<b>Сообщение</b>: '.$_POST['message'].'<br/>';
	}

    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];


$html = <<<HTML
<div style="width:100%;height:100%;font-family:arial,'helvetica neue',helvetica,sans-serif;padding:0;Margin:0"><img alt="" src="https://ci6.googleusercontent.com/proxy/ZZwIdHdmgEpunqpkytV26MX76_JEZLUqEIJJd4Omi5eDrg3hXrVaPaOkF-13XJAAuErEypLj6E6aNYRsJ-5vsRyubcJwBOrSRuAcclGolB_TBe3BbPyOUWOocVKJtYnBe0EnsFZ-bHcZXkbGSvr76GdRmFZ1Lfw6y234p3YfZQ4Alg=s0-d-e1-ft#http://pics.esputnik.com/repository/applications/commons/hidden.png?iid=1AE9BE6E-3D2A-4805-828A-3F2ACC4E626D" class="CToWUd">
    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top">
        <tbody>
            <tr height="0" style="border-collapse:collapse">
                <td style="padding:0;Margin:0">
                <table cellpadding="0" cellspacing="0" border="0" align="center" width="600" style="border-collapse:collapse;border-spacing:0px">
                    <tbody>
                        <tr style="border-collapse:collapse">
                            <td cellpadding="0" cellspacing="0" border="0" height="0" style="padding:0;Margin:0;line-height:1px;min-width:600px"><img width="600" height="1" alt="" style="display:block;border:0;text-decoration:none;display:block;max-height:0px;min-height:0px;min-width:600px;width:600px" src="https://ci4.googleusercontent.com/proxy/wJkw7LuZ2F7tBADvV8CoKHpqKQDElS8x0Z2-0YLlRb-3Z_3UxVBqZBwMl42OSZERYL9ECqU2J9wYph5hLyoBQnbXdIkzu4N9czxfSecpzPDAMoo=s0-d-e1-ft#https://esputnik.com/repository/applications/images/blank.gif" class="CToWUd"></td>
                        </tr>
                    </tbody>
                </table></td>
            </tr>
            <tr style="border-collapse:collapse">
                <td valign="top" style="padding:0;Margin:0">
                <table cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse;border-spacing:0px;table-layout:fixed;width:100%">
                    <tbody>
                        <tr style="border-collapse:collapse">
                            <td align="center" bgcolor="#efefef" style="padding:0;Margin:0;background-color:rgb(239,239,239)">
                            <table bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border-spacing:0px;background-color:rgb(255,255,255)">
                                <tbody>
                                    <tr style="border-collapse:collapse">
                                        <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;padding-left:10px;padding-right:10px">
                                        <table cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse;border-spacing:0px;float:left">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="184" valign="top" align="center" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="center" style="padding:0;Margin:0"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="http://mobifitness.ru/?utm_source=mobifitness&utm_medium=email&utm_campaign=email_mobifitness" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmepwWh2GGv&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNGayNioNS8FcTc-lJGa61gHVBrtpQ"><img alt="" width="186" style="display:block;border:0;text-decoration:none" src="https://ci5.googleusercontent.com/proxy/-YH5fxr3UCPilngeJuFDS0txAbRcl-lC6XfxioiL4Hq0VZYLMkojWFEcKkn1tiCzOnxOHRPKBA0DAWsn8esGkotNzoTgoaWwuNU4bZAzhhYjPHdacjSKuV5JObfXJoOFF1MhXDFH6c41WlLpGBw=s0-d-e1-ft#https://pics.esputnik.com.ua/repository/home/22227/images/msg/39734971/1507818960022.png" class="CToWUd"> </a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;border-spacing:0px">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="369" align="left" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="right" style="padding:0;Margin:0;padding-top:15px">
                                                                <table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px">
                                                                    <tbody>
                                                                        <tr style="border-collapse:collapse">
                                                                            <td align="center" valign="top" style="padding:0;Margin:0;padding-right:20px"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="https://vk.com/mobifitness_ru" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmfMlBPOiOv&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNGZsTcN7CNi0r6XTiZjrmXyaGxBbA"><img title="Vkontakte" alt="Vk" width="32" style="display:block;border:0;text-decoration:none" src="https://ci5.googleusercontent.com/proxy/AfxZ3Y_vu1QZ07tpquMU3-LwSKGHiEuiTumIMoKB6aZWVJzE_CurdTQiGqYMjTFz7imfN5EKN67Qlc5YhDjati9Q07xi-RBNgd5V8vQjivSN_pWhGv4XxUBaNh_ssd4Q2wReNJJvTKyxwTdKgw0C2PpESXpB5PHLbiZ8iVmXhTNm-WYPVF89=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/vk-circle-colored.png" class="CToWUd"></a></td><td align="center" valign="top" style="padding:0;Margin:0;padding-right:20px"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="https://www.facebook.com/mobifitness.ru/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmfkCVdN2Wv&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNGkQJs605RXdrrpLv7i0AjSPCgW0A"><img title="Facebook" alt="Fb" width="32" style="display:block;border:0;text-decoration:none" src="https://ci3.googleusercontent.com/proxy/Qp1TudfATk9zGJna0Bm3gVlb0gnbimMI-pUUIvbbvUO5D-LPFvBLoh0L-2pi8dhVz-0-_0zuL45D3Kfu2ccjLhQcUhfn3JeIrUM_2fGrPG2JR6DNdyjkWESZZVEtvT9pEUHX0DYJSlFf64vAoP56FMQL2hXXtKXCRr788SlF5mCF3cwKa8aaWzBLvaCv=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/facebook-circle-colored.png" class="CToWUd"></a></td><td align="center" valign="top" style="padding:0;Margin:0;padding-right:20px"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="https://www.instagram.com/mobifitness_ru/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmftZq7lA8v&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNEq-hQOpQMHc9mmy4dPopZAy_YczQ"><img title="Instagram" alt="Ig" width="32" style="display:block;border:0;text-decoration:none" src="https://ci4.googleusercontent.com/proxy/FiE_kfWq9W8UDhTUUqMY1w2T-zl_sizK3vAQOZhchvBp21xDWoLxrqvHfLkBxQCIydymtuvP1OKRR6uNZRBzH-JH348tLOTMbQ0Yc5H_HQEUTu1NhoiAPi4sP_LLajs1N7Vs6XZfWoZWVHWGr5VrDufB55DhowJ1tMy6vomACkCNE9o_tRliUfRIwnNOOQ=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/instagram-circle-colored.png" class="CToWUd"></a></td><td align="center" valign="top" style="padding:0;Margin:0;padding-right:20px"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="https://www.youtube.com/channel/UClm9mioWT2Q7hoEyiPCG6cg" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmg2xAc9Iev&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNHm1FCNKoXto6GCNbuYHcoj4gsYGA"><img title="Youtube" alt="Yt" width="32" style="display:block;border:0;text-decoration:none" src="https://ci6.googleusercontent.com/proxy/2ldhPJ-5zngbl2z1L2hOHRHmJwbr9UiidT17RXiBgrWI7kxbuiN_nxlppLL0tMq6GGn-bwI2RrhuXixeGceDvyIy0KlvU4MxEmI9y-0G-hbMYvV-wnhcXDbJ4fCqz_6cxN2FRBDiGlKB1EAdQGxqI9gmEIh73fxJJ3C-GgpQK8BgX_M5qujbiLMU6mU=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/youtube-circle-colored.png" class="CToWUd"></a></td><td align="center" valign="top" style="padding:0;Margin:0"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="http://blog.mobifitness.ru/?utm_source=sait&utm_medium=mobifitness&utm_campaign=sait" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmg7dprLMuv&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNEFyERAo9p__5LJaTwttXNZxykQfw"><img title="Livejournal" alt="Lj" width="32" style="display:block;border:0;text-decoration:none" src="https://ci3.googleusercontent.com/proxy/6utklL0xZAprs36wakaOA8crO3ytIA0A2qFCvGMWF33dZwTuR5zmGjQDUw5ZPAenYhEj4EO3md3Mh6ZtRPur9-OOyGJsn530LjcmA5g5LR09J7EAUKlfyzq7xxrXjC8RqIn4vz90zWZS3Ki9NHdJC9_nt2EfEmxgbRG5wAZVakzAwdx3gAlJGGegpSLaB3_h=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/livejournal-circle-colored.png" class="CToWUd"></a></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                    </tr>
                                </tbody>
                            </table></td>
                        </tr>
                    </tbody>
                </table>
                <table cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse;border-spacing:0px;table-layout:fixed;width:100%">
                    <tbody>
                        <tr style="border-collapse:collapse">
                            <td bgcolor="#efefef" style="padding:0;Margin:0;background-color:rgb(239,239,239)">
                            <table align="center" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border-spacing:0px;background-color:rgb(255,255,255);background-image:url(https://ci5.googleusercontent.com/proxy/didIRv0OlAz0DN4ImZGJsLXwKMfeUA1ORnBXpIfiwwD6EoLA0cOVlWbWCYPdp0k5GiSUcektR81_Dc2xuH9VFea_es2CGbyqzgkDP7OudAOu_5ctdNE4aV4fJi6oFBOPslwVFkybS1xXQcms1RY=s0-d-e1-ft#https://pics.esputnik.com.ua/repository/home/22227/images/msg/39734971/1507819610078.jpg);background-position:left top;background-repeat:no-repeat">
                                <tbody>
                                    <tr style="border-collapse:collapse">
                                        <td align="left" style="padding:0;Margin:0;padding-bottom:30px;padding-top:40px;padding-left:40px;padding-right:40px">
                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="560" align="center" valign="top" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="center" style="padding:0;Margin:0;padding-top:5px;padding-bottom:5px"><h3 style="Margin:0;line-height:120%;font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:20px;font-style:normal;font-weight:bold;color:rgb(51,51,51);color:rgb(255,255,255)"><span style="font-size:19px">От вас получена заявка на сайте&nbsp;<a style="color:white" href="http://mobifitness.ru/?utm_source=mobifitness&utm_medium=email&utm_campaign=email_mobifitness" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=http://mobifitness.ru&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNEbEVP2Tkl6oM7_n7o64L_2kpFCEQ">mobifitness.ru</a>&nbsp;</span></h3></td>
                                                            </tr>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="center" style="padding:0;Margin:0">
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51);color:rgb(255,255,255)">
                                                                    Мы постараемся связаться с вами в ближайшее время!
                                                                </p></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                    </tr>
                                </tbody>
                            </table></td>
                        </tr>
                    </tbody>
                </table>
                <table cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse;border-spacing:0px;table-layout:fixed;width:100%">
                    <tbody>
                        <tr style="border-collapse:collapse">
                            <td align="center" bgcolor="#efefef" style="padding:0;Margin:0;background-color:rgb(239,239,239)">
                            <table bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border-spacing:0px;background-color:rgb(255,255,255)">
                                <tbody>
                                    <tr style="border-collapse:collapse">
                                        <td align="left" style="padding:0;Margin:0">
                                        <table cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse;border-spacing:0px;float:left">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="290" align="left" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="left" style="padding:0;Margin:0;padding-left:20px;padding-top:25px">
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51)">
                                                                    <strong><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="http://mobifitness.ru/?utm_source=mobifitness&utm_medium=email&utm_campaign=email_mobifitness" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmgH1ALjUuv&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNHZRiFxCMlSZDutelXKvCLJCWRpCA">Mobifitness</a></strong>&nbsp;– технологический лидер в области решений для фитнес-клубов:
                                                                </p>
                                                                <ul>
                                                                    <li style="font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;Margin-bottom:15px;color:rgb(51,51,51)">
                                                                        функциональные мобильные приложения,
                                                                    </li>
                                                                    <li style="font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;Margin-bottom:15px;color:rgb(51,51,51)">
                                                                        личный кабинет для сайта,
                                                                    </li>
                                                                    <li style="font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;Margin-bottom:15px;color:rgb(51,51,51)">
                                                                        виджеты&nbsp;расписания и оплаты для сайта и соц.сетей,
                                                                    </li>
                                                                    <li style="font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;Margin-bottom:15px;color:rgb(51,51,51)">
                                                                        интеграция со всеми клубными системами.
                                                                    </li>
                                                                </ul></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;border-spacing:0px;float:right">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="290" align="left" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="center" style="padding:0;Margin:0"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="http://mobifitness.ru/?utm_source=mobifitness&utm_medium=email&utm_campaign=email_mobifitness" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmgLhpavY0v&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNHrNVWCChvjSMUus7XvlQod928KWA"><img alt="" width="290" style="display:block;border:0;text-decoration:none" src="https://ci5.googleusercontent.com/proxy/AglSZzMpoCi0XPPi0MWQbSODLjfUvCqQOAEeNIhm-agf7LTjzchiFO_CrKIjVUA5o8gHDe6l96wMADbdWXwzk2k59hxlcL4aXEz1Njaeg55ynnxrQEoBXCvpel49u2GXdvaPAIgUWLVI45A6M6I=s0-d-e1-ft#https://pics.esputnik.com.ua/repository/home/10266/images/msg/97942247/1508417904693.jpg" class="CToWUd"> </a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                    </tr>
                                    <tr style="border-collapse:collapse">
                                        <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;padding-left:10px;padding-right:10px">
                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="580" align="center" valign="top" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="left" style="padding:0;Margin:0;padding-right:15px;padding-left:25px">
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51);line-height:150%">
                                                                    <span style="font-family:arial,helvetica neue,helvetica,sans-serif;text-align:center">Наша мобильная платформа имеет функциональность, позволяющую клубам расти, разгружать сотрудников, увеличивать продажи и быть лидером среди конкурентов.</span>
                                                                </p></td>
                                                            </tr>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="center" style="padding:20px;Margin:0">
                                                                <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px">
                                                                    <tbody>
                                                                        <tr style="border-collapse:collapse">
                                                                            <td style="padding:0;Margin:0;border-bottom:1px solid #cccccc;background:none;height:1px;width:100%;margin:0px 0px 0px 0px">&nbsp;</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table></td>
                                                            </tr>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="left" style="padding:0;Margin:0;padding-left:25px">
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51)">
                                                                    На всякий случай, дублируем вам данные из заявки:
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51)">
                                                                    &nbsp;
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51)">
                                                                    <strong>Имя: {$name}</strong>
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51)">
                                                                    <strong>E-mail: {$email}</strong>
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51)">
                                                                    <strong>Телефон: {$phone}</strong>
                                                                </p></td>
                                                            </tr>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="center" style="padding:20px;Margin:0">
                                                                <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px">
                                                                    <tbody>
                                                                        <tr style="border-collapse:collapse">
                                                                            <td style="padding:0;Margin:0;border-bottom:1px solid #cccccc;background:none;height:1px;width:100%;margin:0px 0px 0px 0px">&nbsp;</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                    </tr>
                                    <tr style="border-collapse:collapse">
                                        <td align="left" style="padding:0;Margin:0">
                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="580" align="center" valign="top" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="center" style="padding:0;Margin:0">
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51)">
                                                                    <strong><span style="font-size:16px;line-height:150%">Присоединяйтесь к нам!</span></strong>
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51)">
                                                                    <span style="font-size:16px">Новости, акции, советы экспертов и многое другое!</span>
                                                                </p></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                    </tr>
                                </tbody>
                            </table></td>
                        </tr>
                    </tbody>
                </table>
                <table cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse;border-spacing:0px;table-layout:fixed;width:100%">
                    <tbody>
                        <tr style="border-collapse:collapse">
                            <td align="center" bgcolor="#efefef" style="padding:0;Margin:0;background-color:rgb(239,239,239)">
                            <table bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border-spacing:0px;background-color:rgb(255,255,255)">
                                <tbody>
                                    <tr style="border-collapse:collapse">
                                        <td align="left" style="padding:0;Margin:0">
                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="580" align="center" valign="top" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="center" style="padding:0;Margin:0;padding-top:15px">
                                                                <table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px">
                                                                    <tbody>
                                                                        <tr style="border-collapse:collapse">
                                                                            <td align="center" valign="top" style="padding:0;Margin:0;padding-right:20px"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="https://vk.com/mobifitness_ru" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmgQOUq7cWv&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNERYkoYqtYfXFgZixETuTsA-1D1FQ"><img title="Vkontakte" alt="Vk" width="32" style="display:block;border:0;text-decoration:none" src="https://ci5.googleusercontent.com/proxy/AfxZ3Y_vu1QZ07tpquMU3-LwSKGHiEuiTumIMoKB6aZWVJzE_CurdTQiGqYMjTFz7imfN5EKN67Qlc5YhDjati9Q07xi-RBNgd5V8vQjivSN_pWhGv4XxUBaNh_ssd4Q2wReNJJvTKyxwTdKgw0C2PpESXpB5PHLbiZ8iVmXhTNm-WYPVF89=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/vk-circle-colored.png" class="CToWUd"></a></td><td align="center" valign="top" style="padding:0;Margin:0;padding-right:20px"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="https://www.facebook.com/mobifitness.ru/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmgV5A5Jg0v&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNHmTJFH9Z4IvWP81pT3yLtIIjEWQQ"><img title="Facebook" alt="Fb" width="32" style="display:block;border:0;text-decoration:none" src="https://ci3.googleusercontent.com/proxy/Qp1TudfATk9zGJna0Bm3gVlb0gnbimMI-pUUIvbbvUO5D-LPFvBLoh0L-2pi8dhVz-0-_0zuL45D3Kfu2ccjLhQcUhfn3JeIrUM_2fGrPG2JR6DNdyjkWESZZVEtvT9pEUHX0DYJSlFf64vAoP56FMQL2hXXtKXCRr788SlF5mCF3cwKa8aaWzBLvaCv=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/facebook-circle-colored.png" class="CToWUd"></a></td><td align="center" valign="top" style="padding:0;Margin:0;padding-right:20px"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="https://www.instagram.com/mobifitness_ru/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmgZlpKVkGv&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNE7cnnUe8saed8l1ChOLZRxiPmPmg"><img title="Instagram" alt="Ig" width="32" style="display:block;border:0;text-decoration:none" src="https://ci4.googleusercontent.com/proxy/FiE_kfWq9W8UDhTUUqMY1w2T-zl_sizK3vAQOZhchvBp21xDWoLxrqvHfLkBxQCIydymtuvP1OKRR6uNZRBzH-JH348tLOTMbQ0Yc5H_HQEUTu1NhoiAPi4sP_LLajs1N7Vs6XZfWoZWVHWGr5VrDufB55DhowJ1tMy6vomACkCNE9o_tRliUfRIwnNOOQ=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/instagram-circle-colored.png" class="CToWUd"></a></td><td align="center" valign="top" style="padding:0;Margin:0;padding-right:20px"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="https://www.youtube.com/channel/UClm9mioWT2Q7hoEyiPCG6cg" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmgeSUZho8v&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNHQucf8CZsNuxI1GfRciZ-a0bT6OQ"><img title="Youtube" alt="Yt" width="32" style="display:block;border:0;text-decoration:none" src="https://ci6.googleusercontent.com/proxy/2ldhPJ-5zngbl2z1L2hOHRHmJwbr9UiidT17RXiBgrWI7kxbuiN_nxlppLL0tMq6GGn-bwI2RrhuXixeGceDvyIy0KlvU4MxEmI9y-0G-hbMYvV-wnhcXDbJ4fCqz_6cxN2FRBDiGlKB1EAdQGxqI9gmEIh73fxJJ3C-GgpQK8BgX_M5qujbiLMU6mU=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/youtube-circle-colored.png" class="CToWUd"></a></td><td align="center" valign="top" style="padding:0;Margin:0"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="http://blog.mobifitness.ru/?utm_source=sait&utm_medium=mobifitness&utm_campaign=sait" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmgj99otsGv&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNFR9sIpPYXMaOaTerAyux-XaxiW8g"><img title="Livejournal" alt="Lj" width="32" style="display:block;border:0;text-decoration:none" src="https://ci3.googleusercontent.com/proxy/6utklL0xZAprs36wakaOA8crO3ytIA0A2qFCvGMWF33dZwTuR5zmGjQDUw5ZPAenYhEj4EO3md3Mh6ZtRPur9-OOyGJsn530LjcmA5g5LR09J7EAUKlfyzq7xxrXjC8RqIn4vz90zWZS3Ki9NHdJC9_nt2EfEmxgbRG5wAZVakzAwdx3gAlJGGegpSLaB3_h=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/livejournal-circle-colored.png" class="CToWUd"></a></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                    </tr>
                                    <tr style="border-collapse:collapse">
                                        <td align="left" style="padding:0;Margin:0">
                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="580" align="center" valign="top" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="center" style="padding:20px;Margin:0">
                                                                <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px">
                                                                    <tbody>
                                                                        <tr style="border-collapse:collapse">
                                                                            <td style="padding:0;Margin:0;border-bottom:1px solid #cccccc;background:none;height:1px;width:100%;margin:0px 0px 0px 0px">&nbsp;</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                    </tr>
                                </tbody>
                            </table></td>
                        </tr>
                    </tbody>
                </table>
                <table cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse;border-spacing:0px;table-layout:fixed;width:100%">
                    <tbody>
                        <tr style="border-collapse:collapse">
                            <td bgcolor="#efefef" style="padding:0;Margin:0;background-color:rgb(239,239,239)">
                            <table bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border-spacing:0px;background-color:rgb(255,255,255)">
                                <tbody>
                                    <tr style="border-collapse:collapse">
                                        <td align="left" bgcolor="#ffffff" style="padding:0;Margin:0;padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px;background-color:rgb(255,255,255)">
                                        <table cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse;border-spacing:0px;float:left">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="180" valign="top" align="center" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="center" style="padding:0;Margin:0"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="http://academy.mobifitness.ru/?utm_source=mobifitness&utm_medium=email&utm_campaign=rasp_email" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmgnpp45wWv&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNHHdKaMgAMjCLYDlWVxlkPq_oLv5Q"><img alt="" width="170" style="display:block;border:0;text-decoration:none" src="https://ci5.googleusercontent.com/proxy/Hx4qdJGomfqUcznXmAfYZ2FZFQMP-DzNfLp9tiI4rzhwRMJgvPOlijJPJuMMRXEtzb72I82xyogbbgaUL5_SfnhcX_77_TvRaYD6m-HO1NRnA7ORfhQwLjQBtofJEXlp7bOP68mSN7YvnfhT69k=s0-d-e1-ft#https://pics.esputnik.com.ua/repository/home/22227/images/msg/39734971/1507820304838.png" class="CToWUd"></a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;border-spacing:0px">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="360" align="left" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="left" style="padding:0;Margin:0;padding-bottom:20px"><h3 style="Margin:0;line-height:120%;font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:20px;font-style:normal;font-weight:bold;color:rgb(51,51,51)">Бесплатные онлайн-вебинары</h3></td>
                                                            </tr>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="left" style="padding:0;Margin:0">
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51)">
                                                                    Зарегистрироваться на бесплатные онлайн-вебинары по маркетингу и продажам для&nbsp;фитнес-клубов, студий&nbsp;танцев и йоги&nbsp;вы можете на сайте&nbsp;
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51)">
                                                                    &nbsp;<strong><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="http://academy.mobifitness.ru/?utm_source=mobifitness&utm_medium=email&utm_campaign=mobifitness_email" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmgsWUJI0uv&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNFjT-zZFagdbvytY0Iua0uiaLQJyA">academy.mobifitness.ru</a></strong>
                                                                </p></td>
                                                            </tr>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;padding-right:10px"><span style="background:#333333;border-color:#2cb543;border-radius:4px;border-style:solid solid solid solid;border-width:0px;display:inline-block;width:auto"><a style="text-decoration:none;font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:18px;color:rgb(255,255,255);border-style:solid;border-color:rgb(49,203,75);border-width:10px 20px 10px 20px;display:inline-block;background:rgb(49,203,75);border-radius:30px;font-weight:normal;font-style:normal;line-height:120%;width:auto;text-align:center;background:rgb(51,51,51);border-color:rgb(51,51,51);border-radius:4px;border-width:10px 20px" href="http://academy.mobifitness.ru/?utm_source=mobifitness&utm_medium=email&utm_campaign=mobifitness_email" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmgxD9YU4Ov&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNEQbfeypqLwAwnMZ3AxAKH7sMiVnw">Зарегистрироваться</a></span></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                    </tr>
                                </tbody>
                            </table></td>
                        </tr>
                    </tbody>
                </table>
                <table cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse;border-spacing:0px;table-layout:fixed;width:100%">
                    <tbody>
                        <tr style="border-collapse:collapse">
                            <td align="center" bgcolor="#efefef" style="padding:0;Margin:0;background-color:rgb(239,239,239)">
                            <table bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border-spacing:0px;background-color:rgb(255,255,255);background-image:url(https://ci5.googleusercontent.com/proxy/fYJggENj6O8gKxdcBmoxFJUlf-8vV_R0Oi3jqqjNvqWA3hpmqh9ZrBJ45pRVUS4XFA7WU8hkoZ1-z12y6iJ6WEBsVeqOnMhegAV5iM5at9YeyACPZfWqpKqv9O6hmRteJBfoyan54BtzHmXyIBI=s0-d-e1-ft#https://pics.esputnik.com.ua/repository/home/10266/images/msg/97942247/1508160862900.png);background-position:left top;background-repeat:no-repeat">
                                <tbody>
                                    <tr style="border-collapse:collapse">
                                        <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px;padding-left:10px;padding-right:10px">
                                        <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="580" align="center" valign="top" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="left" style="padding:0;Margin:0">
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51)">
                                                                    &nbsp;
                                                                </p></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                    </tr>
                                    <tr style="border-collapse:collapse">
                                        <td align="left" style="padding:0;Margin:0">
                                        <table cellpadding="0" cellspacing="0" align="left" style="border-collapse:collapse;border-spacing:0px;float:left">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="290" align="left" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px">
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51);line-height:150%;color:rgb(255,255,255)">
                                                                    Команда Mobifitness
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51);line-height:150%;color:rgb(255,255,255)">
                                                                    <strong><span style="line-height:150%"><a href="mailto:hello@mobifitness.ru" style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200);color:rgb(255,255,255)">hello@mobifitness.ru</a></span></strong>
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51);line-height:150%;color:rgb(255,255,255)">
                                                                    <span style="color:#ffffff;line-height:150%"><span><a href="tel:+7%20495%20133-95-32" value="+74951339532" target="_blank" style="color:white;">+7 (495) 133 95 32</a></span></span>
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51);line-height:150%;color:rgb(255,255,255)">
                                                                    &nbsp;
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51);line-height:150%;color:rgb(255,255,255)">
                                                                    <span style="color:#ffffff;line-height:150%">Тех. поддержка:&nbsp;</span><strong><a href="mailto:s@mobifitness.ru" style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200);color:rgb(255,255,255)">s@mobifitness.ru</a></strong>
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51);line-height:150%;color:rgb(255,255,255)">
                                                                    &nbsp;
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51);line-height:150%;color:rgb(255,255,255)">
                                                                    <span style="font-size:12px;line-height:150%">Москва, Семеновский переулок, 15</span>
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51);line-height:150%;color:rgb(255,255,255)">
                                                                    &nbsp;
                                                                </p>
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51);line-height:150%;color:rgb(255,255,255)">
                                                                    &nbsp;
                                                                </p></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table cellpadding="0" cellspacing="0" align="right" style="border-collapse:collapse;border-spacing:0px;float:right">
                                            <tbody>
                                                <tr style="border-collapse:collapse">
                                                    <td width="290" align="left" style="padding:0;Margin:0">
                                                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px;border-radius:0px">
                                                        <tbody>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="right" style="padding:0;Margin:0;padding-top:40px;padding-right:40px">
                                                                <table cellpadding="0" cellspacing="0" style="border-collapse:collapse;border-spacing:0px">
                                                                    <tbody>
                                                                        <tr style="border-collapse:collapse">
                                                                            <td align="center" valign="top" style="padding:0;Margin:0;padding-right:10px"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="https://vk.com/mobifitness_ru" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmh1tong8Gv&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNEY2ioNjDJPts7pCoSmfYsir7FcoQ"><img title="Vkontakte" alt="Vk" width="32" style="display:block;border:0;text-decoration:none" src="https://ci5.googleusercontent.com/proxy/AfxZ3Y_vu1QZ07tpquMU3-LwSKGHiEuiTumIMoKB6aZWVJzE_CurdTQiGqYMjTFz7imfN5EKN67Qlc5YhDjati9Q07xi-RBNgd5V8vQjivSN_pWhGv4XxUBaNh_ssd4Q2wReNJJvTKyxwTdKgw0C2PpESXpB5PHLbiZ8iVmXhTNm-WYPVF89=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/vk-circle-colored.png" class="CToWUd"></a></td><td align="center" valign="top" style="padding:0;Margin:0;padding-right:10px"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="https://www.facebook.com/mobifitness.ru/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmh6aU2sC0v&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNHvrqtVrkPM0-wUUYYmUqpv3gWuHQ"><img title="Facebook" alt="Fb" width="32" style="display:block;border:0;text-decoration:none" src="https://ci3.googleusercontent.com/proxy/Qp1TudfATk9zGJna0Bm3gVlb0gnbimMI-pUUIvbbvUO5D-LPFvBLoh0L-2pi8dhVz-0-_0zuL45D3Kfu2ccjLhQcUhfn3JeIrUM_2fGrPG2JR6DNdyjkWESZZVEtvT9pEUHX0DYJSlFf64vAoP56FMQL2hXXtKXCRr788SlF5mCF3cwKa8aaWzBLvaCv=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/facebook-circle-colored.png" class="CToWUd"></a></td><td align="center" valign="top" style="padding:0;Margin:0;padding-right:10px"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="https://www.instagram.com/mobifitness_ru/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmhBH9I4GWv&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNEmWBMzyD6MEvVOAhSVr211jV6HGw"><img title="Instagram" alt="Ig" width="32" style="display:block;border:0;text-decoration:none" src="https://ci4.googleusercontent.com/proxy/FiE_kfWq9W8UDhTUUqMY1w2T-zl_sizK3vAQOZhchvBp21xDWoLxrqvHfLkBxQCIydymtuvP1OKRR6uNZRBzH-JH348tLOTMbQ0Yc5H_HQEUTu1NhoiAPi4sP_LLajs1N7Vs6XZfWoZWVHWGr5VrDufB55DhowJ1tMy6vomACkCNE9o_tRliUfRIwnNOOQ=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/instagram-circle-colored.png" class="CToWUd"></a></td><td align="center" valign="top" style="padding:0;Margin:0;padding-right:10px"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="https://www.youtube.com/channel/UClm9mioWT2Q7hoEyiPCG6cg" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmhFxoXGKOv&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNHrJALk3ob0-xInHgUYlk44MjWKDg"><img title="Youtube" alt="Yt" width="32" style="display:block;border:0;text-decoration:none" src="https://ci6.googleusercontent.com/proxy/2ldhPJ-5zngbl2z1L2hOHRHmJwbr9UiidT17RXiBgrWI7kxbuiN_nxlppLL0tMq6GGn-bwI2RrhuXixeGceDvyIy0KlvU4MxEmI9y-0G-hbMYvV-wnhcXDbJ4fCqz_6cxN2FRBDiGlKB1EAdQGxqI9gmEIh73fxJJ3C-GgpQK8BgX_M5qujbiLMU6mU=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/youtube-circle-colored.png" class="CToWUd"></a></td><td align="center" valign="top" style="padding:0;Margin:0"><a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200)" href="http://blog.mobifitness.ru/?utm_source=sait&utm_medium=mobifitness&utm_campaign=sait" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmhKeTmSO8v&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNE7QJjS-N5sCNTw9qqrQhCLHG-nZQ"><img title="Livejournal" alt="Lj" width="32" style="display:block;border:0;text-decoration:none" src="https://ci3.googleusercontent.com/proxy/6utklL0xZAprs36wakaOA8crO3ytIA0A2qFCvGMWF33dZwTuR5zmGjQDUw5ZPAenYhEj4EO3md3Mh6ZtRPur9-OOyGJsn530LjcmA5g5LR09J7EAUKlfyzq7xxrXjC8RqIn4vz90zWZS3Ki9NHdJC9_nt2EfEmxgbRG5wAZVakzAwdx3gAlJGGegpSLaB3_h=s0-d-e1-ft#https://esputnik.com/content/message-builder/stripe//assets/img/social-icons/circle-colored/livejournal-circle-colored.png" class="CToWUd"></a></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table></td>
                                                            </tr>
                                                            <tr style="border-collapse:collapse">
                                                                <td align="center" style="padding:0;Margin:0;padding-top:10px">
                                                                <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51)">
                                                                    <span style="color:#ffffff"><strong><span style="font-size:16px;line-height:150%">Подписывайтесь!</span></strong></span>
                                                                </p></td>
                                                            </tr>
                                                        </tbody>
                                                    </table></td>
                                                </tr>
                                            </tbody>
                                        </table></td>
                                    </tr>
                                </tbody>
                            </table></td>
                        </tr>
                    </tbody>
                </table></td>
            </tr>
        </tbody>
    </table>
    <table bgcolor="#ffffff " align="center" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border-spacing:0px;background-color:rgb(255,255,255)">
        <tbody>
            <tr style="border-collapse:collapse">
                <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px">
                <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                    <tbody>
                        <tr style="border-collapse:collapse">
                            <td width="560" align="center" valign="top" style="padding:0;Margin:0">
                            <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                <tbody>
                                    <tr style="border-collapse:collapse">
                                        <td align="center" style="padding:0;Margin:0;display:none">Вставьте сюда блок из вкладки Контент</td>
                                    </tr>
                                </tbody>
                            </table></td>
                        </tr>
                    </tbody>
                </table></td>
            </tr>
        </tbody>
    </table>
    <div lang="ru" style="width:130px;height:75px;display:none">
        <table bgcolor="#ffffff " align="center" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse;border-spacing:0px;background-color:rgb(255,255,255)">
            <tbody>
                <tr style="border-collapse:collapse">
                    <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px">
                    <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                        <tbody>
                            <tr style="border-collapse:collapse">
                                <td width="560" align="center" valign="top" style="padding:0;Margin:0">
                                <table cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-spacing:0px">
                                    <tbody>
                                        <tr style="border-collapse:collapse">
                                            <td align="center" style="padding:0;Margin:0">
                                            <table style="border-collapse:collapse;border-spacing:0px">
                                                <tbody>
                                                    <tr style="border-collapse:collapse">
                                                        <td align="center" style="padding:0;Margin:0;text-align:center">
                                                        <p style="Margin:0;font-size:14px;font-family:arial,'helvetica neue',helvetica,sans-serif;line-height:150%;color:rgb(51,51,51);color:grey">
                                                            <a style="font-family:arial,'helvetica neue',helvetica,sans-serif;font-size:14px;text-decoration:underline;color:rgb(19,118,200);font-size:14px" href="https://mobifitnessru.esclick.me/9EGmhPL91eSev" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ru&amp;q=https://mobifitnessru.esclick.me/9EGmhPL91eSev&amp;source=gmail&amp;ust=1508944366703000&amp;usg=AFQjCNF-rNFA3TNC3Z_LMqjL4HuLX0Rf0A">Отписаться</a>
                                                        </p></td>
                                                    </tr>
                                                </tbody>
                                            </table></td>
                                        </tr>
                                    </tbody>
                                </table></td>
                            </tr>
                        </tbody>
                    </table></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
HTML;
	
	$title = 'Заявка с сайта Mobifitness.ru';
	if(isset($_POST['title'])){
		$title = $_POST['title'];
	}

	if (isset($_POST['email']) && $_GET['action'] != 'lmp'){
		$mails = array();
		$mails[$_POST['email']] = $_POST['email'];
		//LoveisMailer::send_mail_html($title, $html , $mails);
	}

	header('content-type: text/plain; charset=utf-8');
	echo 'ok';

	function subscribe($data = array(), $groupName){
        if(!empty($data)){
            $user = 'apopova@mobifitness.ru'; // логин
            $password = 'mobifitness123'; // пароль

            $first_name = $data['name'];
            $email = $data['email'];	// email контакта
            $subscribe_contact_url = 'https://esputnik.com/api/v1/contact/subscribe';
            $event_contact_url = 'https://esputnik.com/api/v1/contact/subscribe';

            $json_contact_value = new stdClass();
            $contact = new stdClass();
            $contact->firstName = $first_name;
            $contact->channels = array(
                array('type'=>'email', 'value' => $email)
            );
            $json_contact_value->contact = $contact;
            $json_contact_value->groups = array($groupName);	// группы, в которые будет помещен контакт
            //echo "<pre>"; print_r($json_contact_value); echo "</pre>";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_contact_value));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_URL, $subscribe_contact_url);
            curl_setopt($ch,CURLOPT_USERPWD, $user.':'.$password);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            $code=curl_getinfo($ch,CURLINFO_HTTP_CODE);
            //echo($output);
            //echo($code);
            curl_close($ch);

            /*$contact_id = json_decode($output, 1);
            echo "<pre>"; print_r($contact_id); echo "</pre>";
            $json_event_value = new stdClass();
            $json_event_value->eventTypeKey = 'lmp';
            $json_event_value->keyValue = $email;
            $json_event_value->params = array(
                array('name'=>'EmailAddress', 'value' => $email),
                array('name'=>'firstName', 'value' => $first_name),
                array('name'=>'code', 'value' => $contact_id['id'])
            );
            echo "<pre>"; print_r($json_event_value); echo "</pre>";

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_event_value));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_URL, $event_contact_url);
            curl_setopt($ch,CURLOPT_USERPWD, $user.':'.$password);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            $code=curl_getinfo($ch,CURLINFO_HTTP_CODE);
            echo($output);
            echo($code);
            curl_close($ch);*/

            if ($groupName != 'Цепочка с главного сайта') {
                return;
            }

            $event = new stdClass();
            $event->eventTypeKey = 'eventFromApi'; 
            $event->keyValue = $email;
            $event->params = array(array('name' => 'EmailAddress', 'value' => $email));//$respArray['id']));
            
            // Запуск события
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($event));
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json;charset=UTF-8'));
            curl_setopt($ch, CURLOPT_URL, 'https://esputnik.com/api/v1/event');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch,CURLOPT_USERPWD, $user.':'.$password);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);
            $info = curl_getinfo($ch);
        //    var_dump($info, $output);
            curl_close($ch);
        }
    }

?>
