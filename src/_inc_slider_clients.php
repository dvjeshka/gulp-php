<section class="section-text g-section-margin">
    <div class="g-grid">
        <div class="g-row g-row_flex g-ai_c_md g-jc_sb_lg">
            <div class="g-col g-col_md_5 g-or_1_md">
                <div>
                    <?= $sliderClientsText ?>
                </div>
            </div>
            <div class="g-col g-col_md_6 ">
                <!--       <div class="slider-clients__item"><div style="background-image:url(<? /*=$p."img/clients/1.jpg"*/ ?>)" class="slider-clients__img"></div></div>
                        <div class="slider-clients__item"><div style="background-image:url(<? /*=$p."img/clients/2.jpg"*/ ?>)" class="slider-clients__img"></div></div>
                        <div class="slider-clients__item"><div style="background-image:url(<? /*=$p."img/clients/3.jpg"*/ ?>)" class="slider-clients__img"></div></div>
                        <div class="slider-clients__item"><div style="background-image:url(<? /*=$p."img/clients/4.jpg"*/ ?>)" class="slider-clients__img"></div></div>
                        <div class="slider-clients__item"><div style="background-image:url(<? /*=$p."img/clients/5.jpg"*/ ?>)" class="slider-clients__img"></div></div>
                        <div class="slider-clients__item"><div style="background-image:url(<? /*=$p."img/clients/6.jpg"*/ ?>)" class="slider-clients__img"></div></div>
                        <div class="slider-clients__item"><div style="background-image:url(<? /*=$p."img/clients/7.jpg"*/ ?>)" class="slider-clients__img"></div></div>
                        <div class="slider-clients__item"><div style="background-image:url(<? /*=$p."img/clients/8.jpg"*/ ?>)" class="slider-clients__img"></div></div>
                        <div class="slider-clients__item"><div style="background-image:url(<? /*=$p."img/clients/9.jpg"*/ ?>)" class="slider-clients__img"></div></div>-->


                <div class="slider-clients__wrap g-col_md_12">
                    <div class="slider-clients "
                         data-slick='{"rows":3,"dots":false,"slidesToShow": 3, "slidesToScroll": 3,"dots": true, "fade": false,"arrows":true,"responsive": [{"breakpoint": 767,"settings":{"slidesToShow": 2, "slidesToScroll": 2,"arrows": false}}]}'>

                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/10.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/11.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/12.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/13.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/14.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/15.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/16.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/17.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/18.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/19.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/20.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/21.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/22.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/23.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/24.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/25.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/26.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/27.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/28.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/29.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/30.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/31.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/32.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/33.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/34.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/35.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/36.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/37.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/38.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/39.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/40.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/41.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/42.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/43.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/44.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/45.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/46.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/47.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/48.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/49.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/50.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/51.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/52.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/53.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                        <div class="slider-clients__item">
                            <div style="background-image:url(<?= $p . "img/clients/54.jpg" ?>)"
                                 class="slider-clients__img"></div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>
</section>