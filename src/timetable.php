<?
$metaTitle = 'Онлайн-запись на занятия в расписании вашего фитнес-клуба';
$metaDescription = 'Система учета для фитнес-клубов: учёт клиентов, расписание тренировок на сайт, онлайн запись на занятия - Mobifitness';
include_once("_inc_header.php"); ?>


    <section class="box-main box-main_raspisanie g-section-margin">
            <div class="g-grid">
                <div class="box-main__box">
                    <div class="g-row">
                        <div class="g-col g-col_md_6"></div>
                        <div class="g-col g-col_md_6">
                            <div class="box-main__text">

                                <h1 class="g-hide_xs g-show_md">Безупречное расписание с возможностью онлайн-записи на
                                    занятия</h1>
                                <div class="h1 g-hide_md">Онлайн-запись <br> на занятия</div>

                                <p>Устанавливается на любой сайт и в соцсети за считанные секунды, цвета
                                    подстраиваются под ваш фирменный стиль.</p>
                                <p></p>

                                <p class="g-d_f g-ai_c_xs box-main__control"><a href="#popup-details"
                                                                                class="btn btn_plr g-mr_2_xs"
                                                                                data-colorbox>Попробовать
                                        бесплатно</a><a href="#popup-consultation" class="g-link-js g-fs_sm g-tt_u_xs"
                                                        data-colorbox>Получить
                                        консультацию</a></p>

                                <div class="box-main__img" data-px='{"d":1,"s":1500}'></div>
                                <div class="box-main__circle-decor" data-px='{"d":-1,"s":250}'>
                                    всего от
                                    <div class="box-main__circle-decor-title">490</div>
                                    рублей в месяц
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    <div class="section-text-timetable-orders-xs">
        <? $productTargetTitle = "Кто полюбит нашу онлайн-запись на занятия";
        include("_inc_product_target.php"); ?>


        <section class="section-text section-text_online-group section-text_img-abs_left g-section-margin_big">
            <div class="g-grid g-grid_lg">
                <div class="g-pr">

                    <div class="g-col_md_5 section-text__text g-list-wrap_mb_1">
                        <div class="h2 g-hide_md g-d_b" hidden>
                            Онлайн-запись<br>
                            на групповые занятия
                        </div>
                        <h2 class="g-hide_xs g-show_ib_md">
                            Функции виджета онлайн-записи<br>
                            на групповые занятия
                        </h2>
                        <div class=" section-text__img-abs g-col_md_5">
                            <div class="slider-screen-img">
                                <div class="slider-screen"
                                     data-slick='{"dots":false,"slidesToShow": 1, "slidesToScroll": 1,"dots": false, "fade": false,"arrows":true,"responsive": [{"breakpoint": 767,"settings":{"slidesToShow": 2, "slidesToScroll": 2,"arrows": false}}]}'>
                                    <div class="slider-screen__item">
                                        <div style="background-image:url(<?= $p . "img/page_raspisanie/screen-mac/1.png" ?>)"
                                             class="slider-screen__img"></div>
                                    </div>
                                    <div class="slider-screen__item">
                                        <div style="background-image:url(<?= $p . "img/page_raspisanie/screen-mac/3.png" ?>)"
                                             class="slider-screen__img"></div>
                                    </div>
                                    <div class="slider-screen__item">
                                        <div style="background-image:url(<?= $p . "img/page_raspisanie/screen-mac/2.png" ?>)"
                                             class="slider-screen__img"></div>
                                    </div>
                                    <div class="slider-screen__item">
                                        <div style="background-image:url(<?= $p . "img/page_raspisanie/screen-mac/6.png" ?>)"
                                             class="slider-screen__img"></div>
                                    </div>
                                    <div class="slider-screen__item">
                                        <div style="background-image:url(<?= $p . "img/page_raspisanie/screen-mac/4.png" ?>)"
                                             class="slider-screen__img"></div>
                                    </div>
                                    <div class="slider-screen__item">
                                        <div style="background-image:url(<?= $p . "img/page_raspisanie/screen-mac/5.png" ?>)"
                                             class="slider-screen__img"></div>
                                    </div>
                                </div>

                                </div>


                        </div>
                        <ul data-dots-listener='{"parent":".section-text","target":".slider-screen"}'>
                                <li>изменение расписания в режиме реального времени</li>
                                <li>фильтры по направлениям занятий и тренерам</li>
                                <li>запись на групповые занятия прямо из расписания</li>
                                <li>информация о тренерах и описание занятий</li>
                                <li>видео занятий</li>
                                <li>печать расписания с сайта и соц. сетей</li>
                            <li class="li-decor-no">и многое другое</li>
                            </ul>
                        <p class="g-hidden g-show_md"><a href="#popup-request" class="btn btn-gr" data-colorbox>Экономьте
                                время сотрудников</a></p>
                        </div>

                </div>
            </div>
        </section>

        <section class="section-text section-text_online-personal section-text_img-abs_right g-section-margin_big">
            <div class="g-grid g-grid_lg">
                <div class="g-pr">
                    <div class="g-col_md_5 section-text__text g-list-wrap_mb_1">
                        <div class="h2 g-hide_md g-d_b" hidden>
                            Онлайн-запись<br>
                            на персональные занятия
                        </div>
                        <h2 class="g-hide_xs g-show_ib_md">
                            Функции виджета онлайн-записи<br>
                            на персональные занятия
                        </h2>


                        <div class=" section-text__img-abs g-col_md_5">
                            <div class="slider-screen-img">
                                <div class="slider-screen"
                                     data-slick='{"dots":false,"slidesToShow": 1, "slidesToScroll": 1,"dots": false, "fade": false,"arrows":true,"responsive": [{"breakpoint": 767,"settings":{"slidesToShow": 2, "slidesToScroll": 2,"arrows": false}}]}'>
                                    <div class="slider-screen__item">
                                        <div style="background-image:url(<?= $p . "img/page_raspisanie/screen-mac2/1.png" ?>)"
                                             class="slider-screen__img"></div>
                                    </div>
                                    <div class="slider-screen__item">
                                        <div style="background-image:url(<?= $p . "img/page_raspisanie/screen-mac2/2.png" ?>)"
                                             class="slider-screen__img"></div>
                                    </div>
                                    <div class="slider-screen__item">
                                        <div style="background-image:url(<?= $p . "img/page_raspisanie/screen-mac2/3.png" ?>)"
                                             class="slider-screen__img"></div>
                                    </div>
                                    <div class="slider-screen__item">
                                        <div style="background-image:url(<?= $p . "img/page_raspisanie/screen-mac2/4.png" ?>)"
                                             class="slider-screen__img"></div>
                                    </div>
                                    <div class="slider-screen__item">
                                        <div style="background-image:url(<?= $p . "img/page_raspisanie/screen-mac2/5.png" ?>)"
                                             class="slider-screen__img"></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <ul data-dots-listener='{"parent":".section-text","target":".slider-screen"}'>
                            <li>запись на персональную тренировку</li>
                            <li>выбор тренера по графику работы</li>
                            <li>информация о тренере</li>
                            <li>описание занятий</li>
                            <li>авторизация по телефону</li>
                        </ul>
                        <p><a href="#popup-request" class="btn btn-gr" data-colorbox>Экономьте время сотрудников</a></p>
                    </div>
                </div>
            </div>
        </section>
    </div>



        <? include_once("_inc_form_box.php"); ?>


        <section class="section-text section-text_refresh g-section-margin_big">
            <div class="g-grid">
                <div class="g-pr">
                    <div class="g-col_md_5">
                        <h2>Забудьте о том, что расписание
                            нужно обновлять в нескольких
                            местах
                        </h2>
                        <div class=" section-text__img-abs g-col_md_6 g-col-offset_md_6">
                            <img src="<?= $p . "img/page_raspisanie/raspisanie_refresh.png" ?>"
                                 alt="Забудьте о том, что расписание нужно обновлять в нескольких местах">
                        </div>
                        <p>Одно из главных преимуществ виджета онлайн-записи от Mobifitness – современная и очень
                            простая в использовании панель управления, единая для вашего сайта, социальных сетей и
                            мобильного приложения.</p>

                        <p>Виджет онлайн-записи интегрируется с вашей клубной системой.</p>
                    </div>
                </div>
            </div>
        </section>

    <section class="slider-examples g-ta_c_xs g-section-margin g-hide_xs g-show_md">
            <div class="g-grid">
                <h2 class="g-ta_c_xs">Примеры для сайта и социальных сетей</h2>
                <p>Цвета и дизайн подстраиваются под ваш фирменный стиль</p>
                <ul class="slider-examples__btns"
                    data-dots-listener='{"parent":".slider-examples","target":".slider-examples__slider"}'>
                    <li class="slider-examples__btn slider-examples__btn_1">Расписание <br> для сайта</li>
                    <li class="slider-examples__btn slider-examples__btn_2">Расписание <br> для соцсетей</li>
                </ul>
                <div class="slider-examples__slider"
                     data-slick='{"slidesToShow": 1, "slidesToScroll": 1,"dots": false, "fade": false,"arrows":false,"responsive": [{"breakpoint": 767,"settings":{"slidesToShow": 2, "slidesToScroll": 2,"arrows": false}}]}'>
                    <div>
                        <div class="slider-examples__item">
                            <img class="slider-examples__img"
                                 src="<?= $p . "img/page_raspisanie/examples_phone_1.png" ?>"
                                 alt="Расписание для сайта">
                            <img class="slider-examples__img" src="<?= $p . "img/page_raspisanie/form-box_mac.png" ?>"
                                 alt="Расписание для сайта">
                        </div>
                    </div>
                    <div>
                        <div class="slider-examples__item">
                            <img class="slider-examples__img"
                                 src="<?= $p . "img/page_raspisanie/examples_phone_2.png" ?>"
                                 alt="Расписание для соцсетей">
                            <img class="slider-examples__img" src="<?= $p . "img/page_raspisanie/form-box_mac_2.png" ?>"
                                 alt="Расписание для соцсетей">
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <div class="g-hidden g-show_md">
        <? $formLineVariant = 1;
        include("_inc_form_line.php"); ?>
    </div>


        <section class="text-box-img g-section-margin">
            <div class="g-grid">
                <div class="text-box-img__box text-box-img__box_girl">
                    <div class="text-box-img__img-wrap">
                        <div class="text-box-img__text">
                            <h2>Экономьте время и повышайте
                                лояльность клиентов</h2>
                            <ul class="">
                                <li>
                                    <h3 class="g-clr_4">Экономит время сотрудников клуба</h3>
                                    <p>- Расписание редактируется прямо в админ-панели,<br class="g-hidden g-show_md">
                                        после чего оно
                                        автоматически
                                        меняется на сайте<br class="g-hidden g-show_md"> и в социальных сетях. <br>
                                        - Клиенты могут записываться на занятия прямо из<br class="g-hidden g-show_md">
                                        расписания, что
                                        значительно
                                        разгружает ресепшн.
                                    </p>
                                </li>
                                <li>
                                    <h3 class="g-clr_4">Минимизирует уровень недовольных клиентов</h3>
                                    <p>- Все отмены и переносы занятий автоматически<br class="g-hidden g-show_md">&nbsp;появляются
                                        в
                                        расписании, клиенты
                                        больше не<br class="g-hidden g-show_md"> запутаются.</p>
                                </li>
                                <li>
                                    <h3 class="g-clr_4">Значительно упрощает работу сотрудников</h3>
                                    <p>- Больше не нужно заполнять таблицы в Excel. <br>
                                        - Распечатать расписание можно прямо с сайта и<br class="g-hidden g-show_md">
                                        соцсетей, нажав
                                        всего одну
                                        кнопку.</p>
                                </li>
                            </ul>

                        </div>
                    </div>

                    <div class="text-box-img__btn g-ta_r_md g-mr_4_md">
                        <a href="#popup-request" data-colorbox class="btn btn-icon-arrow g-ta_l_md">
                            Оставь заявку и получи доступ<br class="g-hidden g-show_md">
                            на две недели бесплатно
                        </a>
                    </div>

                </div>
            </div>
        </section>

        <section class="rates g-section-margin">
            <div class="g-grid">
                <h2 class="g-ta_c_xs">Тарифы</h2>
                <p class="g-ta_c_xs">Выберите один из наиболее привлекательных для себя <br class="g-hidden g-show_md">
                    тарифов расписания</p>
                <form action="?" class="rates__form rates__form_height">
                    <ul class="rates__list">
                        <li class="rates__item rates__item_1">
                            <div class="rates__box">
                                <div class="rates__label">Расписание для</div>
                                <h3 class="rates__name">Соцсетей</h3>
                                <div class="rates__soc">VK/FB/Instagram</div>
                                <div class="rates__price">
                                    <fieldset class="rates__select js-rates__select">
                                        <strong class="rates__select-name g-link-js"
                                        >
                                            <span class="h1">490</span> руб./мес. <span
                                                    class="rates__select-arrow"><span
                                                        class="g-icon-down"></span>   </span></strong>

                                        <ul class="rates__select-list">
                                            <li class="rates__select-item">
                                                <label class="chr">
                                                    <input class="chr__input" type="radio" name="rates__item_1"
                                                           value="1 год">
                                                    <span class="chr__text">
                                                        <span class="h1" data-percent="45">490</span> руб. / месяц
                                                        <span class="rates__label">При оплате за год</span>
                                                    </span>
                                                </label>
                                            </li>
                                            <li class="rates__select-item">
                                                <label class="chr">
                                                    <input class="chr__input" type="radio" name="rates__item_1"
                                                           value="6 месяцев">
                                                    <span class="chr__text">
                                                        <span class="h1" data-percent="23">690</span> руб. / месяц
                                                        <span class="rates__label">При оплате за полгода</span>
                                                    </span>
                                                </label>
                                            </li>
                                            <li class="rates__select-item">
                                                <label class="chr">
                                                    <input class="chr__input" type="radio" name="rates__item_1"
                                                           value="3 месяца">
                                                    <span class="chr__text">
                                                        <span class="h1" data-percent="0">890</span> руб. / месяц
                                                        <span class="rates__label">При оплате за 3 месяца</span>
                                                    </span>
                                                </label>
                                            </li>
                                        </ul>
                                    </fieldset>
                                </div>
                                <div class="rates__saving">
                                    <ul>
                                        <li>при оплате за год <br> экономия <span class="rates__saving-numb">45</span>%
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="rates__btn-wrap">
                                <a href="#popup-request-rate"
                                   onclick="func.jsSetTariff('Расписание для соц.сетей','rates__item_1')"
                                   class="rates__btn btn btn_plr" data-colorbox>Выбрать
                                    тариф</a>
                            </div>

                        </li>

                        <li class="rates__item rates__item_2">
                            <div class="rates__box">
                                <div class="rates__label">Расписание для</div>
                                <h3 class="rates__name">Сайта и соцсетей</h3>
                                <div class="rates__soc"></div>
                                <div class="rates__price">
                                    <fieldset class="rates__select js-rates__select">
                                        <strong class="rates__select-name g-link-js"
                                        >
                                            <span class="h1">1 390</span> руб./мес. <span
                                                    class="rates__select-arrow"><span
                                                        class="g-icon-down"></span>   </span></strong>

                                        <ul class="rates__select-list">
                                            <li class="rates__select-item">
                                                <label class="chr">
                                                    <input class="chr__input" type="radio" name="rates__item_3"
                                                           value="1 год">
                                                    <span class="chr__text">
                                                        <span class="h1" data-percent="35">1 390 </span> руб. / месяц
                                                        <span class="rates__label">При оплате за год</span>
                                                    </span>
                                                </label>
                                            </li>
                                            <li class="rates__select-item">
                                                <label class="chr">
                                                    <input class="chr__input" type="radio" name="rates__item_3"
                                                           value="6 месяцев">
                                                    <span class="chr__text">
                                                        <span class="h1" data-percent="18">1 880</span> руб. / месяц
                                                        <span class="rates__label">При оплате за полгода</span>
                                                    </span>
                                                </label>
                                            </li>
                                            <li class="rates__select-item">
                                                <label class="chr">
                                                    <input class="chr__input" type="radio" name="rates__item_3"
                                                           value="3 месяца">
                                                    <span class="chr__text">
                                                        <span class="h1" data-percent="0">2 280</span> руб. / месяц
                                                        <span class="rates__label">При оплате за 3 месяца</span>
                                                    </span>
                                                </label>
                                            </li>
                                        </ul>
                                    </fieldset>
                                </div>
                                <div class="rates__saving">
                                    <ul>
                                        <li>при оплате за год <br> экономия <span class="rates__saving-numb">35</span>%
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="rates__btn-wrap">
                                <a href="#popup-request-rate"
                                   onclick="func.jsSetTariff('Расписание для соц.сетей','rates__item_3')"
                                   class="rates__btn btn btn_plr" data-colorbox>Выбрать
                                    тариф</a>
                            </div>
                        </li>
                        <li class="rates__item rates__item_3">
                            <div class="rates__box">
                                <div class="rates__label">Расписание для</div>
                                <h3 class="rates__name">Сайта</h3>
                                <div class="rates__soc"></div>
                                <div class="rates__price">
                                    <fieldset class="rates__select js-rates__select">
                                        <strong class="rates__select-name g-link-js"
                                        >
                                            <span class="h1">990</span> руб./мес. <span
                                                    class="rates__select-arrow"><span
                                                        class="g-icon-down"></span>   </span></strong>

                                        <ul class="rates__select-list">
                                            <li class="rates__select-item">
                                                <label class="chr">
                                                    <input class="chr__input" type="radio" name="rates__item_2"
                                                           value="1 год">
                                                    <span class="chr__text">
                                                        <span class="h1" data-percent="29">990</span> руб. / месяц
                                                        <span class="rates__label">При оплате за год</span>
                                                    </span>
                                                </label>
                                            </li>
                                            <li class="rates__select-item">
                                                <label class="chr">
                                                    <input class="chr__input" type="radio" name="rates__item_2"
                                                           value="6 месяцев">
                                                    <span class="chr__text">
                                                        <span class="h1" data-percent="15">1 190</span> руб. / месяц
                                                        <span class="rates__label">При оплате за полгода</span>
                                                    </span>
                                                </label>
                                            </li>
                                            <li class="rates__select-item">
                                                <label class="chr">
                                                    <input class="chr__input" type="radio" name="rates__item_2"
                                                           value="3 месяца">
                                                    <span class="chr__text">
                                                        <span class="h1" data-percent="0">1 390</span> руб. / месяц
                                                        <span class="rates__label">При оплате за 3 месяца</span>
                                                    </span>
                                                </label>
                                            </li>
                                        </ul>
                                    </fieldset>
                                </div>
                                <div class="rates__saving">
                                    <ul>
                                        <li>при оплате за год <br> экономия <span class="rates__saving-numb">29</span>%
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="rates__btn-wrap">
                                <a href="#popup-request-rate"
                                   onclick="func.jsSetTariff('Расписание для сайта','rates__item_2')"
                                   class="rates__btn btn btn_plr" data-colorbox>Выбрать
                                    тариф</a>
                            </div>
                        </li>
                    </ul>
                </form>

            </div>
        </section>

        <? $sliderClientsText = '
    <h2>Фитнес клубы, которые<br>присоединились<br>к Mobifitness</h2>
    <p>У нас уже более <span data-target="clients">555</span> клиентов по России и СНГ. <br> Среди них, как небольшие одиночные клубы, так и крупные федеральные сети фитнес-клубов.
</p>    
    ';
        include_once("_inc_slider_clients.php"); ?>

<? include_once("_inc_reviews_items_raspisanie.php"); ?>
<? include_once("_inc_footer.php"); ?>