<section class="form-box <?= $formBoxClass ?> g-section-margin">
    <div class="g-grid">
        <div class="form-box__box">
            <div class="g-row">
                <div class="g-col g-col_md_6">

                </div>
                <div class="g-col g-col_md_6">
                    <? if ($formBoxClass) { //Учетная система?>

                        <h2 class="h2_lg">ПОПРОБУЙТЕ БЕСПЛАТНО!</h2>
                        <p>Оставьте заявку и получите полноценный доступ <br class="g-hidden g-show_md"> к учетной
                            системе на две недели.</p>
                    <? } else { //Расписание?>
                        <h2 class="h2_lg">ПОПРОБУЙТЕ БЕСПЛАТНО!</h2>
                        <p>Оставьте заявку и получите полноценный доступ <br class="g-hidden g-show_md"> к расписанию с
                            онлайн-записью на две
                            недели.</p>
                    <? } ?>
                    <form class="form-dark js-validated form-box__form " action="?">
                        <fieldset class="">
                            <ul class="form-line__list g-row">
                                <li class="form-line__item g-col">
                                    <label>
                                        <span class="form-dark__label">Имя*</span>
                                        <input type="text" class="form-dark__field" name="name">
                                    </label>
                                </li>
                                <li class="form-line__item g-col">
                                    <label>
                                        <span class="form-dark__label">Телефон*</span>
                                        <input type="tel" class="form-dark__field js-mask" name="phone">
                                    </label>
                                </li>
                                <li class="form-line__item g-col">
                                    <label>
                                        <span class="form-dark__label">E-mail*</span>
                                        <input type="email" class="form-dark__field js-mask"
                                               name="email">
                                    </label>
                                </li>
                                <li class="form-line__item g-col">

                                    <? if ($formBoxClass) { ?>
                                        <input type="submit" class="btn form-box__btn" value="Попробовать бесплатно">
                                    <? } else { ?>
                                        <input type="submit" class="btn form-box__btn" value="Получить расписание">
                                    <? } ?>
                                </li>
                            </ul>
                        </fieldset>
                        <input type="hidden" name="group" value="free-schedule">
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>