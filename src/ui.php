<? include_once("_inc_header.php"); ?>
<? include_once("_inc_breadcrumbs.php"); ?>
    <div class="d-grid text-content ">
        <div class="d-row ">
            <div class="d-col_6 d-col">
                <h1>H1 Заголовок <br> с переносом строки</h1>
                <p>параграф параграф <strong>жирненький текст</strong> параграф параграф <i>курсив текст</i>
                    <a href="#">ссылка</a> параграф
                    <small>small text</small>
                    параграф параграф параграф параграф параграф параграф параграф параграф
                </p>
                <h2>H2 Заголовок <br> с переносом строки
                    <small>small text</small>
                </h2>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>
                <h3>H3 Заголовок <br> с переносом строки
                    <small>small text</small>
                </h3>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>
                <h4>H4 Заголовок <br> с переносом строки
                    <small>small text</small>
                </h4>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>
                <h5>H5 Заголовок <br> с переносом строки
                    <small>small text</small>
                </h5>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>
                <h6>H6 Заголовок <br> с переносом строки
                    <small>small text</small>
                </h6>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>
                <p><img src="upload/slider1.jpg" alt="Картинка"></p>
                <p><img src="upload/slider1.jpg" alt="Картинка"></p>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>
                <hr>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>
                <ol>
                    <li>ol li</li>
                    <li>ol li пункт списка <br> c переносом строки</li>
                    <li>ol li</li>
                </ol>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>
                <ul>
                    <li>ul li</li>
                    <li>ol li пункт списка <br> c переносом строки</li>
                    <li>ul li</li>
                </ul>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>
                <p>
                <blockquote>&lt;blockquote&gt;Блочная цитата Блочная цитата Блочная цитата Блочная цитата Блочная цитата
                    Блочная цитата Блочная цитата Блочная цитата&lt;/blockquote&gt;
                </blockquote>
                </p>

                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>
                <table>
                    <thead>
                    <tr>
                        <th>th</th>
                        <th>th</th>
                        <th>th</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>td</td>
                        <td>td</td>
                        <td>td</td>
                    </tr>
                    <tr>
                        <td>td</td>
                        <td>td</td>
                        <td>td</td>
                    </tr>
                    </tbody>
                </table>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>

            </div>
            <div class="d-col_6 d-col">
                <h1>Показать растояние между двумя заголовками если оно не стандартное</h1>
                <h2>H2 Заголовок</h2>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>
                <h2>H2 Заголовок</h2>
                <h3>H3 Заголовок</h3>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>
                <h2>H2 Заголовок</h2>
                <h4>H4 Заголовок</h4>
                <p>параграф параграф параграф параграф параграф параграф параграф параграф параграф параграф</p>


            </div>
        </div>
        <hr/>
        <hr/>
        <hr/>
        <hr/>
        <hr/>
        <hr/>
        <p>
            <small>small text</small>
        </p>
        <p><a href="javascript:">ссылка</a></p>
        <p><a href="tel:88008000000">88008000000</a></p>
        <ul class="list">
            <li>ul class="list" li</li>
            <li>ul class="list" li</li>
        </ul>
        <dl>
            <dt>dt</dt>
            <dd>dd</dd>
            <dt>dt</dt>
            <dd>dd</dd>
        </dl>

        <form action="">
            <fieldset class="filter-block">
                <legend class="h6 d-ttu filter-block__name">Ценовой диапозон</legend>
                <div class="noUi-mod" data-min="500" id="js-range-input" data-max="20000" data-step="1"
                     data-start="500,50000"></div>
                <div class="filter-block__ambit">
                    <div class="filter-block-ambit">
                        <div class="d-ib filter-block-ambit__item">
                            <span class="filter-block-ambit__text">от</span>
                            <input type="text" class="filter-block-ambit__input" id="js-range-input-min" value="11 500">
                        </div>
                        <div class="d-ib filter-block-ambit__item">
                            <span class="filter-block-ambit__text">до</span>
                            <input type="text" class="filter-block-ambit__input" id="js-range-input-max"
                                   value="259 550">
                        </div>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <ul>
                    <li><input type="text" name="" value="" placeholder="input[type='text']"/></li>
                    <li><input type="search" name="" value="" placeholder="input[type='search']"/></li>
                    <li><input type="reset" name="" value="" placeholder="input[type='reset']"/></li>
                    <li><input type="password" name="" value="" placeholder="input[type='password']"/></li>
                    <li><input type="email" name="" value="" placeholder="input[type='email']"/></li>
                    <li><select>
                            <option>select</option>
                            <option>select</option>
                        </select></li>
                    <li><select data-select>
                            <option>option 0</option>
                            <option>option 1</option>
                        </select></li>
                    <li><textarea name="" placeholder="textarea"></textarea></li>
                </ul>
            </fieldset>
            <fieldset>
                <label class="d-db"><input type="checkbox"><span class="chr-text">Тариф «Упрямство»</span></label>
                <label class="d-db"><input type="checkbox"><span class="chr-text">Тариф «Упрямство»</span></label>
                <label class="d-db"><input type="radio" name="radio1"><span
                            class="chr-text">Тариф «Упрямство»</span></label>
                <label class="d-db"><input type="radio" name="radio1"><span
                            class="chr-text">Тариф «Упрямство»</span></label>
            </fieldset>
            <input type="submit" name="" value="submit"/>
            <input type="submit" name="" value="submit btn_o" class="btn_o"/>
            <a href="javascript:" class="btn">btn</a>
            <a href="javascript:" class="btn btn_o">btn_o</a>
            <a href="javascript:" class="btn btn_bg_1">btn btn_bg_1</a>

        </form>

        <nav class="nav-pages d-tac">
            <ul>
                <li class="nav-pages__prev"><a href="javascript:">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 21" enable-background="new 0 0 12 21">
                            <path d="M0 10.5c0-.2.1-.4.2-.5l9.6-9.8c.2-.3.6-.3.9 0l1 1.1c.3.3.3.7 0 .9l-8.1 8.3 8.1 8.3c.1.1.2.3.2.5s-.1.4-.2.5l-1 1.1c-.2.3-.6.3-.9 0l-9.6-9.9c-.1-.1-.2-.3-.2-.5z"/>
                        </svg>
                    </a></li>
                <li><a href="javascript:" class="current">1</a></li>
                <li><a href="javascript:">2</a></li>
                <li><a href="javascript:">3</a></li>
                <li><a href="javascript:">...</a></li>
                <li><a href="javascript:">9</a></li>
                <li class="nav-pages__next"><a href="javascript:">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 21" enable-background="new 0 0 12 21">
                            <path d="M12 10.5c0-.2-.1-.4-.2-.5l-9.6-9.8c-.2-.3-.6-.3-.9 0l-1 1.1c-.3.3-.3.7 0 .9l8.1 8.3-8.1 8.3c-.2.1-.3.3-.3.5s.1.4.2.5l1 1.1c.2.3.6.3.9 0l9.6-9.8c.2-.2.3-.4.3-.6z"/>
                        </svg>
                    </a></li>
            </ul>
        </nav>
        <p><a href="#" class="d-lnk-js h5 d-lc_2"
              data-toggle='{"animation":"js","parent":".vacancy-item","target":".vacancy-item__more", "className":"vacancy-item_open","textHide":"свернуть описание","textShow":"Подробнее"}'>Подробнее</a>
        </p>
        <p><a href="javascript:" data-colorbox='{"href": "./img/ajax-loader.gif", "className": "asd"}'>img</a></p>
        <p><a href="javascript:" data-colorbox='{"href": "#popup-inline", "inline": "true"}'>inline with href</a></p>

        <div id="popup-inline">inline</div>
        <a href="1.pdf"  data-colorbox='{"iframe":true,"inline":false}'>1</a>
        <table>
            <caption> <span><button></button></span></caption>
        </table>
    </div>

<? include_once("_inc_footer.php"); ?>