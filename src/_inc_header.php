<?

$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";
$protocol = 'https';

$referer = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';

$p = $protocol . '://new.mobifitness.ru/';
$p = '';

?>

<!doctype html>
<html lang="ru" class="no-js">
<head>
    <title><?= ($metaTitle) ? $metaTitle : 'markup' ?></title>
    <meta name="description" content="<?= $metaDescription ?>">
    <meta charset="utf-8"/>
    <meta name="author" content=""/>
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
    <meta name="format-detection" content="telephone=no"/>

    <meta id="viewport" name="viewport" content="width=device"/>


    <script>


        function getDevice() {
            


            var idViewport = document.getElementById('viewport');
            var mql = window.matchMedia("(orientation: portrait)");

            function mobile(action) {
                var tagHtml = document.getElementsByTagName('html')[0];
                if (action == 'remove'){
                    idViewport.setAttribute('content', 'width=device');
                    tagHtml.classList.remove('mobile');

                    setTimeout(function () {
                        tagHtml.classList.remove('is-header-trans')
                    },100)

                } else {
                    tagHtml.classList.add('mobile');
                    idViewport.setAttribute('content', 'width=device-width')
                    setTimeout(function () {
                        tagHtml.classList.add('is-header-trans')
                    },100)
                }
            }


            if (!mql.matches) {

                return mobile('remove');
            }
            if (/Android|webOS|iPhone|iPod|Opera Mini/i.test(navigator.userAgent)) {

                return  mobile();
            }


            action(window.outerWidth)

            action(window.innerWidth)  //на планшете ipad косяк был

            function action(width) {

                //if (width=0) return
                var text = ''
                if (width <= 767) {
                    mobile()
                    text = "add mobile";
                }
                else {
                    mobile('remove')
                    text = "remove mobile";
                }
                console.log(width + text)
            }



        }

        getDevice();
        window.onresize = function (event) {

            if (/iPhone|iPod/i.test(navigator.userAgent)) {
                //console.log(navigator.userAgent);
                //document.location.reload()
            }
            getDevice()

        };


    </script>

    <!--[if IE]>
    <meta http-equiv="imagetoolbar" content="no"/>
    <![endif]-->

    <link rel="shortcut icon" href="<?= $p . "favicon.ico" ?>"/>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i&amp;subset=cyrillic"
          type="text/css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Russo+One&amp;subset=cyrillic" rel="stylesheet">


    <link rel="stylesheet" href="<?= $p . "css/main.css" ?>"/>
    <link rel="stylesheet" href="<?= $p . "css/custom.css" ?>"/>

    <link rel="canonical" href="https://mobifitness.ru<?= parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) ?>"/>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="<?= $p . "js/plugins.js" ?>"></script>
    <script src="<?= $p . "js/main.js" ?> "></script>
    <script src="<?= $p . "js/amo.js" ?> "></script>

    <!-- Google Analytics -->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-90582890-1', 'auto');
        ga('send', 'pageview');
    </script>
    <!-- End Google Analytics -->
    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '203922590441157');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" src="https://www.facebook.com/tr?id=203922590441157&ev=PageView&noscript=1">
    </noscript>
    <!-- End Facebook Pixel Code -->
</head>

<body class="page <?= (isset($GLOBALS["bodyClass"])) ? $GLOBALS["bodyClass"] : '' ?>">
<div class="page__ow">


    <div class="page__wrap">
        <div class="page__nav-main-toggle">
            <a href="/" class="page__nav-main-toggle__link"></a>
            <div class="nav-main-toggle js-nav-main-toggle"><span class="nav-main-toggle__icon"></span></div>
        </div>

        <header class="page__header header">

            <div class="g-grid header__grid-top">
                <div class="header__top">
                    <? include("_inc_soc.php"); ?>

                    <ul class="header__control">
                        <li class="header__control-item"><a
                                    href="https://academy.mobifitness.ru/?utm_source=menu&utm_medium=mobifitness&utm_campaign=menumobifitness"
                                    target="_blank"
                                    class="header__control-link icon-ak g-link-js-parent"><span
                                        class="g-link-js">Академия Mobifitness</span></a></li>
                        <li class="header__control-item"><a href="<?= $p . "Mobifitness_Presentation.pdf" ?>"
                                                            target="_blank"
                                                            class="header__control-link icon-picture g-link-js-parent"><span
                                        class="g-link-js">Скачать презентацию</span></a></li>
                        <!-- <li class="header__control-item"><a href="#popup-call-back" data-colorbox class="header__control-link icon-phone g-link-js-parent"><span class="g-link-js">Перезвоните мне</span></a></li>-->
                        <li class="header__control-item header__control-item_sign">
                            <ul class="header__sign">
                                <li class="header__sign-item header__sign-item_left"><a target="_blank"
                                                                                        href="https://mobifitness.ru/cp/login"
                                                                                        class="header__control-link g-link-js-parent"><span
                                                class="g-link-js">Вход</span></a></li>
                                <li class="header__sign-item header__sign-item_right"><a href="#popup-request-decor"
                                                                                         data-colorbox
                                                                                         class="header__control-link g-link-js-parent"><span
                                                class="g-link-js">Регистрация</span></a></li>
                            </ul>
                        </li>


                    </ul>
                </div>
            </div>
            <hr class="header__hr">
            <div class="g-grid">
                <? include("_inc_nav_main_blocks.php"); ?>
                <div class="g-row">
                    <div class="g-col_md_2">

                    </div>
                    <div class="g-col_md_10">


                    </div>
                </div>
            </div>
        </header>

        <main class="page__main">
            <article class="page__article">