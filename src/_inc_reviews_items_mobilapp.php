<section class="section-reviews g-section-margin">
    <div class="g-grid">
        <h2 class="g-ta_c_xs">Отзывы</h2>
        <p class="g-ta_c_xs">Клиенты говорят о мобильном приложении для фитнес-клубов</p>
        <div class="g-col_md_10 g-mlr_auto_md">
            <div class="slider-reviews"
                 data-slick='{"respondTo": "min", "slidesToShow": 2,"slidesToScroll": 2,"dots": true, "fade": false,"arrows":true,"responsive": [{"breakpoint": 639,"settings":{"slidesToShow": 1, "slidesToScroll": 1,"arrows": false}}]}'>


                <div class="slider-reviews__item">
                    <a href="#" class="reviews-item g-link-js-parent">
                        <div class="reviews-item__avatar"
                             style="background-image: url(<?= $p . "img/reviews/VR.jpg" ?>);"></div>
                        <div class="reviews-item__text">
                            <p class="reviews-item__name">Валерия Рольник, директор по маркетингу сети фитнес-клубов
                                "Maximus".</p>
                            <p>Через мобильное приложение нашего клуба за 8 месяцев пришло 2500 заявок на заморозку
                                карт, что значительно снизило нагрузку на рецепцию клуба.</p>
                        </div>
                        <span class="g-link-js g-clr_4 g-fs_sm g-hidden">Подробнее</span>
                    </a>
                </div>
                <div class="slider-reviews__item">
                    <a href="#" class="reviews-item g-link-js-parent">
                        <div class="reviews-item__avatar"
                             style="background-image: url(<?= $p . "img/reviews/reviews_item_1.png" ?>);"></div>
                        <div class="reviews-item__text">
                            <p class="reviews-item__name">Евгений Шумихин, digital маркетолог сети фитнес-клубов "Hard
                                Candy Fitness" и "Планета фитнес".</p>
                            <p>Приложение было разработано совместно с сотрудниками фитнес-клубов и в нем нет ничего
                                лишнего. В приложении только то лучшее, что ждет каждый член фитнес-клуба.</p>
                        </div>
                        <span class="g-link-js g-clr_4 g-fs_sm g-hidden">Подробнее</span>
                    </a>
                </div>
                <div class="slider-reviews__item">
                    <a href="#" class="reviews-item g-link-js-parent">
                        <div class="reviews-item__avatar"
                             style="background-image: url(<?= $p . "img/reviews/reviews_item_2.png" ?>);"></div>
                        <div class="reviews-item__text">
                            <p class="reviews-item__name">Ольга Шведова, директор по рекламе сети фитнес-центров "Point
                                Fitness"</p>
                            <p>Мобильное приложение позволяет клиентам фитнес-клубов экономить массу времени. Теперь
                                можно быстро и удобно планировать посещения занятий, оформить заморозку или продление, и
                                забыть о том, что фитнес-карту нужно носить с собой.</p>
                        </div>
                        <span class="g-link-js g-clr_4 g-fs_sm g-hidden">Подробнее</span>
                    </a>
                </div>
                <div class="slider-reviews__item">
                    <a href="#" class="reviews-item g-link-js-parent">
                        <div class="reviews-item__avatar"
                             style="background-image: url(<?= $p . "img/reviews/revies_item_5.png" ?>);"></div>
                        <div class="reviews-item__text">
                            <p class="reviews-item__name">Петров Павел Анатольевич, зам.дир.по развитию "МАУ ДС
                                Ока" </p>
                            <p>В приложении для фитнес-центра от Mobifitness нам удалось реализовать полное расписание
                                работы спорткомплекса, при том что во "Дворце спорта" проходят занятия спортивных секций
                                по мини - футболу, волейболу, настольному теннису и многое другое.</p>
                        </div>
                        <span class="g-link-js g-clr_4 g-fs_sm g-hidden">Подробнее</span>
                    </a>
                </div>

            </div>
        </div>
        <div class="section-reviews__btn g-ta_c_xs">
            <a href="#popup-request-decor" class="btn btn-icon btn-icon_mod" data-colorbox>
                Стать клиентом Mobifitness
            </a>
        </div>
    </div>
</section>