<?
$metaTitle = 'Mobifitness - Другие продукты для вашего клуба';
$metaDescription = 'Эффективные решения для оптимизации вашего спортивного или фитнес-клуба - Mobifitness';
include_once("_inc_header.php"); ?>


    <section class="section-text section-text_other g-section-margin_big">
        <div class="g-grid">
            <div class="g-row g-row_flex g-ai_c_md">
                <div class="g-col g-col_md_6">
                    <div>
                        <h2>Личный кабинет клиента <br> для сайта и соцсетей вашего клуба
                        </h2>
                        <div>
                            <p>Функции мобильного приложения на сайте и <br> в социальных сетях вашего клуба. <br>
                                Теперь клиент клуба сможет:</p>
                            <ul>
                                <li>оплатить выбранное занятие</li>
                                <li>посмотреть историю своих посещений</li>
                                <li>записаться на групповую/персональную тренировку</li>
                                <li>следить за окончанием срока действия абонемента</li>
                                <li>заморозить/разморозить контракт <br> в соответствии с политикой заморозки</li>
                            </ul>
                            <p class="g-d_f_xs g-ai_c_xs section-text-control"><a href="#popup-details"
                                                                                  class="btn btn_plr g-mr_2_xs"
                                                                                  data-colorbox>Узнать
                                    подробнее</a><a href="#popup-consultation"
                                                    class="g-link-js g-tt_u g-fs_sm g-clr_4 g-hidden g-show_ib_md"
                                                    data-colorbox>Получить
                                    консультацию</a></p>
                        </div>
                    </div>
                </div>
                <div class="g-col g-col_md_6">
                    <div class="box-img-circle box-img-circle_1">
                        <div class="box-img-circle__img-wrap" data-px='{"d":1,"s":1000}'>
                            <div class="box-img-circle__circle"></div>
                            <div class="box-img-circle__icon box-img-circle__icon_1_1" data-px='{"d":-1,"s":50}'></div>
                            <div class="box-img-circle__icon box-img-circle__icon_1_2" data-px='{"d":-1,"s":50}'></div>
                            <div class="box-img-circle__icon box-img-circle__icon_1_3" data-px='{"d":-1,"s":50}'></div>
                            <img class="box-img-circle__img" src="<?= $p . "img/page_other/orther_item_1.png" ?>"
                                 data-px='{"d":-1,"s":500}' alt="Комплексное решение для управления вашим клубом">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-text section-text_other g-section-margin_big">
        <div class="g-grid">
            <div class="g-row g-row_flex g-ai_c_md">
                <div class="g-col g-col_md_6">
                    <div>
                        <h2>Магазин для сайта и прием <br> платежей в мобильном приложении</h2>
                        <div>
                            <p>Эффективный инструмент повышения спонтанных продаж продуктов и услуг на сайте и в
                                мобильном приложении вашего клуба.
                                Предоставьте вашему клиенту возможность оплаты тренировок или спортивного питания в два
                                клика.</p>

                            <p class="g-d_f_xs g-ai_c_xs section-text-control"><a href="#popup-details"
                                                                               class="btn btn_plr g-mr_2_xs"
                                                                               data-colorbox>Узнать
                                    подробнее</a><a href="#popup-consultation"
                                                    class="g-link-js g-tt_u g-fs_sm g-clr_4 g-hidden g-show_ib_md"
                                                    data-colorbox>Получить
                                    консультацию</a></p>
                        </div>
                    </div>
                </div>
                <div class="g-col g-col_md_6">
                    <div class="box-img-circle box-img-circle_1">
                        <div class="box-img-circle__img-wrap" data-px='{"d":-1,"s":1000}'>
                            <div class="box-img-circle__circle"></div>
                            <div class="box-img-circle__icon box-img-circle__icon_2_1" data-px='{"d":1,"s":50}'></div>
                            <div class="box-img-circle__icon box-img-circle__icon_2_2" data-px='{"d":1,"s":50}'></div>
                            <div class="box-img-circle__icon box-img-circle__icon_2_3" data-px='{"d":1,"s":50}'></div>
                            <img class="box-img-circle__img" src="<?= $p . "img/page_other/orther_item_2.png" ?>"
                                 data-px='{"d":1,"s":500}' alt="Комплексное решение для управления вашим клубом">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-text section-text_other g-section-margin_big">
        <div class="g-grid">
            <div class="g-row g-row_flex g-ai_c_md">
                <div class="g-col g-col_md_6">
                    <div>
                        <h2>Виджет расписания для TV-панелей</h2>
                        <div>
                            <p>Брендированное расписание клуба для TV-панелей показывает актуальное расписание ближайших
                                по времени групповых занятий.</p>

                            <p class="g-d_f_xs g-ai_c_xs section-text-control"><a href="#popup-details"
                                                                               class="btn btn_plr g-mr_2_xs"
                                                                               data-colorbox>Узнать
                                    подробнее</a><a href="#popup-consultation"
                                                    class="g-link-js g-tt_u g-fs_sm g-clr_4 g-hidden g-show_ib_md"
                                                    data-colorbox>Получить
                                    консультацию</a></p>
                        </div>
                    </div>
                </div>
                <div class="g-col g-col_md_6">
                    <div class="box-img-circle box-img-circle_1">
                        <div class="box-img-circle__img-wrap" data-px='{"d":1,"s":1000}'>
                            <div class="box-img-circle__circle"></div>
                            <div class="box-img-circle__icon box-img-circle__icon_1_1" data-px='{"d":-1,"s":50}'></div>
                            <div class="box-img-circle__icon box-img-circle__icon_1_2" data-px='{"d":-1,"s":50}'></div>
                            <div class="box-img-circle__icon box-img-circle__icon_1_3" data-px='{"d":-1,"s":50}'></div>
                            <img class="box-img-circle__img" src="<?= $p . "img/page_other/orther_item_3.png" ?>"
                                 data-px='{"d":-1,"s":500}' alt="Комплексное решение для управления вашим клубом">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-text section-text_other g-section-margin_big">
        <div class="g-grid">
            <div class="g-row g-row_flex g-ai_c_md">
                <div class="g-col g-col_md_6">
                    <div>
                        <h2>Виджет для amoCRM</h2>
                        <div>
                            <p>Возможность записи клиентов на групповые и персональные занятия прямо из amoCRM.</p>

                            <p class="g-d_f_xs g-ai_c_xs section-text-control"><a href="#popup-details"
                                                                                  class="btn btn_plr g-mr_2_xs"
                                                                                  data-colorbox>Узнать
                                    подробнее</a><a href="#popup-consultation"
                                                    class="g-link-js g-tt_u g-fs_sm g-clr_4 g-hidden g-show_ib_md"
                                                    data-colorbox>Получить
                                    консультацию</a></p>
                        </div>
                    </div>
                </div>
                <div class="g-col g-col_md_6">
                    <div class="box-img-circle box-img-circle_1">
                        <div class="box-img-circle__img-wrap" data-px='{"d":-1,"s":1000}'>
                            <div class="box-img-circle__circle"></div>
                            <div class="box-img-circle__icon box-img-circle__icon_3_1" data-px='{"d":1,"s":50}'></div>
                            <div class="box-img-circle__icon box-img-circle__icon_3_2" data-px='{"d":1,"s":50}'></div>
                            <div class="box-img-circle__icon box-img-circle__icon_3_3" data-px='{"d":1,"s":50}'></div>
                            <img class="box-img-circle__img" src="<?= $p . "img/page_other/orther_item_4.png" ?>"
                                 data-px='{"d":1,"s":500}' alt="Комплексное решение для управления вашим клубом">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<? $sliderClientsText = '
    <h2>Фитнес клубы, которые<br>присоединились<br>к Mobifitness</h2>
    <p>У нас уже более <span data-target="clients">555</span> клиентов по России и СНГ. <br> Среди них, как небольшие одиночные клубы, так и крупные федеральные сети фитнес-клубов.
</p>    
    ';
include_once("_inc_slider_clients.php"); ?>

<?
include_once("_inc_subs.php"); ?>




<? include_once("_inc_footer.php"); ?>