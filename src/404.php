<?
$metaTitle = '404';
$metaDescription = '404';
include_once("_inc_header.php"); ?>
    <style>
        /*    .page__main{display:flex}
            .page__article {
                margin:auto;
                padding:2em;
            }*/
    </style>
    <article class="page__404 p404">
        <div class="g-grid">
            <section class="box-main  g-section-margin">
                <div class="g-grid">
                    <div class="box-main__box">
                        <h1 class="p404__title">Страница не найдена</h1>
                        <p class="p404__text">Вы перешли на страницу, которой не существует.</p>
                        <p class="g-d_f g-ai_c_xs g-jc_c"><a href="/" class="btn btn_plr g-mr_2_xs">Перейти на
                                главную</a><a href="#popup-consultation" class="g-link-js g-tt_u g-fs_sm"
                                              data-colorbox=>Получить
                                консультацию</a></p>

                    </div>
                </div>
            </section>


        </div>

    </article>


<? include_once("_inc_footer.php"); ?>