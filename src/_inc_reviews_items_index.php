<section class="section-reviews g-section-margin">
    <div class="g-grid">
        <h2 class="g-ta_c_xs">Отзывы</h2>
        <p class="g-ta_c_xs">Клиенты говорят о Mobifitness</p>
        <div class="g-col_md_10 g-mlr_auto_md">
            <div class="slider-reviews"
                 data-slick='{"respondTo": "min", "slidesToShow": 2,"slidesToScroll": 2,"dots": true, "fade": false,"arrows":true,"responsive": [{"breakpoint": 639,"settings":{"slidesToShow": 1, "slidesToScroll": 1,"arrows": false}}]}'>
                <!--item-->
                <div class="slider-reviews__item">
                    <a href="#" class="reviews-item g-link-js-parent">
                        <div class="reviews-item__avatar"
                             style="background-image: url(<?= $p . "img/reviews/mz.jpg" ?>);"></div>
                        <div class="reviews-item__text">
                            <p class="reviews-item__name">Марина Захарова, главный редактор "CLUB BUSINESS".</p>
                            <p>То, что делает Mobifitness - удобно и красиво. Для клуба такое решение закрывает кучу
                                важных задач - от продлений до сарафанного радио.</p>
                        </div>
                        <span class="g-link-js g-clr_4 g-fs_sm g-hidden">Подробнее</span>
                    </a>
                </div>
                <!--item-->
                <div class="slider-reviews__item">
                    <a href="#" class="reviews-item g-link-js-parent">
                        <div class="reviews-item__avatar"
                             style="background-image: url(<?= $p . "img/reviews/kp.jpg" ?>);"></div>
                        <div class="reviews-item__text">
                            <p class="reviews-item__name">Кирилл Потамошев, генеральный директор "ProФитнес".</p>
                            <p>Благодаря ПО для фитнес-клуба получилось полностью автоматизировать клуб и отказаться от
                                рецепции.</p>
                        </div>
                        <span class="g-link-js g-clr_4 g-fs_sm g-hidden">Подробнее</span>
                    </a>
                </div>
                <div class="slider-reviews__item">
                    <a href="#" class="reviews-item g-link-js-parent">
                        <div class="reviews-item__avatar"
                             style="background-image: url(<?= $p . "img/reviews/esh.jpg" ?>);"></div>
                        <div class="reviews-item__text">
                            <p class="reviews-item__name">Евгений Шумихин, digital маркетолог сети фитнес-клубов "Hard
                                Candy Fitness" и "Планета фитнес".</p>
                            <p>Приложение было разработано совместно с сотрудниками фитнес-клубов и в нем нет ничего
                                лишнего. В приложении только то лучшее, что ждет каждый член фитнес-клуба.</p>
                        </div>
                        <span class="g-link-js g-clr_4 g-fs_sm g-hidden">Подробнее</span>
                    </a>
                </div>
                <div class="slider-reviews__item">
                    <a href="#" class="reviews-item g-link-js-parent">
                        <div class="reviews-item__avatar"
                             style="background-image: url(<?= $p . "img/reviews/revies_item_5.jpg" ?>);"></div>
                        <div class="reviews-item__text">
                            <p class="reviews-item__name">Ольга Шведова, директор по рекламе сети фитнес-центров "Point
                                Fitness"</p>
                            <p>Мобильное приложение позволяет клиентам фитнес-клубов экономить массу времени. Теперь
                                можно быстро и удобно планировать посещения занятий, оформить заморозку или продление, и
                                забыть о том, что фитнес-карту нужно носить с собой.</p>
                        </div>
                        <span class="g-link-js g-clr_4 g-fs_sm g-hidden">Подробнее</span>
                    </a>
                </div>

            </div>
        </div>
        <div class="section-reviews__btn g-ta_c_xs">
            <a href="#popup-request-decor" class="btn btn-icon btn-icon_mod" data-colorbox>
                Стать клиентом Mobifitness
            </a>
        </div>
    </div>
</section>