<? /*$popupAfterVariant=1*/ ?>
<section class="subs subs_<?= $subsVariant ?> g-section-margin">
    <div class="g-grid">
        <form class="subs__box g-col_md_8 g-mlr_auto_md js-validated no-metrics"
              data-variant="<?= ($subsVariant == "mobil" ? '1' : '0') ?>">
            <div class="subs__circle" data-px='{"d":-1,"s":800}'></div>
            <div class="subs__email" data-px='{"d":1,"s":800}'></div>
            <fieldset>
                <? if ($subsVariant == "mobil") { ?>
                    <legend class="h2">Узнать подробнее <br> о мобильном приложении</legend>
                    <p>Укажите свою электронную почту, и мы детально опишем <br> все преимущества мобильных приложений
                        Mobifitness</p>
                <? } else { ?>
                    <legend class="h2">Будьте в курсе всех событий</legend>
                    <p>Подпишитесь на бесплатную новостную рассылку</p>
                <? } ?>

            </fieldset>
            <div class="subs__group">
                <input type="email" class="subs__input js-mask subs-input" name="email"
                       placeholder="Напишите свой e-mail">
                <input type="hidden" name="group" value="subscribe">
                <input type="submit" value="" class="subs__btn btn-arrow">
            </div>
        </form>
    </div>
</section>