<? $navMain = array(
    array("URL" => "crm", "NAME" => "Учетная система"),
    array("URL" => "app", "NAME" => "Приложение "),
    array("URL" => "timetable", "NAME" => "Онлайн-запись"),
    array("URL" => "other", "NAME" => "Другие продукты"),
    array("URL" => "contacts", "NAME" => " Контакты"),
) ?>


<nav class="nav-main">
    <ul class="nav-main__list">
        <? foreach ($navMain as $arItem) { ?>
            <li class="nav-main__item <?= ($_SERVER['REQUEST_URI'] == '/' . $arItem["URL"]) ? 'nav-main__item_active' : '' ?>">
                <a href="<?= $arItem["URL"] ?><?= ($_SERVER['SERVER_NAME'] == 'assets') ? '.php' : '' ?>"
                   class="nav-main__link"><?= $arItem["NAME"] ?></a>
            </li>
        <? } ?>
    </ul>
</nav>