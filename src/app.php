<?
$metaTitle = 'Mobifitness - мобильное приложение для фитнес клуба (Android и iOS)';
$metaDescription = 'Mobifitness - мобильное приложение для работы фитнес клубов. Пожалуй, лучшее мобильное приложение, разработанное для вашего тренажерного зала или фитнес-клуба';
include_once("_inc_header.php"); ?>


    <section class="box-main box-main_mobileapp g-section-margin">
        <div class="g-grid">
            <div class="box-main__box">
                <div class="g-row">
                    <div class="g-col g-col_md_8">
                        <div class="box-main__text">

                            <h1 class="h1_lg">Мобильное приложение для фитнес-клуба</h1>

                            <p class="box-main__descr">Индивидуальный дизайн и разработка - бесплатны для каждого
                                клиента</p>

                            <p class="g-d_f g-ai_c_xs box-main__control"><a href="#popup-details"
                                                                            class="btn btn_plr g-mr_2_xs"
                                                                            data-colorbox>Заказать приложение</a><a
                                        href="#popup-consultation" class="g-link-js g-fs_sm g-tt_u_xs"
                                        data-colorbox>Получить
                                    консультацию</a></p>

                            <div class="box-main__img" data-px='{"d":1,"s":1500}'></div>
                            <div class="box-main__circle-decor" data-px='{"d":-1,"s":250}'>
                                всего от
                                <div class="box-main__circle-decor-title">1 990</div>
                                рублей в месяц
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="section-text section-text_includes section-text_img-abs_left g-section-margin_big">
        <div class="g-grid g-grid_lg">
            <div class="g-pr">

                <div class="g-col_md_5 section-text__text g-list-wrap_mb_1">
                    <h2>Что будет в мобильном приложении
                        вашего клуба?</h2>
                    <div class=" section-text__img-abs g-col_md_5">
                        <div class="slider-screen-img slider-screen-img_includes">
                            <div class="slider-screen"
                                 data-slick='{"slidesToShow": 1, "slidesToScroll": 1,"dots": false, "fade": false,"arrows":false,"responsive": [{"breakpoint": 767,"settings":{"slidesToShow": 1, "slidesToScroll": 1,"arrows": false}}]}'>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/1.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/2.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/3.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/4.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/5.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/6.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/7.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/8.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/9.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/10.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/11.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/12.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/13.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/14.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                                <div class="slider-screen__item">
                                    <div style="background-image:url(<?= $p . "img/page_mobilapp/slider_includes/15.jpg" ?>)"
                                         class="slider-screen__img"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul data-dots-listener='{"parent":".section-text","target":".slider-screen"}'>
                        <li>расписание занятий</li>
                        <li>push-уведомления</li>
                        <li>новости и акции клуба</li>
                        <li>личный кабинет клиента</li>
                        <li>индивидуальный дизайн</li>
                        <li>полная информация о клубе</li>
                        <li>заморозка и продление карты</li>
                        <li>запись на групповые занятия</li>
                        <li>запись на персональные тренировки</li>
                        <li>оплата из мобильного приложения</li>
                        <li>виртуальная клубная карта клиента</li>
                        <li>все фитнес-клубы сети в одном приложении</li>
                        <li>информация о тренерах и описание тренировок</li>
                        <li>программа лояльности</li>
                        <li>форма обратной связи</li>
                    </ul>
                </div>

            </div>
        </div>
    </section>


    <section class="g-section-margin">
        <div class="g-grid">
            <div class="box-store ">
                <div class="g-row">
                    <div class="g-col g-col_md_5">
                        <div class="box-store__title h3">Посмотреть все разработанные приложения</div>
                    </div>
                    <div class="g-col g-col_md_7">
                        <div class="g-row">
                            <div class="g-col g-col_xs_6"><a target="_blank"
                                                             href="https://itunes.apple.com/ru/developer/llc-effective-web-solutions/id680588310"
                                                             class="box-store__btn box-store__btn_app"><span
                                            class="g-sr-only">Загрузите в AppStore</span></a></div>
                            <div class="g-col g-col_xs_6"><a target="_blank"
                                                             href="https://play.google.com/store/apps/developer?id=Mobifitness"
                                                             class="box-store__btn box-store__btn_google"><span
                                            class="g-sr-only">Доступно на GogglePlay</span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="func">
        <div class="g-grid">
            <h2 class="g-ta_c_xs">Основные функции, проверенные временем <br>
                и <span data-target="clubs">1700</span> фитнес-клубами</h2>
            <ul class="func__list">
                <li class="func__row g-row g-row_flex">
                    <div class="func__image g-col g-col_md_4">
                        <div>
                            <div class="h3 func__title func__title_psevdo"
                                 data-text="1. Безупречное расписание"></div>
                            <img class="func__img" src="<?= $p . "img/func/item1.png" ?>"
                                 alt="1. Безупречное расписание">
                        </div>
                    </div>
                    <div class="func__text g-col g-col_md_4">
                        <div>
                            <h3 class="func__title">1. Безупречное расписание</h3>
                            <ul>
                                <li>Цвета и дизайн подстраиваются под ваш фирменный стиль</li>
                                <li>Запись на персональные и групповые тренировки</li>
                                <li>Оплата занятий из расписания</li>
                                <li>Возможность ограничить емкость группы</li>
                                <li>Фильтры по времени, занятиям, направлениям и тренерам</li>
                                <li>Описание занятий и направлений</li>
                                <li>Информация о тренерах</li>
                                <li>Личный график занятий клиента</li>
                            </ul>
                        </div>
                    </div>

                    <div class="func__reviews g-col g-col_md_4">
                        <div class="func__reviews-box">
                            <div class="reviews-item reviews-item_box">
                                <div class="reviews-item__top">
                                    <div class="reviews-item__avatar"
                                         style="background-image:url(<?= $p . "img/reviews/revies_item_3.png" ?>)"></div>
                                    <small class="reviews-item__name">
                                        Петр Горецкий, руководитель студии "EXPROMT"
                                    </small>
                                </div>
                                <p>Расписание в мобильном приложении – одно из самых удобных нововведений нашего
                                    клуба. Теперь у наших клиентов есть доступ к достоверной и свежей информации о
                                    работе клуба и тренеров 24/7.</p>
                            </div>
                        </div>

                    </div>
                </li>
                <li class="func__row g-row g-row_flex">
                    <div class="func__image g-col g-col_md_4">
                        <div>
                            <div class="h3 func__title func__title_psevdo"
                                 data-text="2. Push-уведомления клиентам"></div>
                            <img class="func__img" src="<?= $p . "img/func/item2.png" ?>"
                                 alt="2. Push-уведомления клиентам">
                        </div>
                    </div>
                    <div class="func__text g-col g-col_md_4">
                        <div>
                            <h3 class="func__title">2. Push-уведомления клиентам</h3>
                            <ul>
                                <li>Удобные и бесплатные push-уведомления о новостях, акциях и сервисных
                                    оповещениях клуба
                                </li>
                                <li>Уведомления о предстоящих занятиях и изменениях в расписании</li>
                                <li>Рассылка уведомлений на всю активную базу или персонально клиенту</li>
                            </ul>
                            <p>Экономия на смс-рассылках – они больше
                                не нужны (стоимость 1 смс в среднем - 2 руб, <br>1 push-уведомление стоит 0 руб).</p>
                        </div>
                    </div>
                    <div class="func__reviews g-col g-col_md_4">
                        <div class="func__reviews-box">
                            <div class="reviews-item reviews-item_box">
                                <div class="reviews-item__top">
                                    <div class="reviews-item__avatar"
                                         style="background-image:url(<?= $p . "img/reviews/revies_item_4.jpg" ?>)"></div>
                                    <small class="reviews-item__name">
                                        Александр Чечнев,
                                        маркетолог фитнес
                                        клуба "PRIDE CLUB"
                                    </small>
                                </div>
                                <p>Push-уведомления оказались удобными, как для маркетологов, так и для других
                                    отделов фитнес-клуба. Теперь наши клиенты моментально получают информацию об
                                    отмене занятий, что позволило значительно снизить негатив.</p>
                            </div>
                        </div>

                    </div>
                </li>
                <li class="func__row g-row g-row_flex">
                    <div class="func__image g-col g-col_md_4">
                        <div>
                            <div class="h3 func__title func__title_psevdo"
                                 data-text="3. Личный кабинет клиента"></div>
                            <img class="func__img" src="<?= $p . "img/func/item3.png" ?>"
                                 alt="3. Личный кабинет клиента">
                        </div>
                    </div>
                    <div class="func__text g-col g-col_md_4">
                        <div>
                            <h3 class="func__title">3. Личный кабинет клиента</h3>
                            <ul>
                                <li>Заморозка и продление карты сразу из мобильного приложения</li>
                                <li>Оплата абонементов, тренировок и дополнительных услуг</li>
                                <li>Отображение в личном кабинете клиента полной информации по его абонементу,
                                    историй посещений и транзакций
                                </li>
                                <li>Виртуальная клубная карта</li>
                                <li>Оценка прошедших тренировок</li>
                            </ul>
                        </div>
                    </div>

                    <div class="func__reviews g-col g-col_md_4">
                        <div class="func__reviews-box">
                            <div class="reviews-item reviews-item_box">
                                <div class="reviews-item__top">
                                    <div class="reviews-item__avatar"
                                         style="background-image:url(<?= $p . "img/reviews/revies_item_5.jpg" ?>)"></div>
                                    <small class="reviews-item__name">
                                        Ольга Шведова,
                                        директор по рекламе
                                        сети фитнес-центров
                                        "POITN FITNESS"
                                    </small>
                                </div>
                                <p>Теперь можно быстро и удобно планировать посещения занятий, оформить заморозку
                                    или продление, и забыть о том, что фитнес-карту нужно носить с собой.</p>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="func__row g-row g-row_flex">
                    <div class="func__image g-col g-col_md_4">
                        <div>
                            <div class="h3 func__title func__title_psevdo"
                                 data-text="4. Программа лояльности для вашего клуба"></div>
                            <img class="func__img" src="<?= $p . "img/func/item4.png" ?>"
                                 alt="4. Программа лояльности для вашего клуба">
                        </div>
                    </div>
                    <div class="func__text g-col g-col_md_4">
                        <div>
                            <h3 class="func__title">4. Программа лояльности для вашего клуба</h3>
                            <ul>
                                <li>Начисление баллов клиентам клуба за различные действия, которые устанавливаете
                                    вы.
                                </li>
                                <li>Возможность группового начисления баллов</li>
                                <li>Каталог вознаграждений, заявка на получение приза прямо из приложения</li>
                                <li>Реферальная программа «приведи друга»</li>
                            </ul>
                        </div>
                    </div>

                    <div class="func__reviews g-col g-col_md_4">
                        <div class="func__reviews-box">
                            <div class="reviews-item reviews-item_box">
                                <div class="reviews-item__top">
                                    <div class="reviews-item__avatar"
                                         style="background-image:url(<?= $p . "img/reviews/revies_item_6.png" ?>)"></div>
                                    <small class="reviews-item__name">Екатерина Деева,
                                        маркетолог
                                        фитнес-клуба
                                        "ПРАЙМ ФИТНЕС"
                                    </small>
                                </div>
                                <p>Программа лояльности стимулирует клиентов совершать покупки и ходить чаще в клуб,
                                    каждый третий клиент накапливает баллы и тратит их в магазине и баре клуба!</p>
                            </div>
                        </div>

                    </div>
                </li>
                <li class="func__row g-row g-row_flex">
                    <div class="func__image g-col g-col_md_4">
                        <div>
                            <div class="h3 func__title func__title_psevdo"
                                 data-text="5. Связь с клиентом 24/7"></div>
                            <img class="func__img" src="<?= $p . "img/func/item5.png" ?>"
                                 alt="5. Связь с клиентом 24/7">
                        </div>
                    </div>
                    <div class="func__text g-col g-col_md_4">
                        <div>
                            <h3 class="func__title">5. Связь с клиентом 24/7</h3>
                            <ul>
                                <li>Размещение полной информации о вашем клубе, а также анонсы актуальных новостей
                                    и акций
                                </li>

                                <li>Получение обратной связи от клиентов напрямую на почту и в админ панель</li>

                                <li>Моментальные ответы клиентам в виде push-уведомлений или по почте</li>
                            </ul>
                        </div>
                    </div>

                    <div class="func__reviews g-col g-col_md_4">
                        <div class="func__reviews-box">
                            <div class="reviews-item reviews-item_box">
                                <div class="reviews-item__top">
                                    <div class="reviews-item__avatar"
                                         style="background-image:url(<?= $p . "img/reviews/revies_item_7.png" ?>)"></div>
                                    <small class="reviews-item__name">Александр Еремин,
                                        Управляющий школ
                                        танцев "9 залов"
                                    </small>
                                </div>
                                <p>Большое количество положительных отзывов от клиентов, людям нравится простой
                                    доступ к расписанию и актуальной информации о работе клуба.</p>
                            </div>
                        </div>

                    </div>
                </li>
            </ul>

        </div>
    </section>


<? $formLineVariant = 3;
include("_inc_form_line.php"); ?>


    <section class="section-text section-text_voz g-section-margin_big">
        <div class="g-grid">
            <div class="g-pr">
                <div class="g-col_md_5">
                    <h2>Расширьте возможности <br>
                        приложения для клиентов
                    </h2>
                    <div class=" section-text__img-abs g-col_md_6">
                        <img src="<?= $p . "img/page_mobilapp/club.png" ?>"
                             alt="Расширьте возможности приложения для клиентов">
                    </div>
                    <p>Мобильное приложение интегрируется с любой клубной системой*</p>
                    <p>
                        <small class="g-clr_4">*при наличии API у клубной системы</small>
                    </p>
                    <p class="g-d_f g-ai_c_xs section-text-control"><a href="#popup-details"
                                                                       class="btn btn_plr g-mr_2_xs" data-colorbox>Узнать
                            подробнее</a><a href="#popup-consultation"
                                            class="g-link-js g-tt_u g-fs_sm g-clr_4 g-hidden g-show_ib_md"
                                            data-colorbox>Получить
                            консультацию</a></p>
                </div>
            </div>

        </div>
    </section>

    <section class="statistic statistic_phone g-section-margin">
        <div class="g-grid">
            <div class="statistic__box">
                <div class="g-col_md_10 g-mlr_auto_md">
                    <div class="g-row  g-pr">
                        <div class="g-col_md_7 statistic__img-text">
                            <h2>Мы - лидеры на рынке разработки<br>мобильных приложений для клубов</h2>
                            <div class="statistic__img-wrap g-col-offset_md_8">
                                <img data-px='{"d":-1,"s":1000}' src="<?= $p . "img/page_mobilapp/r-state.png" ?>"
                                     alt="Мы - лидеры на рынке разработки мобильных приложений для клубов.">
                            </div>
                            <p>Уже более <span data-target="clubs">1600</span> клубов по всей России и СНГ идут в ногу
                                со временем и получили
                                собственное мобильное приложение для фитнес-клуба от Mobifitness. Среди них, как
                                небольшие одиночные
                                клубы площадью от 200 квадратных метров, так и крупные федеральные сети.</p>
                            <p>Отправьте заявку и получите мобильное приложение для вашего
                                фитнес-клуба!</p>
                            <br>
                            <p><a href="#popup-request-decor" class="btn btn-icon btn-icon_no_bsh btn-icon_mod"
                                  data-colorbox>
                                    Стать клиентом Mobifitness
                                </a>
                            </p>

                        </div>

                    </div>
                    <div class="g-row  g-ta_c_xs">
                        <div class="g-col g-col_md_4">
                            <div class="statistic__item">
                                <div class="statistic__text">
                                    <div class="statistic__title">305 000</div>
                                    <p>активных пользователей <br>
                                        мобильными приложениями</p>
                                </div>
                            </div>
                        </div>
                        <div class="g-col g-col_md_4">
                            <div class="statistic__item">
                                <div class="statistic__text">
                                    <div class="statistic__title">1 964 000</div>
                                    <p>заявок из мобильных приложений <br>
                                        на заморозку и продление карты</p>
                                </div>
                            </div>
                        </div>
                        <div class="g-col g-col_md_4">
                            <div class="statistic__item">
                                <div class="statistic__text">
                                    <div class="statistic__title">3 103 000</div>
                                    <p>охват постов в соц.сетях, сделанных <br>
                                        членами фитнес–клубов из мобильных <br>
                                        приложений</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="rates  g-section-margin">
        <div class="g-grid">
            <h2 class="g-ta_c_xs">Тарифы</h2>
            <p class="g-ta_c_xs">Тарифы можно комбинировать, оплачивая только нужные вам функции. <br>
                Прием платежей в приложении стоит от 5880 рублей в год + комиссия эквайера. <br>
                Цены указаны за 1 клуб, каждый дополнительный клуб увеличивает стоимость на 16%.</p>
            <form action="?" class="rates__form">
                <div class="g-row g-row_flex g-ai_c_md">
                    <div class="g-col g-col_md_3">
                        <div class="rates__text-decor rates__text-decor_dev">
                            <div class="rates__text-decor-dev">Разработка и дизайн</div>
                            <div class="rates__text-decor-title">БЕСПЛАТНО!</div>

                            <div class="rates__text-decor-text">
                                для любого тарифа
                            </div>
                        </div>
                    </div>
                    <div class="g-col g-col_md_9">
                        <ul class="rates__list">
                            <li class="rates__item rates__item_1">
                                <div class="rates__box">
                                    <div class="rates__label">Тариф</div>
                                    <h3 class="rates__name">Старт</h3>
                                    <div class="rates__soc">Приложение<br> с минимум функций</div>
                                    <div class="rates__price">
                                        <fieldset class="rates__select js-rates__select">
                                            <strong class="rates__select-name g-link-js">
                                                <span class="h1">1 990</span> руб./мес. <span
                                                        class="rates__select-arrow"><span
                                                            class="g-icon-down"></span>   </span></strong>

                                            <ul class="rates__select-list">
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <input class="chr__input" type="radio" name="rates__item_1"
                                                               checked
                                                               value="1 год">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="26">1 990</span> руб. / месяц
                                                        <span class="rates__label">При оплате за год</span>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <input class="chr__input" ы type="radio" name="rates__item_1"
                                                               value="6 месяцев">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="15">2 290</span> руб. / месяц
                                                        <span class="rates__label">При оплате за полгода</span>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <input class="chr__input" type="radio" name="rates__item_1"
                                                               value="3 месяца">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="0">2 690</span> руб. / месяц
                                                        <span class="rates__label">При оплате за 3 месяца</span>
                                                    </span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </fieldset>
                                    </div>
                                    <div class="rates__saving">
                                        <ul>
                                            <li>при оплате за год <br> экономия <span
                                                        class="rates__saving-numb">26</span>%
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="rates__adaptive">
                                    <p class="rates__adaptive-text">*Для телефонных номеров на территории России. Для
                                        других стран уточняйте
                                        стоимость у менеджера</p>
                                    <div class="btn-arrow rates__btn-arrow js-rates__btn-arrow"></div>

                                </div>

                            </li>
                            <li class="rates__item rates__item_2">
                                <div class="rates__box">
                                    <div class="rates__label">Тариф</div>
                                    <h3 class="rates__name">Базовый</h3>
                                    <div class="rates__soc">Приложение не содержит функционала лояльности</div>
                                    <div class="rates__price">
                                        <fieldset class="rates__select js-rates__select">
                                            <strong class="rates__select-name g-link-js"
                                            >
                                                <span class="h1">3 490</span> руб./мес. <span
                                                        class="rates__select-arrow"><span
                                                            class="g-icon-down"></span>   </span></strong>

                                            <ul class="rates__select-list">
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <input class="chr__input" type="radio" name="rates__item_2"
                                                               checked
                                                               value="1 год">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="21">3 490</span> руб. / месяц
                                                        <span class="rates__label">При оплате за год</span>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <input class="chr__input" type="radio" name="rates__item_2"
                                                               value="6 месяцев">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="14">3 790</span> руб. / месяц
                                                        <span class="rates__label">При оплате за полгода</span>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <input class="chr__input" type="radio" name="rates__item_2"
                                                               value="3 месяца">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="0">4 390</span> руб. / месяц
                                                        <span class="rates__label">При оплате за 3 месяца</span>
                                                    </span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </fieldset>
                                    </div>
                                    <div class="rates__saving">
                                        <ul>
                                            <li>при оплате за год <br> экономия <span
                                                        class="rates__saving-numb">21</span>%
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="rates__adaptive">
                                    <p class="rates__adaptive-text">*Для телефонных номеров на территории России. Для
                                        других стран уточняйте
                                        стоимость у менеджера</p>
                                    <div class="btn-arrow rates__btn-arrow js-rates__btn-arrow"></div>

                                </div>
                            </li>
                            <li class="rates__item rates__item_3">
                                <div class="rates__box">
                                    <div class="rates__label">Тариф</div>
                                    <h3 class="rates__name">Лояльность</h3>
                                    <div class="rates__soc">Полный пакет и спектр приложения</div>
                                    <div class="rates__price">
                                        <fieldset class="rates__select js-rates__select">
                                            <strong class="rates__select-name g-link-js"
                                            >
                                                <span class="h1">5 490</span> руб./мес. <span
                                                        class="rates__select-arrow"><span
                                                            class="g-icon-down"></span>   </span></strong>

                                            <ul class="rates__select-list">
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <input class="chr__input" type="radio" name="rates__item_3"
                                                               checked
                                                               value="1 год">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="22">5 490</span> руб. / месяц
                                                        <span class="rates__label">При оплате за год</span>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <input class="chr__input" type="radio" name="rates__item_3"
                                                               value="6 месяцев">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="13">6 090</span> руб. / месяц
                                                        <span class="rates__label">При оплате за полгода</span>
                                                    </span>
                                                    </label>
                                                </li>
                                                <li class="rates__select-item">
                                                    <label class="chr">
                                                        <input class="chr__input" type="radio" name="rates__item_3"
                                                               value="3 месяца">
                                                        <span class="chr__text">
                                                        <span class="h1" data-percent="0">6 990</span> руб. / месяц
                                                        <span class="rates__label">При оплате за 3 месяца</span>
                                                    </span>
                                                    </label>
                                                </li>
                                            </ul>
                                        </fieldset>
                                    </div>
                                    <div class="rates__saving">
                                        <ul>
                                            <li>при оплате за год <br> экономия <span
                                                        class="rates__saving-numb">22</span>%
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="rates__adaptive">
                                    <p class="rates__adaptive-text">*Для телефонных номеров на территории России. Для
                                        других стран уточняйте
                                        стоимость у менеджера</p>
                                    <div class="btn-arrow rates__btn-arrow js-rates__btn-arrow"></div>

                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </form>
            <div class="g-d_b" hidden>
                <div class="rates-table">
                    <ul class="rates-table__table">
                        <li class="rates-table__item">
                            <h4 class="rates-table__name rates-table__row">Мобильное приложение Android и iOS </h4>
                            <div class="rates-table__rows">
                                <div class="rates-table__row">
                                    <p class="rates-table__text">
                                        Панель управления
                                    </p>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                </div>
                                <div class="rates-table__row">
                                    <p class="rates-table__text">
                                        Брендируется иконка и цвета приложения
                                    </p>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                </div>
                                <div class="rates-table__row">
                                    <p class="rates-table__text">
                                        Расписание занятий
                                    </p>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                </div>
                                <div class="rates-table__row">
                                    <p class="rates-table__text">Персональный график занятий</p>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                </div>
                                <div class="rates-table__row rates-table__row_parent"
                                     data-toggle='{"animation":"js","parent":".rates-table__row","target":".rates-table__rows_children", "className":"open"}'>
                                    <p class="rates-table__text">Клубы</p>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                    <div class="rates-table__rows rates-table__rows_children">
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Список клубов, клубы на карте
                                            </p>
                                            <div class="rates-table__point rates-table__point_1"></div>
                                            <div class="rates-table__point rates-table__point_2"></div>
                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Связь с менеджером клуба
                                            </p>
                                            <div class="rates-table__point rates-table__point_1"></div>
                                            <div class="rates-table__point rates-table__point_2"></div>
                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Информация о клубе (режим работы, фотогалерея)
                                            </p>
                                            <div class="rates-table__point rates-table__point_1"></div>
                                            <div class="rates-table__point rates-table__point_2"></div>
                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="rates-table__row">
                                    <p class="rates-table__text">
                                        Новости (Акции)
                                    </p>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                </div>
                                <div class="rates-table__row">
                                    <p class="rates-table__text">
                                        PUSH-уведомления об акциях <br>
                                        и изменениях в расписании
                                    </p>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_1"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                </div>
                                <div class="rates-table__row">
                                    <p class="rates-table__text">Инструкторы</p>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                </div>
                                <div class="rates-table__row">
                                    <p class="rates-table__text">Обратная связь</p>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                </div>
                                <div class="rates-table__row rates-table__row_parent"
                                     data-toggle='{"animation":"js","parent":".rates-table__row","target":".rates-table__rows_children", "className":"open"}'>
                                    <p class="rates-table__text">Запись на групповые занятия</p>

                                    <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                    <div class="rates-table__rows rates-table__rows_children">
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Запись через мобильные приложения Android и iOS (звонок в клуб,
                                                напоминания)
                                            </p>
                                            <div class="rates-table__point rates-table__point_2"></div>
                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Возможность настраивать доступный для записи интервал времени
                                            </p>
                                            <div class="rates-table__point rates-table__point_2"></div>
                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="rates-table__row rates-table__row_parent"
                                     data-toggle='{"animation":"js","parent":".rates-table__row","target":".rates-table__rows_children", "className":"open"}'>
                                    <p class="rates-table__text">Интеграция с социальными сетями</p>

                                    <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                    <div class="rates-table__rows rates-table__rows_children">
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Facebook, Вконтакте
                                            </p>
                                            <div class="rates-table__point rates-table__point_2"></div>
                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="rates-table__row rates-table__row_parent"
                                     data-toggle='{"animation":"js","parent":".rates-table__row","target":".rates-table__rows_children", "className":"open"}'>
                                    <p class="rates-table__text">Личный кабинет</p>

                                    <div class="rates-table__point rates-table__point_clock rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                    <div class="rates-table__rows rates-table__rows_children">
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Авторизация члена клуба по SMS
                                            </p>
                                            <div class="rates-table__point rates-table__point_text rates-table__point_clock rates-table__point_1">
                                                1.8-2.6 руб/SMS*
                                            </div>
                                            <div class="rates-table__point rates-table__point_text rates-table__point_clock rates-table__point_2">
                                                1.8-2.6 руб/SMS*
                                            </div>
                                            <div class="rates-table__point rates-table__point_text rates-table__point_3">
                                                1.8-2.6 руб/SMS*
                                            </div>
                                        </div>
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Профиль члена клуба
                                            </p>
                                            <div class="rates-table__point rates-table__point_2"></div>
                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Виртуальная клубная карта
                                            </p>
                                            <div class="rates-table__point rates-table__point_2"></div>
                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Заморозка карты
                                            </p>
                                            <div class="rates-table__point rates-table__point_2"></div>
                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Продление карты
                                            </p>
                                            <div class="rates-table__point rates-table__point_2"></div>
                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="rates-table__row rates-table__row_parent"
                                     data-toggle='{"animation":"js","parent":".rates-table__row","target":".rates-table__rows_children", "className":"open"}'>
                                    <p class="rates-table__text">Мои достижения</p>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                    <div class="rates-table__rows rates-table__rows_children">
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Калькулятор ИМТ
                                            </p>

                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Калькулятор целевого пульса
                                            </p>

                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Динамика веса
                                            </p>

                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="rates-table__row rates-table__row_parent"
                                     data-toggle='{"animation":"js","parent":".rates-table__row","target":".rates-table__rows_children", "className":"open"}'>
                                    <p class="rates-table__text">Программа лояльности</p>
                                    <div class="rates-table__point rates-table__point_clock rates-table__point_3"></div>
                                    <div class="rates-table__rows rates-table__rows_children">
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Накопление баллов за публикацию информации в соц сетях и другие
                                                активности
                                            </p>
                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Накопление баллов за покупки в клубе
                                            </p>

                                            <!--<div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Накопление баллов за покупки в клубе
                                            </p>-->

                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Каталог вознаграждений, заявка на получение приза прямо из приложения
                                            </p>

                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                        <div class="rates-table__row">
                                            <p class="rates-table__text">
                                                Система статусов (новичок, ветеран и так далее)
                                            </p>

                                            <div class="rates-table__point rates-table__point_3"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="rates-table__row">
                                    <p class="rates-table__text">Интеграция с клубной учетной системой</p>
                                    <div class="rates-table__point rates-table__point_text rates-table__point_clock rates-table__point_2">
                                        Цена по запросу
                                    </div>
                                    <div class="rates-table__point rates-table__point_text rates-table__point_3">Цена по
                                        запросу
                                    </div>
                                </div>
                            </div>
                        </li>


                        <li class="rates-table__item">
                            <h4 class="rates-table__name rates-table__row">Сервис</h4>
                            <div class="rates-table__rows">
                                <div class="rates-table__row">
                                    <p class="rates-table__text">Помощь в анонсировании приложения</p>

                                    <div class="rates-table__point rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_3"></div>
                                </div>
                                <div class="rates-table__row">
                                    <p class="rates-table__text">Обучение сотрудников</p>

                                    <div class="rates-table__point rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_3"></div>
                                </div>
                                <div class="rates-table__row">
                                    <p class="rates-table__text">Статистика использования приложения</p>

                                    <div class="rates-table__point rates-table__point_2"></div>
                                    <div class="rates-table__point rates-table__point_3"></div>
                                </div>
                            </div>
                        </li>


                    </ul>
                    <div class="g-row g-row_flex rates-table__btns">
                        <p class="g-col g-col_md_3 g-ta_c_xs">
                        </p>
                        <p class="g-col g-col_md_3 g-ta_c_xs">
                            <a href="#popup-request-rate" onclick="func.jsSetTariff('Старт','rates__item_1')"
                               class="rates-table__btn btn btn_plr" data-colorbox="">тариф
                                «старт»</a>
                            <small class="rates-table__small">Неделя в подарок</small>
                        </p>
                        <p class="g-col g-col_md_3 g-ta_c_xs rates-table__col-btn-main">
                            <a href="#popup-request-rate" onclick="func.jsSetTariff('Базовый','rates__item_2')"
                               class="rates-table__btn btn btn-gr btn_plr" data-colorbox="">Тариф
                                «Базовый»</a>
                            <small class="rates-table__small">Две недели в подарок</small>
                        </p>
                        <p class="g-col g-col_md_3 g-ta_c_xs">
                            <a href="#popup-request-rate" onclick="func.jsSetTariff('Все включено','rates__item_3')"
                               class="rates-table__btn btn " data-colorbox="">тариф «лояльность»</a>
                            <small class="rates-table__small">Месяц в подарок</small>
                        </p>
                    </div>

                </div>
            </div>

            <p class="g-ta_c_md g-mt_4_xs g-show_md" hidden>*Для телефонных номеров на территории России. Для других
                стран уточняйте
                стоимость у менеджера</p>
        </div>
    </section>

    <section class="box-present g-section-margin">
        <div class="g-grid">
            <div class="box-present__box">
                <div class="box-present__title h3">
                    Скачать презентацию <br class="g-hide_md"> о мобильном <br>
                    приложении от Mobifitness
                </div>
                <a href="<?= $p . "Mobifitness_Presentation.pdf" ?>" class="box-present__btn btn btn-gr"
                   target="_blank">Скачать
                    презентацию</a>
            </div>
        </div>
    </section>


        <section class="slider-mobil">
            <div class="g-grid_">
                <h2 class="g-ta_c_xs">Идеальное приложение для вашего клуба</h2>
                <p class="g-ta_c_xs">Примеры разработанных приложений для клубов</p>
                <div class="slider-mobil__slider-wrap">
                    <div class="slider-mobil__grid">
                        <div class="slider-mobil__slider"
                             data-slick='{"slidesToShow": 5, "slidesToScroll": 1,"dots": false, "fade": false,"arrows":true,"responsive": [{"breakpoint": 1600,"settings":{"slidesToShow": 3, "arrows": true}},{"breakpoint": 767,"settings":{"slidesToShow": 1, "slidesToScroll": 1,"arrows": false}}]}'>
                            <div class="slider-mobil__item-wrap">
                                <div class="slider-mobil__item slider-mobil__item_1"></div>
                            </div>
                            <div class="slider-mobil__item-wrap">
                                <div class="slider-mobil__item slider-mobil__item_2"></div>
                            </div>
                            <div class="slider-mobil__item-wrap">
                                <div class="slider-mobil__item slider-mobil__item_3"></div>
                            </div>
                            <div class="slider-mobil__item-wrap">
                                <div class="slider-mobil__item slider-mobil__item_4"></div>
                            </div>
                            <div class="slider-mobil__item-wrap">
                                <div class="slider-mobil__item slider-mobil__item_5"></div>
                            </div>
                            <div class="slider-mobil__item-wrap">
                                <div class="slider-mobil__item slider-mobil__item_6"></div>
                            </div>
                            <div class="slider-mobil__item-wrap">
                                <div class="slider-mobil__item slider-mobil__item_7"></div>
                            </div>
                            <div class="slider-mobil__item-wrap">
                                <div class="slider-mobil__item slider-mobil__item_8"></div>
                            </div>
                        </div>
                        <div class="slider-mobil__download">
                            <div class="slider-mobil__download-text">
                                Смотрите и скачивайте бесплатно <br>
                                разработанные нами мобильные приложения
                            </div>
                            <div class="slider-mobil__download-btns">
                                <a href="https://play.google.com/store/apps/developer?id=Mobifitness" target="_blank"
                                   class="slider-mobil__download-btn slider-mobil__download-btn_2"></a>
                                <a href="https://itunes.apple.com/ru/developer/llc-effective-web-solutions/id680588310"
                                   target="_blank"
                                   class="slider-mobil__download-btn slider-mobil__download-btn_1"></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="awards g-section-margin">

            <div class="slider-awards"
                 data-slick='{"slidesToShow": 1, "slidesToScroll": 1,"dots": true, "fade": false,"responsive": [{"breakpoint": 767,"settings":{"slidesToShow": 1, "slidesToScroll": 1,"arrows": false}}]}'>
                <div>
                    <div class="g-grid">
                        <div class="g-row g-row_flex g-ai_c_md">
                            <div class="g-col g-col_md_7">

                                <div class="awards__img awards__img_2"></div>
                            </div>
                            <div class="g-col g-col_md_4">
                                <div>
                                    <h2>Награда Sport Innovations <br> Awards 2018</h2>
                                    <p> По итогам голосования на Sport Innovations Awards 2018
                                        приложение для клиентов фитнес-клуба «Mobifitness»
                                        признано лучшим в категории «Инновационное мобильное приложение для спорта».</p>
                                </div>


                            </div>
                        </div>
                    </div>

                </div>
                <div>
                    <div class="g-grid">
                        <div class="g-row g-row_flex g-ai_c_md">
                            <div class="g-col g-col_md_7">

                                <div class="awards__img awards__img_1"></div>
                            </div>
                            <div class="g-col g-col_md_4">
                                <div>
                                    <h2>Награда MATE APP AWARDS</h2>
                                    <p>По итогам голосования на MATE Expo Apps Awards приложение для клиентов
                                        фитнес-клуба
                                        «Mobifitness» признано лучшим в категории «Лучшее решение в сегменте В2В».</p>
                                </div>


                            </div>
                        </div>
                    </div>

                </div>

            </div>


        </section>

<? include_once("_inc_reviews_items_mobilapp.php"); ?>
    <div class="g-hidden g-show_md">
        <? $sliderClientsText = '
                <h2>С нами работают уже больше  <br><span data-target="clubs">1600</span> фитнес-клубов  <br class="g-hidden g-show_md">по всей России и СНГ</h2>
                <p>Среди клиентов Mobifitness можно найти не только крупные сети фитнес-клубов, но и небольшие студии танцев и йоги площадью не более 200 м<sup>2</sup>.</p>    
            ';
        include_once("_inc_slider_clients.php"); ?>
    </div>


    <section class="request-decor g-list-mb_1 g-show_md g-hidden">
            <div class="g-grid">
                <div class="request-decor__box g-list-wrap_mb_1">
                    <div class="h2">Сомневаетесь, зачем фитнес-клубу <br>
                        мобильное приложение?
                    </div>
                    <ul>
                        <li>Повысить спонтанные продажи</li>
                        <li>Упростить коммуникацию с клиентами клуба</li>
                        <li>Сократить объем рутинной работы сотрудников</li>
                    </ul>
                    <p><a href="#popup-request" class="btn" data-colorbox>Отправить заявку</a></p>

                </div>
            </div>
        </section>

    <section class="faq g-section-margin g-show_md g-hidden">
            <div class="g-grid">
                <h2>Вопрос-ответ о приложении Mobifitness</h2>
                <p>Ознакомьтесь с часто задаваемыми вопросами, если вы не нашли ответ на нужный вопрос, то <a
                            class="g-link-js g-clr_4"
                            href="#popup-info-products" data-colorbox>свяжитесь с нами</a> и мы с радостью ответим.</p>
                <ul class="faq__list">
                    <li class="faq__item"
                        data-toggle='{"animation":"js","parent":".faq__item","target":".faq__text", "className":"open"}'>
                        <h3 class="faq__name">Какие устройства поддерживают приложения Mobifitness?</h3>
                        <div class="faq__text">
                            <p>Мобильное приложение от Mobifitness доступно для загрузки в App Store и Play Market на
                                устройствах версии iOS 9.0 и выше, а на Android версии 4.1 и выше.</p>
                        </div>
                    </li>
                    <li class="faq__item"
                        data-toggle='{"animation":"js","parent":".faq__item","target":".faq__text", "className":"open"}'>
                        <h3 class="faq__name">Сколько времени занимает разработка приложения?</h3>
                        <div class="faq__text">
                            <p>Срок разработки мобильного приложения Mobifitness от 5 дней. Платная подписка начинается
                                только после публикации приложения в App Store и Play Market.</p>
                        </div>
                    </li>
                    <li class="faq__item"
                        data-toggle='{"animation":"js","parent":".faq__item","target":".faq__text", "className":"open"}'>
                        <h3 class="faq__name">Сложно ли клубу управлять мобильным приложением от Mobifitness?</h3>
                        <div class="faq__text">
                            <p>Панель управления Mobifitness позволяет редактировать расписание одновременно и в
                                мобильном приложении, и в виджетах расписания на сайте и в соц.сетях. Все возможные
                                изменения в расписании (отмена/перенос занятия, замена тренера и т.д.) уже прописаны,
                                поэтому для внесения изменения достаточно просто поставить галочку.</p>
                        </div>
                    </li>
                    <!--<li class="faq__item"
                        data-toggle='{"animation":"js","parent":".faq__item","target":".faq__text", "className":"open"}'>
                        <h3 class="faq__name">Кто занимается наполнением контента?</h3>
                        <div class="faq__text">
                            <p>
                                Наполнять приложение и вносить изменения в виджеты онлайн-записи могут выбранные сотрудники клуба с помощью простой и удобной административной панели.</p>
                        </div>-->
                    </li>
                </ul>
            </div>
        </section>
<?
        include_once("_inc_subs.php"); ?>


<? include_once("_inc_footer.php"); ?>