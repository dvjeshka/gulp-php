/*
    Custom
 */


var $width_xs = 767;
var $width_md = 1024;
var $width_lg = 1169;

var selectedBlock = null;

var func = {
    'jsTimer': function () {
        $('[data-timer]').each(function () {
            var date = $(this).data('timer');

            $(this).countdown(date).on('update.countdown', function (event) {
                var $this = $(this).html(event.strftime(''
                    + '<div class="timer__item"><span class="timer__numb">%D</span><span class="timer__text">дня</span></div> '
                    + '<div class="timer__item"><span class="timer__numb">%H</span><span class="timer__text"> часов</span></div>'
                    + '<div class="timer__item"><span class="timer__numb">%M</span><span class="timer__text"> минут</span></div>'
                    + '<div class="timer__item"><span class="timer__numb">%S</span><span class="timer__text"> секунд</span></div>'));
            });
        });
    },
    'jsPopup': function () {
        if ($().colorbox) {



            $('[data-colorbox]').each(function () {

                var s = {

                    previous: '<span class="trans btn">Предыдущая статья</span>',
                    next: '<span class="trans btn btn_o">Следующая статья</span>',
                    close: '&times;',
                    maxWidth: '100%',

                    /*   maxHeight: '100%',*/
                    current: "{current} из {total}",
                    opacity: .6,
                    inline: true,
                    className: 'popup-wrap',
                    /* fixed: true,
                     scrolling: true,*/
                    onOpen: function () {
                        selectedBlock = $(this).data('block');
                    },
                    onClosed: function () {
                        selectedBlock = null;
                    }

                }, o = {};
                $.each($(this).data('colorbox'), function (k, v) {

                    if (k == 'iframe') {
                        o.width = '900px';
                        o.height = '600px';
                    }
                    o[k] = v;

                });

                s = $.extend(s, o);
                $(this).colorbox(s);
            });
        }
    },
    'jsPopupMessage': function (html) {
        $.colorbox({
            opacity: .6,
            close: '&times;',
            maxWidth: '100%',
            html: '<div class="popup-message-wrap">' + html + '</div>',
            scrolling: false
        });
    },
    'jsSlider': function () {

        if ($().slick) {
            var option = {
                speed: 500,
                autoplaySpeed: 6000,
                autoplay: true,
                touchMove: false,
                touchThreshold: 100,
                adaptiveHeight: false,
                prevArrow: '<i class="slick-arrow slick-prev trans"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 21" enable-background="new 0 0 12 21"><path d="M0 10.5c0-.2.1-.4.2-.5l9.6-9.8c.2-.3.6-.3.9 0l1 1.1c.3.3.3.7 0 .9l-8.1 8.3 8.1 8.3c.1.1.2.3.2.5s-.1.4-.2.5l-1 1.1c-.2.3-.6.3-.9 0l-9.6-9.9c-.1-.1-.2-.3-.2-.5z"/></svg></i>',
                nextArrow: '<i class="slick-arrow slick-next trans"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 21" enable-background="new 0 0 12 21"><path d="M12 10.5c0-.2-.1-.4-.2-.5l-9.6-9.8c-.2-.3-.6-.3-.9 0l-1 1.1c-.3.3-.3.7 0 .9l8.1 8.3-8.1 8.3c-.2.1-.3.3-.3.5s.1.4.2.5l1 1.1c.2.3.6.3.9 0l9.6-9.8c.2-.2.3-.4.3-.6z"/></svg></i>'


            };

            function slickInit(slider) {

                var flug = 1;
                $(slider).on('init', function (event, slick, direction) {
                    var $slider = this;

                    func.jsPopup()

                    $('[data-dots-listener]').each(function () {
                        var data = $(this).data('dotsListener'), list = this;
                        $(list).children().eq(0).addClass('active');
                        $(list).children().click(function () {

                            $(list).closest(data.parent).find(data.target).slick('slickGoTo', $(this).index())
                        })
                        $(this).closest(data.parent).find(data.target).on('afterChange', function (event, slick, currentSlide) {
                            $(list).children().removeClass('active');
                            $(list).children().eq(currentSlide).addClass('active')


                        });

                    })
                    if ($($slider).hasClass('slider-func__slider') || $($slider).hasClass('slider-reviews')) {

                        $($slider).find('.slick-slide').height('auto');
                        var slickTrack = $($slider).find('.slick-track'),
                            slickTrackHeight = $(slickTrack).height();
                        $($slider).find('.slick-slide').css('height', slickTrackHeight + 'px');

                    }


                });

                $(slider).slick(option);
                $(slider).slick('setPosition');


            }

            $(window).on('load resize scroll', function () {

                var slider = $('[data-slick]').not('.slick-initialized'),
                    flug_md = true,
                    flug_xs = true,
                    flug_lg = true;




                slider.each(function () {
                    if ($(this).data('slickStart') == 'md' && $(window).width() <= $width_md) {
                        slickInit(this);
                    }

                    if ($(this).data('slickStart') == 'xs' && $(window).width() <= $width_xs) {
                        slickInit(this);
                    }
                    if ($(this).data('slickStart') == undefined) {
                        slickInit(this);
                    }
                });

                if ($(window).width() > $width_lg && $(window).width() > $width_md && $('[data-slick-start="lg"].slick-initialized').length && flug_lg) {
                    setTimeout(function () {
                        $('[data-slick-start="lg"].slick-initialized').slick('destroy');
                        flug_lg = false;
                        flug_md = true;
                        flug_xs = true;
                    }, 500)
                }
                if ($(window).width() > $width_md && $('[data-slick-start="md"].slick-initialized').length && flug_md) {
                    setTimeout(function () {
                        $('[data-slick-start="md"].slick-initialized').slick('destroy');
                        flug_lg = true;
                        flug_md = false;
                        flug_xs = true;
                    }, 500)


                }
                if ($(window).width() > $width_xs & $('[data-slick-start="xs"].slick-initialized').length && flug_xs) {
                    setTimeout(function () {
                        $('[data-slick-start="xs"].slick-initialized').slick('destroy');
                        flug_lg = true;
                        flug_md = true;
                        flug_xs = false;
                    }, 500)
                }

                setTimeout(function () {

                    $('[data-slick-destroy].slick-initialized').each(function () {

                        if ($(this).data("slickDestroy") == "xs" && $(window).width() <= $width_xs) {
                            $(this).slick('destroy')
                        }
                        if ($(this).data("slickDestroy") == "lg" && $(window).width() <= $width_lg) {
                            $(this).slick('destroy')
                        }
                    });
                }, 500)
            });

        }








    },
    'jsToggle': function () {
        $('.js-scrl2top').on('click', function () {
            $('body,html').animate({scrollTop: 0}, 500);
        });

        $('[data-toggle]').on('click', function () {

            var data = $(this).data('toggle');
            var $self = this;
            if (data.target && data.className) {

                if (data.parent) {

                    if ($(this).closest(data.parent).hasClass(data.target.split('.')[1])) {
                        $(this).closest(data.parent).toggleClass(data.className);

                    } else {
                        if (data.animation === "js") $(this).closest(data.parent).find(data.target).slideToggle();
                        $(this).closest(data.parent).toggleClass(data.className)
                        $(this).closest(data.parent).find(data.target).toggleClass(data.className);
                    }
                } else {
                    if (data.animation === "js") $(data.target).slideToggle();
                    $(data.target).toggleClass(data.className);
                }
            }
            if (data.textHide && data.textShow) ($($self).text() != data.textHide) ? $($self).text(data.textHide) : $($self).text(data.textShow);
            if (data.removeClose === "Y") {
                $(document).unbind('click.closeToggle');
                $(document).bind('click.closeToggle', function (e) {
                    if ($(e.target).closest($self).length)
                        return false;
                    $(document).unbind('click.closeToggle');
                    $(data.target).removeClass(data.className);
                })
            }
        });


        /*     $(window).on('load resize scroll',function(){
         var wT = $(window).scrollTop(),
         hH = $('.header').height();
         if(wT > 0) {
         $('html').addClass('is-header-scrolled');

         }
         else {
         $('html').removeClass('is-header-scrolled');
         }
         });*/

        toggleMenu();
        function toggleMenu() {
            $('.js-nav-main-toggle').on('click', function () {
                $('html').toggleClass('is-nav-main-opened');


                $(document).bind('click.main-opened touchstart.main-opened', function (e) {
                    console.log(e);
                    if ($(e.target).closest(".header,.js-nav-main-toggle").length) return;

                    $('html').removeClass('is-nav-main-opened');
                    e.stopPropagation();
                    $(document).unbind('click.main-opened touchstart.main-opened');
                });  // установим обработчик нажатия кнопки мыши на элементе foo

            });
            var touchStartPos = 0, touchLeft = false;

            $('.header').on('touchstart', function (event) {
                var e = event.originalEvent;
                touchStartPos = e.touches[0].pageX;
            });

            $('.header').on('touchmove', function (event) {
                var e = event.originalEvent;
                if (touchStartPos - 50 < e.touches[0].pageX) {
                    touchLeft = true;
                }
            });

            $('.header').on('touchend', function (event) {
                if (touchLeft) {
                    $('html').toggleClass('is-nav-main-opened');
                    touchLeft = false;
                }
            });



        }

        $('.js-scrl2top').on('click', function () {
            $('body,html').animate({scrollTop: 0}, 500);
        });
        toggleClick();
        function toggleClick() {
            $('.js-toggle-click').on('click', function (e) {
                e.preventDefault();
                var toogleClass = $(this).data('toogleclass'),
                    parent = '.' + $(this).data('parent'),
                    target = '.' + $(this).data('target'),
                    elemParent = $(this).closest(parent);

                if ($(elemParent).hasClass(target.split('.')[1])) {
                    $(elemParent).toggleClass(toogleClass);
                } else {
                    $(this).closest(parent).find(target).toggleClass(toogleClass);
                }

            });
        }
    },


    'jsScrollTarget': function () {
        $('[data-scroll-target]').on('click', function (e) {
            e.preventDefault();
            var data = $(this).data('scrollTarget');
            var target = data.href || $(this).attr('href'),
                offset = +data.offset || 0,
                top = $(target).offset().top - offset;


            $('body,html').animate({scrollTop: top}, 500);
            /*  history.replaceState(null, null,target);*/
        });
    },


    'jsFormAction': function () {
        formActions();
        function formActions() {
            $('.js-validated').each(function () {
                $(this).validate({
                    errorClass: 'input-error',
                    /*errorElement: 'span',*/
                    rules: {
                        company: {
                            required: true,
                        },
                        name: {
                            required: true,
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        text: {
                            required: true,
                        },
                        phone: {
                            required: true,
                            minlength: 10
                        },
                        agree: {
                            required: true,
                        }
                    },
                    messages: {
                        company: {
                            required: "не заполнено"
                        },
                        name: {
                            required: "не заполнено"
                        },
                        email: {
                            required: "не заполнено",
                            email: 'Не корректный Email'
                        },
                        text: {
                            required: "не заполнено"
                        },
                        phone: {
                            required: "не заполнено"
                        },
                        agree: {
                            required: "подтвердите согласие"
                        }
                    },
                    submitHandler: function (form) {
                        //###NEW_CODE###//

                        if (!$(form).hasClass('no-metrics') && window.hasOwnProperty('yaCounter22994086') && "object" === typeof yaCounter22994086) {
                            var _page = window.location.pathname.split('/')[1];
                            if (_page.length === 0) {
                                _page = 'main';
                            }
                            _page = _page.split('.php')[0];
                            if ("object" === typeof yaCounter22994086) {
                                yaCounter22994086.reachGoal(_page);
                            }
                            if ("function" === typeof fbq) {
                                fbq('track', _page);
                            }
                        }

                        var textVariant = typeof($(form).data('variant')) !== 'undefined' ? $(form).data('variant') : '1';

                        $('#popup-success').find('.text-variant').hide();
                        $('#popup-success').find('.text-variant-' + textVariant).show();

                        $(form).find('[type="submit"]').prop('disabled', true);
                        $(form).find('[type="submit"]').css('opacity', 0.75);
                        $.ajax({
                            url: 'https://new.mobifitness.ru/ajax.php?action=send',
                            type: 'POST',
                            data: $(form).serialize() + '&selectedBlock=' + encodeURIComponent(selectedBlock)
                        }).always(function () {
                            // $.colorbox.close()
                            // $('input:not([type="submit"]):not([name="group"])').val('');
                            $(form).find('[type="submit"]').prop('disabled', false);
                            $(form).find('[type="submit"]').css('opacity', 1);
                            // setTimeout(function () {
                            //$('.js-popup-success-click').click();
                            // }, 500);
                        });

                        setTimeout(function () {
                            $.colorbox.close();
                            $('input:not([type="submit"]):not([name="group"])').val('');
                            setTimeout(function () {
                                $('.js-popup-success-click').click();
                            }, 500);
                        }, 500);

                        //###END_NEW_CODE###//
                    }
                });
            })


        }
    },
    'jsPrint': function () {

        function jsPrintContent() {
            $('[data-print]').on('click', function () {
                var data = $(this).data('print');
                if (data.target) {
                    var html = $(data.target).html();
                    Popup(html);
                }
            });
        }

        jsPrintContent();

        function Popup(data) {

            var mywindow = window.open('', '', 'height=600,width=1200');
            mywindow.document.write('<html><head><title>&nbsp;</title>');
            mywindow.document.write('</head><body ><link rel="stylesheet" href="/local/templates/.default/include/enertex/css/style.css"  type="text/css">');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');


            setTimeout(function () {
                mywindow.document.close();
                mywindow.focus();
                mywindow.print();
                mywindow.close();
            }, 300);


            return true;
        }
    },
    'jsCommon': function () {

        var divRates = $('.js-rates__select');
        divRates.find('.rates__select-name').click(function () {
            var $self = this;


            $(document).bind('click.closeToggleSelectRates', function (e) {
                if ($(e.target).closest('.js-rates__select.open.g-icon-down-open').length)
                        return false;

                setTimeout(function () {
                    $('.js-rates__select').removeClass('open g-icon-down-open');
                    $(document).unbind('click.closeToggleSelectRates');
                }, 100)

                });
            $($self).closest('.rates__item').siblings().find('.rates__select').removeClass('open g-icon-down-open');
            $($self).parent().toggleClass('open g-icon-down-open')


        })
        $('.js-rates__select label').click(function (e) {
            var $self = this;


            var text = $($self).find('.chr__text .h1').text(),
                percent = $($self).find('.chr__text .h1').data('percent'),
                sale = $($self).find('.rates__sale_select .rates__sale-name').text();
            $('.js-rates__select').removeClass('open g-icon-down-open')
            $($self).closest('.js-rates__select').removeClass('open g-icon-down-open').find('.rates__select-name .h1').text(text);
            $($self).closest('.js-rates__select').find('.rates__select-sale-parent .rates__sale-name').text(sale);
            $($self).closest('.rates__box').find('.rates__saving-numb').text(percent);
        })

        ratesAdaptive();

        function ratesAdaptive() {

            $('.js-rates__btn-arrow').on('click', function () {


                $(this).parent().toggleClass('open').find('.rates-table').slideToggle();
                return
            })

            var tableReal = $('.rates-table');


            $('.rates__item ').each(function () {
                var table = tableReal.clone(),
                    index = $(this).index();
                index++;


                table.find('.rates-table__row').each(function () {
                    $(this).has('.rates-table__point_' + index).addClass('rates-table__row_select_' + index)
                });
                table.find('.rates-table__row').each(function () {
                    $(this).has('.rates-table__point_' + index).addClass('rates-table__row_select_' + index)

                    $(this).has('.rates-table__point_' + index).closest('.rates-table__item').find('.rates-table__name').addClass('rates-table__row_select_' + index)
                });
                table.find('.rates-table__row').each(function () {
                    $(this).not('.rates-table__row_select_' + index).remove()
                });

                table.find('.rates-table__btns p:eq(' + index + ')').addClass('g-show_xs');


                $(this).find('.rates__adaptive').prepend(table);


            })

            /*        $('.rates__adaptive .rates-table').find('[data-toggle]').on('click', function () {

                        var data = $(this).data('toggle');
                        var $self = this;
                        if (data.target && data.className) {

                            if (data.parent) {

                                if ($(this).closest(data.parent).hasClass(data.target.split('.')[1])) {
                                    $(this).closest(data.parent).toggleClass(data.className);

                                } else {
                                    if (data.animation === "js") $(this).closest(data.parent).find(data.target).slideToggle();
                                    $(this).closest(data.parent).toggleClass(data.className)
                                    $(this).closest(data.parent).find(data.target).toggleClass(data.className);
                                }
                            } else {
                                if (data.animation === "js") $(data.target).slideToggle();
                                $(data.target).toggleClass(data.className);
                            }
                        }
                        if (data.textHide && data.textShow) ($($self).text() != data.textHide) ? $($self).text(data.textHide) : $($self).text(data.textShow);
                        if (data.removeClose === "Y") {
                            $(document).unbind('click.closeToggle');
                            $(document).bind('click.closeToggle', function (e) {
                                if ($(e.target).closest($self).length)
                                    return false;
                                $(document).unbind('click.closeToggle');
                                $(data.target).removeClass(data.className);
                            })
                        }
                    });*/
        }
    },

    'jsPX': function () {
        var wW = $(window).width();
        var wH = $(window).height();
        var e = {'clientX': wW / 2, 'clientY': wH / 2}

        function init(e) {

            $('[data-px]').each(function () {
                var data = $(this).data('px'),

                    x = -((e.clientX - wW / 4) * data.d / data.s);
                y = -((e.clientY - wH / 4) * data.d / data.s),
                    sl = (e.clientY / wW / 25) / 1.5 + .95;
                //$(this).css({"-webkit-transform": "translate(" + x + "%," + y + "%)"});
                $(this).css({"transform": "translate(" + x + "%," + y + "%) scale(" + sl + ")"});
            });
        }

        if ($(window).width() > $width_xs) init(e);
        $(window).on('mousemove', function (e) {
            init(e)
        });
    },
    'jsTest': function () {
        var div = $('.js-test'), count = 0;
        div.find('.js-test-next').on('click', function (e) {
            e.preventDefault();
            if ($(this).text() == "Да") {
                count++;
                if (count >= 3) {
                    div.find('.test__result_1').removeClass('test__result_active');
                    div.find('.test__result_2').addClass('test__result_active')
                }

            }
            div.find('.test__item').removeClass('test__item_active');
            $(this).closest('.test__item').next('.test__item').addClass('test__item_active')
        })
        div.find('.js-test-prev').on('click', function (e) {
            e.preventDefault();

            div.find('.test__item').removeClass('test__item_active');
            $(this).closest('.test__item').prev('.test__item').addClass('test__item_active')
        })
    },
    'jsLoadStat': function () {
        $.get("https://www.mobifitness.ru/stat.txt", function (data) {
            data = eval('(' + data + ')');
            $('[data-target="clients"]').text(data.clients);
            $('[data-target="users"]').text(data.users);
            $('[data-target="applications"]').text(data.applications);
            $('[data-target="social"]').text(data.social);
            $('[data-target="cities"]').text(data.cities);
            $('[data-target="clubs"]').text(data.clubs);
        });
    },
    'jsSetTariff': function (name, price_tag_name) {
        $('[name="tariff_name"]').val(name);
        $('[name="tariff_period"]').val($('[name="' + price_tag_name + '"]:checked').val());
    }
};

var app = {
    'init': function () {
        func.jsCommon();
        func.jsTimer();
        func.jsTest();
        func.jsPX();


        func.jsPopup();

        func.jsSlider();
        func.jsToggle();
        func.jsFormAction();

        func.jsPrint();

        func.jsScrollTarget();
        func.jsLoadStat();

    },
    'load': function () {

        func.jsPX();
        //func.jsDotDotDot();
        func.jsScrollTarget();

    },
    'resize': function () {

    }
};

$(function () {


    var prevScroll_top = 100;
    $(window).on('load resize scroll', function () {

        //$.colorbox.resize();

        var wT = $(window).scrollTop(),
            wH = $(window).height(),
            hH = $('.header').height(),
            fT = $('.footer').offset().top;

        /*  if (wT < prevScroll_top)
              $('html').removeClass('is-header-hide');
          else
              $('html').addClass('is-header-hide');*/
        prevScroll_top = wT;
        if (wT > wH)
            $('html').addClass('is-scrl2top-visible');
        else
            $('html').removeClass('is-scrl2top-visible');
        if (wT > hH)
            $('html').addClass('is-header-scrolled');
        else
            $('html').removeClass('is-header-scrolled');
        if (wT > fT - wH)
            $('html').addClass('is-at-footer');
        else
            $('html').removeClass('is-at-footer');

        //if (wT < hH) $('html').removeClass('is-header-hide');
    });

    app.init();
    $(window).on('load', function () {
        app.load();
    });
    $(window).on('resize', function () {
        app.resize();
    });

});