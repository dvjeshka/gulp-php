<? include_once("_inc_header.php"); ?>
<? include_once("_inc_breadcrumbs.php"); ?>


    <form action="?">
        <fieldset>
            <legend>Тексовые поля</legend>
            <ul>
                <li><input type="text" name="" value="" placeholder="input[type='text']"/></li>
                <li><input type="email" name="" value="" placeholder="input[type='email']"/></li>
                <li><input type="search" name="" value="" placeholder="input[type='search']"/></li>
                <li><input type="password" name="" value="" placeholder="input[type='password']"/></li>

                <li><input type="number" name="" value="" placeholder="input[type='number']"/></li>
                <li><input type="tel" name="" value="" placeholder="input[type='tel']"/></li>


                <li><input type="url" name="" value="" placeholder="input[type='url']"/></li>
                <li><input type="date" name="" value="" placeholder="input[type='date']"/></li>
                <li><textarea name="" placeholder="textarea"></textarea></li>
                <li>
                    <select>
                        <option selected>option</option>
                        <option>option</option>
                        <option>option</option>
                    </select>
                </li>
                <li>
                    <select multiple>
                        <option>option</option>
                        <option>option</option>
                        <option>option</option>
                    </select></li>
                <li>
                    <select data-select>
                        <option>option 0</option>
                        <option>option 0</option>
                        <option>option 1</option>
                    </select>
                </li>
            </ul>
        </fieldset>
        <fieldset>
            <legend>Кнопки</legend>
            <ul>
                <li><a href="javascript:" class="btn">a.btn</a></li>
                <li><input type="file" name="" value="file" placeholder="input[type='url']"/></li>
                <li>
                    <button>button</button>
                </li>
                <li>
                    <button type="button">button</button>
                </li>
                <li><input type="submit" name="" value="input submit"></li>
                <li><input type="reset" name="" value="input reset"></li>
                <li>
                <li><input type="range" name="" value=""></li>
                </li>
            </ul>
        </fieldset>
        <fieldset>
            <legend>Флажки</legend>
            <fieldset>
                <legend>checkbox</legend>
                <ul>
                    <li><label><input type="checkbox"><span class="chr-text">checkbox 1</span></label></li>
                    <li><label><input type="checkbox"><span class="chr-text">checkbox 2</span></label></li>
                </ul>

            </fieldset>
            <fieldset>
                <legend>radio</legend>
                <ul>
                    <li><label><input type="radio"><span class="chr-text">radio 1</span></label></li>
                    <li><label><input type="radio"><span class="chr-text">radio 2</span></label></li>
                </ul>
            </fieldset>
        </fieldset>

    </form>


<? include_once("_inc_footer.php"); ?>